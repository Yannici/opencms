/*********************************************\
|****************** OPENCMS ******************|
|*********************************************|
|* @author Yannici                           *|
|* @copyright Yannici                        *|
|*********************************************|
|* @since 04.11.2013                         *|
\*********************************************/

$(document).ready(function() {
    tadaGo();
    tadaGo2();
                    
    $('.close').click(function() {
        if($(this).data('dismiss') === 'alert') {
            $(this).parents('#lr_error').slideUp(500);
        } 
    });
                    
    function tadaGo2() {
        $('.checkin2').toggleClass('pulse');
        setTimeout(tadaGo2, 5000);
    }
                    
    function tadaGo() {
        $('.checkin').toggleClass('tada');
        setTimeout(tadaGo, 3500);
    }
    
    $('#user-profile').unbind();
       
    $('#user-profile').mouseover(function() {
        $(this).css('cursor', 'pointer');
        $('#user-details').stop(false, true);
        $('#user-details').fadeIn();
    });
       
    $('#user-profile').mouseout(function() {
        $(this).css('cursor', 'default');
        $('#user-details').stop(false, true);
        $('#user-details').fadeOut();
    });
    
    /*$('input[type="checkbox"], input[type="radio"]').each(function(){
        var self = $(this),
          label = self.next(),
          label_text = label.text();
          
        label.remove();
        self.iCheck({
          checkboxClass: 'icheckbox_line-grey',
          radioClass: 'iradio_line-grey',
          insert: '<div class="icheck_line-icon"></div>' + label_text
        });
    });*/
});