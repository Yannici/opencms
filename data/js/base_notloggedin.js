/*********************************************\
|****************** OPENCMS ******************|
|*********************************************|
|* @author Yannici                           *|
|* @copyright Yannici                        *|
|*********************************************|
|* @since 04.11.2013                         *|
\*********************************************/


$(document).ready(function() {
    $('#reg').fancybox({
        maxWidth	: 800,
        maxHeight	: 600,
        fitToView	: false,
        width		: '70%',
        height		: '70%',
        autoSize	: true,
        closeClick	: false,
        openEffect	: 'elastic',
        closeEffect	: 'elastic'
    });
    $('#hero').slideDown(1000);
    $('#thumb1').delay(1000).show(2000);
    $('#thumb2').delay(1800).show(2000);
    $('#thumb3').delay(2600).show(2000);
                                
    $('#thumb1').mouseover(function() {
        $(this).stop(true, false);
        $(this).animate({
            opacity: '0.6'
        });
    });
    $('#thumb1').mouseout(function() {
        $(this).stop(true, false);
        $(this).animate({
            opacity: '1'
        });
    });
    $('#thumb2').mouseover(function() {
        $(this).stop(true, false);
        $(this).animate({
            opacity: '0.6'
        });
    });
    $('#thumb2').mouseout(function() {
        $(this).stop(true, false);
        $(this).animate({
            opacity: '1'
        });
    });
    $('#thumb3').mouseover(function() {
        $(this).stop(true, false);
        $(this).animate({
            opacity: '0.6'
        });
    });
    $('#thumb3').mouseout(function() {
        $(this).stop(true, false);
        $(this).animate({
            opacity: '1'
        });
    });
});