/*********************************************\
|****************** OPENCMS ******************|
|*********************************************|
|* @author Yannici                           *|
|* @copyright Yannici                        *|
|*********************************************|
|* @since 04.11.2013                         *|
\*********************************************/

$(document).ready(function() {
    var drop = false;
    $('.dropdown').click(function() {
        drop = true;
        var self = $(this);
        $('.dropdown').each(function() {
            if( ! $(this).is(self)) {
                $(this).removeClass('open'); 
            }
        });
        
        self.toggleClass('open');
    });
    
    $('body').click(function() {
        if(!drop) {
            $('.dropdown').each(function() {
                $(this).removeClass('open'); 
            });
        } else {
            drop = false;
        }
    });
});

