<?php
/*********************************************\
|****************** OPENCMS ******************|
|*********************************************|
|* @author Yannici                           *|
|* @copyright Yannici                        *|
|*********************************************|
|* @since 04.11.2013                         *|
\*********************************************/

class Employee_model extends CI_Model {
    
    private $_last_error = null;
    
    public function __construct() {
        parent::__construct();
    }
    
    public function get_last_error() {
        return $this->_last_error;
    }
    
    public function get_employee() {
        $employees = array();
        
        $result = $this->get_employee_groups('rank', '>', $this->config->item('admin_min_rank'));
        foreach($result As $row) {
            $this->db->where('rank', $row['rank']);
            $this->db->order_by('username');
            $arr = $this->db->get('users');
            $employees[$row['name']] = $arr->result_array();
        }
        
        return $employees;
    }
    
    public function get_rank_name($rank) {
        $result = $this->db->where('rank', $rank)->limit(1)->get('cms_employee_ranking')->result_array();
        
        if(count($result) == 0) 
            return 'NOT_FOUND';
        
        return $result[0]['name'];
    }
    
    public function delete_group($id) {
        if(count($this->db->get_where('cms_employee_ranking', array('id' => $id))->result_array()) > 0) {
            $this->db->where('id', $id);
            if($this->db->delete('cms_employee_ranking')) {
                return TRUE;
            } else {
                $this->_last_error = 'MYSQL-Error: ' . $this->db->_error_message();
                return FALSE;
            }
        } else {
            $this->_last_error = $this->lang->line('hotel_admin_employee_ranking_not_found', $id);
            return FALSE;
        }
    }
    
    public function get_group_description($group_name) {
        $this->db->limit(1);
        $this->db->where('name', $group_name);
        $result = $this->db->get('cms_employee_ranking')->result_array();
        
        if(count($result) == 0) {
            return 'NO_DESCRIPTION_FOUND';
        }
        
        $group = $result[0];
        return $group['description'];
    }
    
    public function get_employee_groups($field = '', $filter = '', $value = '') {
        $this->db->order_by('rank', 'desc');
        if(!empty($field) && !empty($filter) && !empty($value)) {
            $this->db->where($field . ' ' . $filter, $value);
        }
        return $this->db->get('cms_employee_ranking')->result_array();
    }
    
    public function get_employee_ranks($field = '', $filter = '', $value = '') {
        $this->db->order_by('rank', 'desc');
        if(!empty($field) && !empty($filter) && !empty($value)) {
            $this->db->where($field . ' ' . $filter, $value);
        }
        $result = $this->db->get('cms_employee_ranking')->result_array();
        
        if(count($result) == 0) 
            return null;
        
        $ranks = array();
        foreach($result As $row) {
            $ranks[$row['rank']] = utf8_decode($row['name']);
        }
        
        return $ranks;
    }
    
    private function _prepare_input() {
        return array(
            'rank' => $this->input->get_post('input_rank', TRUE),
            'name' => utf8_encode($this->input->get_post('input_name', TRUE)),
            'description' => $this->input->get_post('textarea_description', TRUE)
        );
    }
    
    public function add_group() {
        if($this->db->insert('cms_employee_ranking', $this->_prepare_input())) {
            return TRUE;
        } else {
            $this->_last_error = 'MySQL-Error: ' . $this->db->_error_message();
            return FALSE;
        }
    }
    
    public function edit_group($id) {
        if($this->db->update('cms_employee_ranking', $this->_prepare_input(), array('id' => $id))) {
            return TRUE;
        } else {
            $this->_last_error = 'MySQL-Error: ' . $this->db->_error_message();
            return FALSE;
        }
    }
    
    public function get_employee_group($id) {
        $this->db->order_by('rank', 'desc');
        $this->db->where('id', $id);
        $this->db->limit(1);
        $result = $this->db->get('cms_employee_ranking')->result_array();
        
        return (count($result) == 0) ? null : $result[0];
    }
    
    public function get_employee_group_by_rank($rank) {
        $this->db->where('rank', $rank);
        $this->db->limit(1);
        $result = $this->db->get('cms_employee_ranking')->result_array();
        
        return (count($result) == 0) ? null : $result[0];
    }
}

?>
