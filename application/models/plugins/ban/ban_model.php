<?php
/*********************************************\
|****************** OPENCMS ******************|
|*********************************************|
|* @author Yannici                           *|
|* @copyright Yannici                        *|
|*********************************************|
|* @since 04.11.2013                         *|
\*********************************************/


class Ban_model extends CI_Model {
    
    private $_last_error;
    
    public function __construct() {
        parent::__construct();
    }
    
    public function search_bans($value, $type) {
        $this->db->where(array('value' => $value, 'bantype' => $type));
        $result = $this->db->get('bans')->result_array();
        
        return count($result);
    }
    
    public function ban_user($value, $reason, $length, $type) {
        if($type == 'user') {
            $user = $this->user_model->search_user($value);
            if($user == null) {
                $this->_last_error = $this->lang->line('hotel_admin_useredit_error_user_not_exists');
                return FALSE;
            }
            
            if($user->rank >= $this->user->rank) {
                $this->_last_error = $this->lang->line('hotel_admin_ban_no_permission');
                return FALSE;
            }
            
            $value = $user->username;
        }
        
        if($this->search_bans($value, $type) > 0) {
            $this->_last_error = $this->lang->line('hotel_admin_ban_user_already_banned');
            return FALSE;
        }
        
        $data = array(
            'bantype' => $type,
            'value' => $value,
            'reason' => $reason,
            'expire' => time()+$length,
            'added_by' => $this->user->username,
            'added_date' => date($this->config->item('hotel_default_datetime'), time())
        );
        
        if( ! $this->db->insert('bans', $data)) {
            $this->_last_error = $this->db->_error_message();
            return FALSE;
        }
        
        return TRUE;
    }
    
    public function unban_user($value, $type) {
        if($type == 'user') {
            $user = $this->user_model->search_user($value);
            if($user == null) {
                $this->_last_error = $this->lang->line('hotel_admin_useredit_error_user_not_exists');
                return FALSE;
            }
            
            $value = $user->username;
        }
        
        if($this->search_bans($value, $type) == 0) {
            $this->_last_error = $this->lang->line('hotel_admin_ban_user_already_unbanned');
            return FALSE;
        }
        
        if( ! $this->db->delete('bans', array('value' => $value, 'bantype' => $type))) {
            $this->_last_error = $this->db->_error_message();
            return FALSE;
        }
        
        return TRUE;
    }
    
    public function get_last_error() {
        return $this->_last_error;
    }
}

?>
