<?php
/*********************************************\
|****************** OPENCMS ******************|
|*********************************************|
|* @author Yannici                           *|
|* @copyright Yannici                        *|
|*********************************************|
|* @since 04.11.2013                         *|
\*********************************************/

class Core_model extends CI_Model {

    public function __construct() {
        parent::__construct();
        $this->load->database();
    }

    public function open_hash($password, $username) {
        return md5(sha1($password) . md5(sha1(strtolower($username))));
    }

    public function get_ip() {
        return $this->input->ip_address();
    }

    public function locate($location) {
        header('Location: ' . $location);
        exit();
    }

    public function guid() {
        if (function_exists('com_create_guid')) {
            return com_create_guid();
        } else {
            mt_srand((double) microtime() * 10000); //optional for php 4.2.0 and higher.
            $charid = strtoupper(md5(uniqid(rand(), true)));
            $hyphen = chr(45); // "-"
            $uuid = chr(123)// "{"
                    . substr($charid, 0, 8) . $hyphen
                    . substr($charid, 8, 4) . $hyphen
                    . substr($charid, 12, 4) . $hyphen
                    . substr($charid, 16, 4) . $hyphen
                    . substr($charid, 20, 12)
                    . chr(125); // "}"
            return $uuid;
        }
    }

    public function users_online() {
        $array = $this->db->select('id, COUNT(id) As count', false)->from('users')->where('online', '1')->get()->result_array();
        return (count($array) > 0) ? $array[0]['count'] : 0;
    }

    public function get_pagename($page, $pagearray) {
        $explode = explode('/', $page, 2);

        if (!isset($pagearray[$page]) && !isset($pagearray[$explode[0]]))
            return 'Undefined';

        $page = (isset($pagearray[$page])) ? $page : $explode[0];
        return ($this->starts_with($pagearray[$page], 'lang:')) ? $this->lang->line(str_replace('lang:', '', $pagearray[$page])) : $pagearray[$page];
    }

    public function starts_with($haystack, $needle, $case = false) {
        return ($case) ? $needle === '' || stripos($haystack, $needle) === 0 : $needle === '' || strpos($haystack, $needle) === 0;
    }

    public function ends_with($haystack, $needle, $case = false) {
        return ($case) ? ($needle === '' || strtolower(substr($haystack, -strlen($needle))) === strtolower($needle)) : ($needle === '' || substr($haystack, -strlen($needle)) === $needle);
    }

    public function get_string_language($string) {
        $value = $this->lang->line(str_replace('lang:', '', $string));
        if (!$value) {
            return $string;
        }

        return $value;
    }

    public function MUS($command, $data = '') {
        if($this->config->item('mus_activated')) {
            $MUSdata = $command . chr(1) . $data;
            $socket = @socket_create(AF_INET, SOCK_STREAM, getprotobyname('tcp'));
            @socket_connect($socket, $this->config->item('mus_host'), $this->config->item('mus_port'));
            @socket_send($socket, $MUSdata, strlen($MUSdata), MSG_DONTROUTE);
            @socket_close($socket);
        }
    }

    public function pretty_date($date) {
        $now = time();
        $d = $now - $date;
        if ($d < 60) {
            $d = round($d);
            return ($d == 1) ? $this->lang->line('hotel_pretty_date_second') : $this->lang->line('hotel_pretty_date_seconds', $d);
        }
        $d = $d / 60;
        if ($d < 12.5) {
            $d = round($d);
            return ($d == 1) ? $this->lang->line('hotel_pretty_date_minute') : $this->lang->line('hotel_pretty_date_minutes', $d);
        }
        switch (round($d / 15)) {
            case 1:
                return $this->lang->line('hotel_pretty_date_quarter_hour');
            case 2:
                return $this->lang->line('hotel_pretty_date_half_hour');
            case 3:
                return $this->lang->line('hotel_pretty_date_three_quarter_hour');
        }
        $d = $d / 60;
        if ($d < 6) {
            $d = round($d);
            return ($d == 1) ? $this->lang->line('hotel_pretty_date_hour') : $this->lang->line('hotel_pretty_date_hours', $d);
        }
        if ($d < 36) {
            $day_start = 6;
            if (date('j', ($now - $day_start * 3600)) == date('j', ($date - $day_start * 3600)))
                $r = $this->lang->line('hotel_pretty_date_today');
            elseif (date('j', ($now - ($day_start + 24) * 3600)) == date('j', ($date - $day_start * 3600)))
                $r = $this->lang->line('hotel_pretty_date_yesterday');
            else
                $r = $this->lang->line('hotel_pretty_date_before_yesterday');
            $hour_date = intval(date('G', $date)) + (intval(date('i', $date)) / 60);
            $hour_now = intval(date('G', $now)) + (intval(date('i', $now)) / 60);
            if ($hour_date >= 22.5 || $hour_date < $day_start) {
                $r = $r == $this->lang->line('hotel_pretty_date_today') ? $this->lang->line('hotel_pretty_date_last_night') : $r . ' ' . $this->lang->line('hotel_pretty_date_night');
            } elseif ($hour_date >= $day_start && $hour_date < 9)
                $r .= ' ' . $this->lang->line('hotel_pretty_date_morning');
            elseif ($hour_date >= 9 && $hour_date < 11.5)
                $r .= ' ' . $this->lang->line('hotel_pretty_date_forenoon');
            elseif ($hour_date >= 11.5 && $hour_date < 13.5)
                $r .= ' ' . $this->lang->line('hotel_pretty_date_noon');
            elseif ($hour_date >= 13.5 && $hour_date < 18)
                $r .= ' ' . $this->lang->line('hotel_pretty_date_afternoon');
            elseif ($hour_date >= 18 && $hour_date < 22.5)
                $r .= ' ' . $this->lang->line('hotel_pretty_date_evening');
            return $r;
        }
        $d = $d / 24;
        if ($d < 7) {
            $d = round($d);
            return ($d == 1) ? $this->lang->line('hotel_pretty_date_day') : $this->lang->line('hotel_pretty_date_days', $d);
        }
        $d_weeks = $d / 7;
        if ($d_weeks < 4) {
            $d = round($d_weeks);
            return ($d == 1) ? $this->lang->line('hotel_pretty_date_week') : $this->lang->line('hotel_pretty_date_weeks', $d);
        }
        $d = $d / 30;
        if ($d < 12) {
            $d = round($d);
            return ($d == 1) ? $this->lang->line('hotel_pretty_date_month') : $this->lang->line('hotel_pretty_date_months', $d);
        }
        if ($d < 18)
            return $this->lang->line('hotel_pretty_date_year');
        if ($d < 21)
            return $this->lang->line('hotel_pretty_date_one_and_half_year');
        $d = round($d / 12);
        return $this->lang->line('hotel_pretty_date_years', $d);
    }

    public function get_count_online_workers() {
        return $this->db->where(array('rank >=' => $this->config->item('admin_min_rank'), 'online' => '1'))->from('users')->count_all_results();
    }

    public function get_count_online_users() {
        return $this->db->where('online', '1')->from('users')->count_all_results();
    }

    public function get_user_record() {
        $stats = $this->db->from('system_stats')->get()->result_array();
        return $stats[0]['users'];
    }

    public function get_user_count() {
        return $this->db->count_all_results('users');
    }
}

?>
