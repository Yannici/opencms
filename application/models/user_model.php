<?php

/* * *******************************************\
  |****************** OPENCMS ******************|
  |*********************************************|
  |* @author Yannici                           *|
  |* @copyright Yannici                        *|
  |*********************************************|
  |* @since 04.11.2013                         *|
  \******************************************** */

/**
 * Description of user_model
 */
class User_model extends CI_Model {

    private $_last_error = null;

    public function __construct() {
        parent::__construct();
    }
    
    public function search_user($search) {
        $this->db->select('id');
        $this->db->where('id', $this->db->escape_like_str($search));
        $this->db->or_where('username =', $this->db->escape_like_str($search));
        $this->db->limit(1);
        $result = $this->db->get('users')->result_array();
        
        if(count($result) == 0) return null;
        
        $r = $result[0];
        $user = new User(false);
        $user->set_user_by_id($r['id']);
        return $user;
    }
    
    public function get_bans($username) {
        $this->db->where('value', $username);
        $this->db->order_by('id', 'asc');
        $result = $this->db->get('bans')->result_array();
        
        if(count($result) == 0) 
            return null;
        
        return $result;
    }
    
    public function save_edit($id) {
        $mail = $this->input->get_post('input_email', TRUE);
        $credits = $this->input->get_post('input_credits', TRUE);
        $activity_points = $this->input->get_post('input_activity_points', TRUE);
        $vip_points = $this->input->get_post('input_vip_points', TRUE);
        $rank = intval($this->input->get_post('dropdown_rank', TRUE));
        $vip = (!$this->input->get_post('checkbox_vip', TRUE)) ? '0' : '1';
        $block_newfriends = (!$this->input->get_post('checkbox_block_newfriends', TRUE)) ? '0' : '1';
        $hide_online = (!$this->input->get_post('checkbox_hide_online', TRUE)) ? '0' : '1';
        $trading = (!$this->input->get_post('checkbox_trading', TRUE)) ? '0' : '1';
        
        $data = array(
            'mail' => $mail,
            'credits' => $credits,
            'activity_points' => $activity_points,
            'vip_points' => $vip_points,
            'rank' => $rank,
            'vip' => $vip,
            'block_newfriends' => $block_newfriends,
            'hide_online' => $hide_online,
            'accept_trading' => $trading
        );
        
        if(!$this->db->update('users', $data, array('id' => $id))) {
            $this->_last_error = 'MySQL-Error: ' . $this->db->_error_message();
            return FALSE;
        }
        
        return TRUE;
    }
    
    public function is_online($id) {
        $result = $this->db->select('online')->where('id', $id)->limit(1)->get('users')->result_array();
        
        if(count($result) == 0) {
            return FALSE;
        }
        
        return ($result[0]['online'] == '1');
    }

    public function delete_user($user) {
        $id = $user->id;

        if($id == 0 || $id == null) {
            $this->_last_error = $this->lang->line('hotel_admin_members_delete_no_user_found');
            return FALSE;
        }
        
        if($this->user->rank <= $user->rank) {
            $this->_last_error = $this->lang->line('hotel_admin_members_delete_no_permission', $user->username);
            return FALSE;
        }
        
        if (count($this->db->get_where('users', array('id' => $id))->result_array()) > 0) {
            if (    $this->db->delete('users', array('id' => $id)) &&
                    $this->db->delete('user_badges', array('user_id' => $id)) &&
                    $this->db->delete('user_effects', array('user_id' => $id)) &&
                    $this->db->delete('user_favorites', array('user_id' => $id)) &&
                    $this->db->delete('user_achievements', array('user_id' => $id)) &&
                    $this->db->delete('user_ignores', array('user_id' => $id)) &&
                    $this->db->delete('user_info', array('user_id' => $id)) &&
                    $this->db->delete('user_pets', array('user_id' => $id)) &&
                    $this->db->delete('user_quests', array('user_id' => $id)) &&
                    $this->db->delete('user_roomvisits', array('user_id' => $id)) &&
                    $this->db->delete('user_stats', array('id' => $id)) &&
                    $this->db->delete('user_subscriptions', array('user_id' => $id)) &&
                    $this->db->delete('user_tags', array('user_id' => $id)) &&
                    $this->db->delete('user_wardrobe', array('user_id' => $id))) {

                return TRUE;
            } else {
                $this->_last_error = 'MYSQL-Error: ' . $this->db->_error_message();
                return FALSE;
            }
        } else {
            $this->_last_error = $this->lang->line('hotel_admin_members_delete_no_user_found');
            return FALSE;
        }
    }

    public function get_last_error() {
        return $this->_last_error;
    }

}
?>
