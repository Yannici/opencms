<?php
/*********************************************\
|****************** OPENCMS ******************|
|*********************************************|
|* @author Yannici                           *|
|* @copyright Yannici                        *|
|*********************************************|
|* @since 04.11.2013                         *|
\*********************************************/

class News_model extends CI_Model {
    
    private $_last_error = '';
    
    public function __construct() {
        parent::__construct();
    }
    
    public function get_last_error() {
        return $this->_last_error;
    }
    
    private function _prepare_input($type) {
        return ($type == 'add') ? $data = array(
            'title' => utf8_encode($this->input->get_post('input_title', TRUE)),
            'shortstory' => utf8_encode($this->input->get_post('input_shortstory', TRUE)),
            'image' => $this->db->escape_str($this->input->get_post('input_image', TRUE)),
            'longstory' => utf8_encode($this->input->get_post('textarea_longstory', TRUE)),
            'published' => time(),
            'author' => $this->user->id
        ) : $data = array(
            'title' => utf8_encode($this->input->get_post('input_title', TRUE)),
            'shortstory' => utf8_encode($this->input->get_post('input_shortstory', TRUE)),
            'image' => $this->db->escape_str($this->input->get_post('input_image', TRUE)),
            'longstory' => utf8_encode($this->input->get_post('textarea_longstory', TRUE)),
            'author' => $this->user->id
        );
    }
    
    public function add_news() {
        if($this->db->insert('cms_news', $this->_prepare_input('add'))) {
            return TRUE;
        } else {
            $this->_last_error = 'MySQL-Error: ' . $this->db->_error_message();
            return FALSE;
        }
    }
    
    public function edit_news($id) {
        if($this->db->update('cms_news', $this->_prepare_input('edit'), array('id' => $id))) {
            return TRUE;
        } else {
            $this->_last_error = 'MySQL-Error: ' . $this->db->_error_message();
            return FALSE;
        }
    }
    
    public function delete_news($id) {
        if(count($this->db->get_where('cms_news', array('id' => $id))->result_array()) > 0) {
            $this->db->where('id', $id);
            if($this->db->delete('cms_news')) {
                return TRUE;
            } else {
                $this->_last_error = 'MYSQL-Error: ' . $this->db->_error_message();
                return FALSE;
            }
        } else {
            $this->_last_error = $this->lang->line('hotel_admin_news_delete_no_news_found');
            return FALSE;
        }
    }
    
    public function get_news($limit) {
        $this->db->limit($limit);
        $this->db->order_by('published', 'DESC');
        $result = $this->db->get('cms_news');
        
        return $result->result_array();
    }
    
    public function get_dategrouped_news($limit) {
        $news = $this->get_news($limit);
        $grouped = array();
        foreach($news As $row) {
            $grouped[date($this->config->item('hotel_default_date'), $row['published'])][] = $row;
        }
        
        return $grouped;
    }
    
    public function get_by_id($id) {
        if( ! is_numeric($id) || $id == 0) {
            return null;
        }
        
        $this->db->where('id', $id);
        $this->db->limit(1);
        $result = $this->db->get('cms_news');
        $array = $result->result_array();
        
        if(count($array) == 0) {
            return null;
        }
        
        return $array[0];
    }
    
    public function get_latest_news() {
        $this->db->limit(1);
        $this->db->order_by('published', 'DESC');
        $result = $this->db->get('cms_news');
        $array = $result->result_array();
        
        if(count($array) == 0) {
            return null;
        }
        
        return $array[0];
    }
    
    public function get_last_comment($user_id) {
        $this->db->where('user_id', $user_id);
        $this->db->limit(1);
        $result = $this->db->order_by('timestamp', 'desc')->get('cms_news_comments')->result_array();
        
        if(count($result) == 0)
            return null;
        
        return $result[0];
   }
    
    public function get_news_comments($news_id, $page = 0) {
        if($page > 0)
            $this->db->limit($this->config->item('news_comments_per_page'), ($page * $this->config->item('news_comments_per_page')) - $this->config->item('news_comments_per_page'));
        
        $this->db->order_by('timestamp', 'desc');
        $result = $this->db->where('news_id', $news_id)->get('cms_news_comments')->result_array();
        
        if(count($result) == 0)
            return null;
        
        return $result;
    }
    
    public function get_news_comments_count($news_id) {
        return count($this->db->where('news_id', $news_id)->get('cms_news_comments')->result_array());
    }
    
    public function save_comment($id) {
        $message = $this->input->get_post('textarea_comment', TRUE);
        $data = array(
            'news_id' => $id,
            'user_id' => $this->user->id,
            'message' => $message,
            'timestamp' => time()
        );
        
        if( ! $this->db->insert('cms_news_comments', $data)) {
            $this->_last_error = $this->db->_error_message();
            return FALSE;
        }
        
        return TRUE;
    }
}

?>
