<?php

/* * *******************************************\
  |****************** OPENCMS ******************|
  |*********************************************|
  |* @author Yannici                           *|
  |* @copyright Yannici                        *|
  |*********************************************|
  |* @since 04.11.2013                         *|
  \******************************************** */

class Update_model extends CI_Model {

    private $_last_error;
    private $_update_content;

    public function __construct() {
        parent::__construct();
    }

    public function get_last_error() {
        return $this->_last_error;
    }

    private function _get_proxy_settings() {
        $auth = base64_encode($this->config->item('update_proxy_username') . ':' . $this->config->item('update_proxy_password'));

        return ($this->config->item('update_proxy_username') != '') ? array(
            'http' => array(
                'proxy' => $this->config->item('update_proxy'),
                'request_fulluri' => true,
                'header' => "Proxy-Authorization: Basic $auth",
            )
                ) : array(
            'http' => array(
                'proxy' => $this->config->item('update_proxy'),
                'request_fulluri' => true
            )
                );
    }

    private function handle_error($errno, $errstr, $errfile, $errline, array $errcontext) {
        if (0 === error_reporting() || null === error_reporting()) {
            return false;
        }

        throw new ErrorException($errstr, 0, $errno, $errfile, $errline);
    }

    public function update_available() {
        set_error_handler(array($this, 'handle_error'));

        try {
            if ($this->config->item('update_proxy') != '') {
                $cxContext = stream_context_create($this->_get_proxy_settings());
                $this->_update_content = file_get_contents('http://yannici.bplaced.net/version.txt', FALSE, $cxContext);
            } else {
                $this->_update_content = file_get_contents('http://yannici.bplaced.net/version.txt');
            }

            if (strlen($this->_update_content) == 0 || $this->_update_content == null) {
                return FALSE;
            }

            $a = explode(';', $this->_update_content);

            return version_compare(VERSION, $a[1], '<');
        } catch (ErrorException $ex) {
            $this->_last_error = $this->lang->line('hotel_update_service_unavailable');
            return FALSE;
        }
    }

    public function get_update_link() {
        $link = explode(';', $this->_update_content);
        return $link[0];
    }

    public function get_latest_version() {
        $version = explode(';', $this->_update_content);
        return $version[1];
    }

    public function get_version() {
        return VERSION;
    }

}

?>