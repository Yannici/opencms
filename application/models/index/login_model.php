<?php
/*********************************************\
|****************** OPENCMS ******************|
|*********************************************|
|* @author Yannici                           *|
|* @copyright Yannici                        *|
|*********************************************|
|* @since 04.11.2013                         *|
\*********************************************/

class Login_model extends CI_Model {
    
    public function __construct() {
        parent::__construct();
    }
    
    private function _login() {
        $this->user->set_user_by_username($this->input->get_post('login_username', TRUE));
        $this->session->set_userdata(array(
            'username' => $this->user->username, 
            'password' => $this->user->password,
            'userid' => $this->user->id));
    }
    
    private function _admin_login() {
        $this->session->set_userdata(array('admin' => TRUE, 'security' => $this->input->get_post('adminlogin_security', TRUE)));
    }
     
   public function check_admin_login($username, $password) {
        if(!$this->user->logged_in()) {
            return FALSE;
        }
        
        $this->db->where(array(
            'LOWER(username)' => strtolower($username), 
            'password' => $this->core_model->open_hash($password, $username),
            'rank >=' => $this->config->item('admin_min_rank')));
        $this->db->from('users');
        
        if ($this->db->count_all_results() == 0) {
            $this->form_validation->set_message('check_adminlogin', $this->lang->line('hotel_error_message_check_login', $this->lang->line('hotel_username'), $this->lang->line('hotel_password')));
            return FALSE;
        }
        
        $this->db->flush_cache();
        $this->_admin_login($username, $password);
        return TRUE;
    }
    
    public function check_login($username, $password) {
        $this->db->where(array(
            'LOWER(username)' => strtolower($username), 
            'password' => $this->core_model->open_hash($password, $username)));
        $this->db->from('users');
        
        if ($this->db->count_all_results() == 0) {
            $this->form_validation->set_message('check_login', $this->lang->line('hotel_error_message_check_login', $this->lang->line('hotel_username'), $this->lang->line('hotel_password')));
            return FALSE;
        }
        $this->db->flush_cache();
        
        $this->db->where(array('LOWER(value)' => strtolower($username), 'expire >=' => time(), 'bantype' => 'user'));
        $bans = $this->db->get('bans')->result_array();
        $bans_ip = $this->db->get_where('bans', array('LOWER(value)' => $this->input->ip_address(), 'expire >=' => time(), 'bantype' => 'ip'))->result_array();
        
        if(count($bans) > 0 || count($bans_ip) > 0) {
            $bans = (count($bans) == 0) ? $bans_ip : $bans;
            $this->form_validation->set_message('check_login', $this->lang->line('hotel_error_message_check_login_bans', $bans[0]['reason'], date($this->config->item('hotel_default_datetime'), $bans[0]['expire'])));
            return FALSE;
        }
        
        $this->_login();
        return TRUE;
    }
    
    public function initialize_adminlogin() {
        /*
         * Adminlogin-Form
         */
        $data = array();
        $data['adminlogin_username'] = array(
            'placeholder' => $this->lang->line('hotel_username'),
            'name' => 'adminlogin_username',
            'class' => 'input-block-level'
        );
        
         $data['adminlogin_password'] = array(
            'placeholder' => $this->lang->line('hotel_password'),
            'name' => 'adminlogin_password',
            'class' => 'input-block-level'
        );
         
         $data['adminlogin_security'] = array(
            'placeholder' => $this->lang->line('hotel_admin_login_security_code'),
            'name' => 'adminlogin_security',
            'class' => 'input-block-level'
        );
         
         return $data;
    }
    
    public function initialize_login() {
        /*
         * Login-Form
         */
        $data = array();
        $data['login_username'] = array(
            'placeholder' => $this->lang->line('hotel_username'),
            'name' => 'login_username'
        );
        
         $data['login_password'] = array(
            'placeholder' => $this->lang->line('hotel_password'),
            'name' => 'login_password'
        );
         
         return $data;
    }
}
?>
