<?php
/*********************************************\
|****************** OPENCMS ******************|
|*********************************************|
|* @author Yannici                           *|
|* @copyright Yannici                        *|
|*********************************************|
|* @since 04.11.2013                         *|
\*********************************************/


class Register_model extends CI_Model {
    
    public $username;
    public $password;
    public $email;
    
    public $register_error;
    
    public function __construct() {
        parent::__construct();
    }
    
    public function register() {
        $data = array(
            'username' => $this->username,
            'password' => $this->core_model->open_hash($this->password, $this->username),
            'mail' => $this->email,
            'account_created' => time(),
            'last_online' => time(),
            'ip_last' => $this->core_model->get_ip(),
            'ip_reg' => $this->core_model->get_ip()
        );
        
        if(!$this->db->insert('users', $data)) {
            $this->form_validation->set_message('register_username', 'MYSQLi-ERROR: ' . $this->db->_error_message());
            return false;
        } else {
            $this->user->set_user_by_username($data['username']);
            
            $data_info = array(
                'user_id' => $this->user->id,
                'reg_timestamp' => time()
            );
            
            $data_stats = array(
                'id' => $this->user->id
            );
            
            $this->db->insert('user_info', $data_info);
            $this->db->insert('user_stats', $data_stats);
            
            $this->session->set_userdata(array('username' => $data['username'], 
                'password' => $data['password'], 
                'user' => $this->user,
                'userid' => $this->user->id));
            
            return true;
        }
    }
    
    public function check_username($username) {
        // CHECKINGS
        $filter = preg_replace('/[^a-z\d\-=\?!@:\.]/i', '', $username);
        $ip_check = count($this->db->select('ip_last,ip_reg')->from('users')->where('ip_last', $this->core_model->get_ip())->or_where('ip_reg', $this->core_model->get_ip())->limit(3)->get()->result_array());
        $bans_ip = $this->db->get_where('bans', array('LOWER(value)' => $this->input->ip_address(), 'expire >=' => time(), 'bantype' => 'ip'))->result_array();
        $first = strtoupper(substr($username, 0, 4));
        
        if($ip_check >= 3) {
            $this->form_validation->set_message('correct_username', $this->lang->line('hotel_error_message_ip_check_failed'));
            return false;
        }
        
        if($first == 'MOD-' || $first == 'HEAD-') {
            $this->form_validation->set_message('correct_username', $this->lang->line('hotel_error_message_username_incorrect'));
            return false;
        }
        
        if($filter !== $username) {
            $this->form_validation->set_message('correct_username', $this->lang->line('hotel_error_message_username_incorrect'));
            return false;
        }
        
        if(count($bans_ip) > 0) {
            $this->form_validation->set_message('correct_username', $this->lang->line('hotel_error_message_check_login_bans', $bans_ip[0]['reason'], (is_numeric($bans_ip[0]['expire'])) ? date($this->config->item('hotel_default_datetime'), $bans_ip[0]['expire']) : $bans_ip[0]['expire']));
            return false;
        }
        
        return true;
    }
    
    public function initialize_register() {
        $data = array();
        
         /*
          * Register-Form
          */
         $style = 'margin-top:5px;width:90%;';
         $data['register_username'] = array(
            'style' => $style,
            'name' => 'register_username',
            'maxlength' => '12'
        );
         
         $data['register_password'] = array(
            'style' => $style,
            'name' => 'register_password',
            'maxlength' => '30'
        );
         
         $data['register_password_wdh'] = array(
            'style' => $style,
            'name' => 'register_password_wdh',
            'maxlength' => '30'
        );
         
         $data['register_email'] = array(
            'style' => $style,
            'name' => 'register_email',
            'maxlength' => '100'
        );
         
         $data['register_captcha'] = array(
            'style' => $style,
            'name' => 'register_captcha',
            'maxlength' => '30'
        );
         
        return $data;
    }
}
