<?php
/*********************************************\
|****************** OPENCMS ******************|
|*********************************************|
|* @author Yannici                           *|
|* @copyright Yannici                        *|
|*********************************************|
|* @since 04.11.2013                         *|
\*********************************************/


class Register_model extends CI_Model {
    
    public $username;
    public $password;
    public $email;
    
    public $register_error;
    
    public function __construct() {
        parent::__construct();
    }
    
    public function register() {
        $data = array(
            'username' => $this->username,
            'password' => $this->core_model->open_hash($this->password, $this->username),
            'mail' => $this->email,
            'account_created' => time(),
            'last_online' => time(),
            'ip_last' => $this->core_model->get_ip(),
            'ip_reg' => $this->core_model->get_ip(),
            'credits' => $this->config->item('register_credits'),
            'activity_points' => $this->config->item('register_pixels'),
            'vip_points' => $this->config->item('register_vip_points')
        );
        
        if(!$this->db->insert('users', $data)) {
            $this->register_error = $this->db->_error_message();
            return false;
        } else {
            $this->user->set_user_by_username($data['username']);
            $this->session->set_userdata(array('username' => $data['username'], 
                'password' => $data['password'], 
                'user' => $this->user,
                'userid' => $this->user->id));
            return true;
        }
    }
}
