<?php
/*********************************************\
|****************** OPENCMS ******************|
|*********************************************|
|* @author Yannici                           *|
|* @copyright Yannici                        *|
|*********************************************|
|* @since 04.11.2013                         *|
\*********************************************/

class Tabledata_model extends CI_Model {
    
    public function __construct() {
        parent::__construct();
    }
    
    public function get_users($filter = '') {
        $this->load->helper('form');
        
        $columns = array('id', 'username', 'mail', 'rank', 'ip_last', 'ip_reg', 'last_online', 'online');
        $table = 'users';
        
        $output = $this->_get_all_data($columns, $table, $filter);
        $columns[] = 'actions';
        
        foreach ($output['rResult']->result_array() As $aRow) {
            $row = array();
            
            // Edit data individually
            $aRow['last_online'] = date($this->config->item('hotel_default_datetime'), $aRow['last_online']);
            $aRow['online'] = ($aRow['online'] == '1') ? '<img src="' . $this->config->item('path') . '/data/img/online.gif" />' : '<img src="' . $this->config->item('path') . '/data/img/offline.gif" />';
            
            // Adding Button-Column
            // This was made as a Form, because of security risks (CSRF)
            $aRow['actions'] = utf8_encode(form_open('openadmin/useredit/edit/' . $aRow['id'], array('style' => 'float:left;margin:0;margin-right:5px;')) . '<button type="submit" class="btn btn-warning"><i class="icon-edit"></i></button> ' . form_close() .
                     form_open('openadmin/members/delete/' . $aRow['id'], array('style' => 'float:left;width:25px;height:15px;margin-right:5px;')) . '<button onclick="return confirm(\'' . $this->lang->line('hotel_admin_delete_confirmation') . '\');" type="submit" class="btn btn-danger"><i class="icon-trash"></i></button>' . form_close());
            
            foreach ($columns as $col) {
                $row[] = $aRow[$col];
            }
            
            $output['aaData'][] = $row;
        }
        
        unset($output['rResult']);
        echo json_encode($output);
    }
    
    public function get_chatlogs($type = 'user', $searchString = 0, $sFilter = '') {
        $this->load->model('openadmin/chatlogs_model');
        
        if(!$this->chatlogs_model->check_input($type, $searchString)) {
            exit;
        }

        $aColumns = array('id', 'room_id', 'timestamp', 'user_id', 'user_name', 'message');
        $sTable = 'chatlogs';
        
        $iDisplayStart = $this->input->get_post('iDisplayStart', TRUE);
        $iDisplayLength = $this->input->get_post('iDisplayLength', TRUE);
        $iSortCol_0 = $this->input->get_post('iSortCol_0', TRUE);
        $iSortingCols = $this->input->get_post('iSortingCols', TRUE);
        $sEcho = $this->input->get_post('sEcho', TRUE);
        $sSearch = ($sFilter !== '' && !empty($sFilter)) ? $sFilter : $this->input->get_post('sSearch', TRUE);

        if (isset($iDisplayStart) && $iDisplayLength != '-1') {
            $this->db->limit($this->db->escape_str($iDisplayLength), $this->db->escape_str($iDisplayStart));
        }

        if (isset($iSortCol_0)) {
            $this->_sorting($iSortingCols, $aColumns);
        }

        if (isset($sSearch) && !empty($sSearch)) {
            $this->_search($sSearch, $aColumns);
        }
        
        if($type == 'user') {
            $this->db->where('user_id', $this->db->escape_like_str($searchString));
            $this->db->or_where('user_name LIKE', '%' . $this->db->escape_like_str($searchString) . '%');
        } elseif($type == 'rooms') {
            $this->db->where('room_id', $this->db->escape_like_str($searchString));
        }
        
        $this->db->select('SQL_CALC_FOUND_ROWS ' .str_replace(' , ', ' ', implode(', ', $aColumns)), FALSE);
        $this->db->from($sTable);
        $rResult = $this->db->get();
        $result = $rResult->result_array();
        
        $this->db->select('FOUND_ROWS() AS found_rows');
        $iFilteredRecords = $this->db->get()->row()->found_rows;
    
        $iTotal = $this->db->count_all($sTable);
        
        $output = array(
            'sEcho' => intval($sEcho),
            'iTotalRecords' => $iTotal,
            'iTotalDisplayRecords' => $iFilteredRecords,
            'aaData' => array()
        );
        
        foreach ($result As $aRow) {
            $row = array();
            
            // Edit data individually
            $aRow['message'] = htmlentities($aRow['message'], ENT_COMPAT | ENT_HTML401, 'ISO-8859-1');
            $aRow['timestamp'] = date($this->config->item('hotel_default_datetimesecond'), $aRow['timestamp']);
            $room = $this->db->select('caption')->from('rooms')->where('id', $aRow['room_id'])->limit(1)->get()->result_array();
            $aRow['room_id'] = (count($room) == 0) ? $this->lang->line('hotel_admin_chatlogs_no_room_found') : $room[0]['caption'];
            
            foreach ($aColumns as $col) {
                $row[] = $aRow[$col];
            }
            
            $output['aaData'][] = $row;
        }
        
        echo json_encode($output);
    }
    
    public function get_cmdlogs($type = 'user', $searchString = 0, $sFilter = '') {
        $this->load->model('openadmin/cmdlogs_model');
        
        if(!$this->cmdlogs_model->check_input($type, $searchString)) {
            exit;
        }

        $aColumns = array('id', 'timestamp', 'user_id', 'user_name', 'command', 'extra_data');
        $sTable = 'cmdlogs';
        
        $iDisplayStart = $this->input->get_post('iDisplayStart', TRUE);
        $iDisplayLength = $this->input->get_post('iDisplayLength', TRUE);
        $iSortCol_0 = $this->input->get_post('iSortCol_0', TRUE);
        $iSortingCols = $this->input->get_post('iSortingCols', TRUE);
        $sEcho = $this->input->get_post('sEcho', TRUE);
        $sSearch = ($sFilter !== '' && !empty($sFilter)) ? $sFilter : $this->input->get_post('sSearch', TRUE);

        if (isset($iDisplayStart) && $iDisplayLength != '-1') {
            $this->db->limit($this->db->escape_str($iDisplayLength), $this->db->escape_str($iDisplayStart));
        }

        if (isset($iSortCol_0)) {
            $this->_sorting($iSortingCols, $aColumns);
        }

        if (isset($sSearch) && !empty($sSearch)) {
            $this->_search($sSearch, $aColumns);
        }
        
        if($type == 'user') {
            $this->db->where('user_id', $this->db->escape_like_str($searchString));
            $this->db->or_where('user_name LIKE', '%' . $this->db->escape_like_str($searchString) . '%');
        } elseif($type == 'command') {
            $this->db->where('command', $this->db->escape_like_str($searchString));
        }
        
        $this->db->select('SQL_CALC_FOUND_ROWS ' .str_replace(' , ', ' ', implode(', ', $aColumns)), FALSE);
        $this->db->from($sTable);
        $rResult = $this->db->get();
        $result = $rResult->result_array();
        
        $this->db->select('FOUND_ROWS() AS found_rows');
        $iFilteredRecords = $this->db->get()->row()->found_rows;
    
        $iTotal = $this->db->count_all($sTable);
        
        $output = array(
            'sEcho' => intval($sEcho),
            'iTotalRecords' => $iTotal,
            'iTotalDisplayRecords' => $iFilteredRecords,
            'aaData' => array()
        );
        
        foreach ($result As $aRow) {
            $row = array();
            
            // Edit data individually
            $aRow['extra_data'] = htmlentities($aRow['extra_data'], ENT_COMPAT | ENT_HTML401, 'ISO-8859-1');
            $aRow['timestamp'] = date($this->config->item('hotel_default_datetimesecond'), $aRow['timestamp']);
                        
            foreach ($aColumns as $col) {
                $row[] = $aRow[$col];
            }
            
            $output['aaData'][] = $row;
        }
        
        echo json_encode($output);
    }
    
    private function _sorting($sortingCols, $columns) {
        for($i=0; $i<intval($sortingCols); $i++)
            {
                $iSortCol = $this->input->get_post('iSortCol_'.$i, true);
                $bSortable = $this->input->get_post('bSortable_'.intval($iSortCol), true);
                $sSortDir = $this->input->get_post('sSortDir_'.$i, true);
    
                if($bSortable == 'true')
                {
                    $this->db->order_by($columns[intval($this->db->escape_str($iSortCol))], $this->db->escape_str($sSortDir));
                }
            }
    }
    
    private function _search($search, $columns) {
        for ($i = 0; $i < count($columns); $i++) {
            $bSearchable = $this->input->get_post('bSearchable_' . $i, TRUE);

            if (isset($bSearchable) && $bSearchable == 'true') {
                $this->db->or_like($columns[$i], $this->db->escape_like_str($search));
            }
        }
    }
    
    private function _get_all_data($aColumns, $sTable, $sFilter = '') {
        $iDisplayStart = $this->input->get_post('iDisplayStart', TRUE);
        $iDisplayLength = $this->input->get_post('iDisplayLength', TRUE);
        $iSortCol_0 = $this->input->get_post('iSortCol_0', TRUE);
        $iSortingCols = $this->input->get_post('iSortingCols', TRUE);
        $sSearch = ($sFilter !== '' && !empty($sFilter)) ? $sFilter : $this->input->get_post('sSearch', TRUE);
        $sEcho = $this->db->escape_str($this->input->get_post('sEcho', TRUE));

        if (isset($iDisplayStart) && $iDisplayLength != '-1') {
            $this->db->limit($this->db->escape_str($iDisplayLength), $this->db->escape_str($iDisplayStart));
        }

        if (isset($iSortCol_0)) {
            $this->_sorting($iSortingCols, $aColumns);
        }

        if (isset($sSearch) && !empty($sSearch)) {
            $this->_search($sSearch, $aColumns);
        }
        
        $this->db->select('SQL_CALC_FOUND_ROWS ' . str_replace(' , ', ' ', implode(', ', $aColumns)), FALSE);
        $this->db->from($sTable);
        $rResult = $this->db->get();
        
        $this->db->select('FOUND_ROWS() AS found_rows');
        $iFilteredRecords = $this->db->get()->row()->found_rows;
    
        $iTotal = $this->db->count_all($sTable);
        
        $output = array(
            'sEcho' => intval($sEcho),
            'iTotalRecords' => $iTotal,
            'iTotalDisplayRecords' => $iFilteredRecords,
            'rResult' => $rResult,
            'aaData' => array()
        );
        
        return $output;
    }
    
    public function get_news() {
        $this->load->helper('form');
        
        $columns = array(
            'id',
            'title',
            'shortstory',
            'author',
            'published',
            'image'
        );
        
        $table = 'cms_news';
        
        $output = $this->_get_all_data($columns, $table);
        $columns[] = 'actions';
        
        foreach($output['rResult']->result_array() As $aRow) {
            $row = array();
            
            $aRow['author'] = User::get_username_by_id($aRow['author']);
            $aRow['published'] = date($this->config->item('hotel_default_datetime'), $aRow['published']);
            
            // Adding Button-Column
            // This was made as a Form, because of security risks (CSRF)
            $aRow['actions'] = utf8_encode(form_open('openadmin/news/edit/' . $aRow['id'], array('style' => 'float:left;margin:0;margin-right:5px;')) . '<button type="submit" class="btn btn-warning"><i class="icon-edit"></i></button> ' . form_close() .
                     form_open('openadmin/news/delete/' . $aRow['id'], array('style' => 'float:left;margin:0;')) . '<button type="submit" class="btn btn-danger"><i class="icon-trash"></i></button>' . form_close());
            
            foreach ($columns As $col) {
                $row[] = $aRow[$col];
            }
            
            $output['aaData'][] = $row;
        }
        
        unset($output['rResult']);
        echo json_encode($output);
    }
}
?>