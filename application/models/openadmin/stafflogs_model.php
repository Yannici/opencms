<?php
/*********************************************\
|****************** OPENCMS ******************|
|*********************************************|
|* @author Yannici                           *|
|* @copyright Yannici                        *|
|*********************************************|
|* @since 04.11.2013                         *|
\*********************************************/

class Stafflogs_model extends CI_Model {
    
    public function __construct() {
        parent::__construct();
    }
    
    public function new_stafflog($message, $user_id) {
        return ($this->db->insert('cms_stafflogs', array('message' => utf8_encode($message), 'user_id' => $user_id, 'stamp' => time())));
    }
}

?>
