<?php
/*********************************************\
|****************** OPENCMS ******************|
|*********************************************|
|* @author Yannici                           *|
|* @copyright Yannici                        *|
|*********************************************|
|* @since 04.11.2013                         *|
\*********************************************/

class Dashboard_model extends CI_Model {
    
    public function __construct() {
        parent::__construct();
    }
    
    public function get_last_online_users() {
        $users = array();
        $result = $this->db->select('username, ip_last, last_online, online')->from('users')->order_by('last_online', 'DESC')->limit(10)->get()->result_array();
        foreach($result As $key => $row) {
            $row['online'] = ($row['online'] == '1') ? '<img src="' . $this->config->item('path') . '/data/img/online.gif" />' : '<img src="' . $this->config->item('path') . '/data/img/offline.gif" />';
            $row['last_online'] = date($this->config->item('hotel_default_datetime'), $row['last_online']);
            $users[] = $row;
        }
        
        return $users;
    }
    
    public function get_last_staff_activities() {
        $activities = array();
        $result = $this->db->select('user_id, message, stamp')->from('cms_stafflogs')->order_by('stamp', 'DESC')->limit(10)->get()->result_array();
        foreach($result As $row) {
            $row['stamp'] = date($this->config->item('hotel_default_datetime'), $row['stamp']);
            $row['username'] = User::get_username_by_id($row['user_id']);
            $row['message'] = utf8_decode($row['message']);
            $activities[] = $row;
        }
        
        return $activities;
    }
}

?>
