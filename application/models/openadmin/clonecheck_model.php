<?php
/*********************************************\
|****************** OPENCMS ******************|
|*********************************************|
|* @author Yannici                           *|
|* @copyright Yannici                        *|
|*********************************************|
|* @since 04.11.2013                         *|
\*********************************************/

class Clonecheck_model extends CI_Model {
    
    private $_last_error = null;
    
    public function __construct() {
        parent::__construct();
    }
    
    private function _get_clones_by_ip($ip) {
        $result = $this->db->where('ip_last', $ip)->or_where('ip_reg', $ip)->get('users')->result_array();
        
        if(count($result) == 0) {
            return null;
        }
        
        return $result;
    }
    
    private function _get_clones_by_user($user) {
        $this->db->like('username', $user->username);
        $this->db->or_like('mail', $user->mail);
        $this->db->or_like('ip_last', $user->ip_last, 'none');
        $this->db->or_like('ip_reg', $user->ip_reg, 'none');
        $result = $this->db->get('users')->result_array();
        
        if(count($result) == 0) {
            return null;
        }
        
        return $result;
    }
    
    public function check_for_clones($input, $type = 'user') {
        $this->load->model('user_model');
        
        if(trim($input) == '') {
            $this->_last_error = 'Du musst schon etwas zum Suchen eingeben!';
            return FALSE;
        }
        
        switch ($type) {
            case 'ip':
                return $this->_get_clones_by_ip($input);
                break;
            case 'user':
                $user = $this->user_model->search_user($input);
                if($user == null) {
                    $this->_last_error = 'Kein User mit diesem Usernamen / User-ID gefunden!';
                    return FALSE;
                }
                return $this->_get_clones_by_user($user);
                break;
            default:
                $this->_last_error = 'Such-Typ ist fehlerhaft!';
                return FALSE;
                break;
        }
    }
    
    public function get_last_error() {
        return $this->_last_error;
    }
}

?>
