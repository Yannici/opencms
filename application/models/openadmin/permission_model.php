<?php

/* * *******************************************\
  |****************** OPENCMS ******************|
  |*********************************************|
  |* @author Yannici                           *|
  |* @copyright Yannici                        *|
  |*********************************************|
  |* @since 04.11.2013                         *|
  \******************************************** */

class Permission_model extends CI_Model {

    private $_permission_sites = array();
    private $_last_error = null;

    public function __construct() {
        parent::__construct();
        $this->_load_permission_sites();
    }

    private function _load_permission_sites() {
        $nav = $this->config->item('hotel_admin_navigation');
        foreach ($nav As $lang => $forward) {
            if ( ! is_array($forward)) {
                $this->_permission_sites[] = $forward;
                continue;
            }

            foreach ($forward As $f => $lang) {
                $this->_permission_sites[] = $f;
            }
        }
    }

    public function get_permission_sites() {
        return $this->_permission_sites;
    }

    public function have_permission($rank, $site) {
        $permission = count($this->db->where(array('rank' => $rank, 'site' => $site))->get('cms_permission')->result_array());
    
        return ($permission > 0);
    }
    
    public function save_permissions($rank, $p) {
        $this->db->delete('cms_permission', array('rank' => $rank));
        $data = array(
            'rank' => $rank
        );
        
        foreach($p As $site => $v) {
            $data['site'] = $site;
            if( ! $this->db->insert('cms_permission', $data)) {
                $this->_last_error = $this->db->_error_message();
                return FALSE;
            }
        }
        
        return TRUE;
    }
    
    public function get_last_error() {
        return $this->_last_error;
    }
}
?>
