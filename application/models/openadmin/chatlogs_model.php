<?php
/*********************************************\
|****************** OPENCMS ******************|
|*********************************************|
|* @author Yannici                           *|
|* @copyright Yannici                        *|
|*********************************************|
|* @since 04.11.2013                         *|
\*********************************************/

class Chatlogs_model extends CI_Model {
    
    public function __construct() {
        parent::__construct();
    }
    
    public function check_input($d_type, $s_input, $FORM = FALSE) {
        if($d_type !== 'user' && $d_type !== 'rooms') {
            if($FORM) $this->form_validation->set_message('check_chatlog_input', $this->lang->line('hotel_admin_error_chatlogs_wrong_type'));
            return FALSE;
        }
        
        if((!is_int($s_input) && !is_string($s_input)) || empty($s_input)) {
            if($FORM) $this->form_validation->set_message('check_chatlog_input', $this->lang->line('hotel_admin_error_chatlogs_wrong_input'));
            return FALSE;
        }
        
        return TRUE;
    }
}

?>
