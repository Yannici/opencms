<?php
/*********************************************\
|****************** OPENCMS ******************|
|*********************************************|
|* @author Yannici                           *|
|* @copyright Yannici                        *|
|*********************************************|
|* @since 04.11.2013                         *|
\*********************************************/

class Profile_model extends CI_Model {

    public function __construct() {
        parent::__construct();
    }

    public function save_settings() {
        $this->form_validation->set_rules('settings_email', $this->lang->line('hotel_email'), 'required|valid_email|xss_clean|trim|max_length[100]');
        $this->form_validation->set_rules('settings_volume', $this->lang->line('hotel_profile_settings_volume'), 'is_numeric');
        $this->form_validation->set_rules('settings_volume', $this->lang->line('hotel_profile_settings_volume'), 'max_length[50]|xss_clean');
        $block_newfriends = ($this->input->get_post('settings_block_newfriends') && $this->input->get_post('settings_block_newfriends') == 'set') ? '0' : '1';
        $hide_online = ($this->input->get_post('settings_hide_online') && $this->input->get_post('settings_hide_online') == 'set') ? '1' : '0';
        $accept_trading = ($this->input->get_post('settings_trading') && $this->input->get_post('settings_trading') == 'set') ? '1' : '0';

        if ($this->form_validation->run()) {
            $this->user->mail = $this->input->get_post('settings_email', TRUE);
            $this->user->volume = $this->input->get_post('settings_volume', TRUE);
            $this->user->block_newfriends = $block_newfriends;
            $this->user->hide_online = $hide_online;
            $this->user->motto = $this->input->get_post('settings_motto', TRUE);
            $this->user->accept_trading = $accept_trading;

            return $this->user->store();
        } else {
            return FALSE;
        }
    }

    public function save_password() {
        $this->form_validation->set_rules('input_password', $this->lang->line('hotel_settings_newpassword_active_password'), 'required|min_length[5]|max_length[100]|callback_is_active_password');
        $this->form_validation->set_rules('input_newpassword', $this->lang->line('hotel_settings_newpassword_new_password'), 'required|max_length[100]|min_length[5]');
        $this->form_validation->set_rules('input_newpassword_wdh', $this->lang->line('hotel_settings_newpassword_new_password_wdh'), 'required|min_length[5]|max_length[100]|matches[input_newpassword]');
   
        if($this->form_validation->run() === TRUE) {
            return $this->user->edit_password($this->input->get_post('input_newpassword', TRUE));
        } else {
            return FALSE;
        }
    }
}

?>
