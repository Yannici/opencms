<?php
/* * *******************************************\
  |****************** OPENCMS ******************|
  |*********************************************|
  |* @author Yannici                           *|
  |* @copyright Yannici                        *|
  |*********************************************|
  |* @since 04.11.2013                         *|
  \******************************************** */
?>

<!DOCTYPE html>
<html class="no-js">

    <head>
        <meta charset="ISO-8859-1">
        <meta name="viewport" content="width=device-width; initial-scale=1.0; maximum-scale=1.0; user-scalable=0;" />
        <title><?php echo $title; ?> - <?php echo $this->lang->line('hotel_admin_name'); ?> - <?php echo $this->config->item('hotel_name'); ?></title>
        <!-- Bootstrap -->
        <link href="<?php echo $PATH; ?>/data-admin/bootstrap/css/bootstrap.css" rel="stylesheet" media="screen">
        <link href="<?php echo $PATH; ?>/data-admin/assets/font-awesome/css/font-awesome.min.css" rel="stylesheet" media="screen">
        <link href="<?php echo $PATH; ?>/data-admin/bootstrap/css/bootstrap-responsive.css" rel="stylesheet" media="screen">
        <link href="<?php echo $PATH; ?>/data-admin/vendors/easypiechart/jquery.easy-pie-chart.css" rel="stylesheet" media="screen">
        <link rel="stylesheet" href="<?php echo $PATH; ?>/data/css/toggle-switch.css">
        <link href="<?php echo $PATH; ?>/data-admin/assets/styles.css" rel="stylesheet" media="screen">
        <link rel="shortcut icon" type="image/png" href="<?php echo $PATH; ?>/data/img/favicon.ico">
        <!-- HTML5 shim, for IE6-8 support of HTML5 elements -->
        <!--[if lt IE 9]>
            <script src="http://html5shim.googlecode.com/svn/trunk/html5.js"></script>
        <![endif]-->

        <link href="<?php echo $PATH; ?>/data-admin/vendors/jquery-ui.css" rel="stylesheet">
        <script type="text/javascript" src="<?php echo $PATH; ?>/data-admin/vendors/jquery-1.9.1.min.js"></script>
        <script type="text/javascript" src="<?php echo $PATH; ?>/data-admin/vendors/datatables/js/jquery.dataTables.min.js"></script>
        <script type="text/javascript" src="<?php echo $PATH; ?>/data-admin/vendors/datatables/js/jquery.dataTables.min.js"></script>
        <script type="text/javascript" src="<?php echo $PATH; ?>/data-admin/vendors/datatables/js/jquery.dataTables.delay.min.js"></script>
    </head>
    <body <?php echo (isset($LOGIN)) ? 'id="login"' : ''; ?>>
        <script type="text/javascript">
            $(document).ready(function() {
                $('input').tooltip();
            });
        </script>
        <?php if (!isset($LOGIN)): ?>
            <?php include('header-navigation.php'); ?>
        <?php endif; ?>
        <div class="container-fluid">
            <div class="row-fluid">
                <div class="span12" id="content">
                    <?php if (!isset($LOGIN)): ?>
                        <?php include('header-content.php'); ?>
                    <?php endif; ?>

                    <?php if (isset($FORM_ERROR)): ?>
                        <?php if (validation_errors() !== '' OR (is_string($FORM_ERROR) && $FORM_ERROR !== '')): ?>
                            <div class="row-fluid" id="lr_error">
                                <div class="span12">
                                    <div class="alert alert-error">
                                        <button type="button" class="close" data-dismiss="alert">&times;</button>
                                        <h4><?php echo $this->lang->line('hotel_error_message_error') ?></h4>
                                        <p><?php echo (is_string($FORM_ERROR) && $FORM_ERROR !== '') ? $FORM_ERROR : validation_errors(); ?></p>
                                    </div>
                                </div>
                            </div>
                        <?php endif; ?>
                    <?php endif; ?>

                    <?php if (isset($FORM_SUCCESS) && $FORM_SUCCESS !== ''): ?>
                        <div class="row-fluid" id="lr_error">
                            <div class="span12">
                                <div class="alert alert-success">
                                    <button type="button" class="close" data-dismiss="alert">&times;</button>
                                    <strong><?php echo $this->lang->line('hotel_error_message_success') ?></strong>
                                    <?php echo $FORM_SUCCESS; ?>
                                </div>
                            </div>
                        </div>
                    <?php endif; ?>