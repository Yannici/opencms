<div class="span3">
    <div class="panel panel-info">
        <div class="panel-heading">
            <div class="row">
                <div class="col-xs-6">
                    <i class="fa fa-user fa-5x"></i>
                </div>
                <div class="col-xs-6 text-right">
                    <p class="announcement-heading"><?php echo $USER_ONLINE; ?></p>
                    <p class="announcement-text"><?php echo $this->lang->line('hotel_admin_online_users'); ?></p>
                </div>
            </div>
        </div>
    </div>
</div>
<div class="span3">
    <div class="panel panel-success">
        <div class="panel-heading">
            <div class="row">
                <div class="col-xs-6">
                    <i class="fa fa-arrow-up fa-5x"></i>
                </div>
                <div class="col-xs-6 text-right">
                    <p class="announcement-heading"><?php echo $USER_RECORD; ?></p>
                    <p class="announcement-text"><?php echo $this->lang->line('hotel_admin_user_record'); ?></p>
                </div>
            </div>
        </div>
    </div>
</div>
<div class="span3">
    <div class="panel panel-danger">
        <div class="panel-heading">
            <div class="row">
                <div class="col-xs-6">
                    <i class="fa fa-list-ol fa-5x"></i>
                </div>
                <div class="col-xs-6 text-right">
                    <p class="announcement-heading"><?php echo $USER_TOTAL; ?></p>
                    <p class="announcement-text"><?php echo $this->lang->line('hotel_admin_user_total'); ?></p>
                </div>
            </div>
        </div>
    </div>
</div>
<div class="span3">
    <div class="panel panel-warning">
        <div class="panel-heading">
            <div class="row">
                <div class="col-xs-6">
                    <i class="fa fa-user-md fa-5x"></i>
                </div>
                <div class="col-xs-6 text-right">
                    <p class="announcement-heading"><?php echo $WORKERS_ONLINE; ?></p>
                    <p class="announcement-text"><?php echo $this->lang->line('hotel_admin_workers_online'); ?></p>
                </div>
            </div>
        </div>
    </div>
</div>