<?php
/* * *******************************************\
  |****************** OPENCMS ******************|
  |*********************************************|
  |* @author Yannici                           *|
  |* @copyright Yannici                        *|
  |*********************************************|
  |* @since 04.11.2013                         *|
  \******************************************** */
?>      
</div>
</div>
<hr>
<footer>
    <p>&copy; 2013 OpenCMS by <strong>Yannici</strong> - All rights reserved</p>
</footer>
</div>
<!--/.fluid-container-->

<link href="<?php echo $PATH; ?>/data-admin/vendors/wysiwyg/bootstrap-wysihtml5.css" rel="stylesheet" media="screen">
<script type="text/javascript" src="<?php echo $PATH; ?>/data-admin/vendors/wysiwyg/wysihtml5-0.3.0.js"></script>
<script type="text/javascript" src="<?php echo $PATH; ?>/data-admin/bootstrap/js/bootstrap.min.js"></script>
<script type="text/javascript" src="<?php echo $PATH; ?>/data-admin/bootstrap/js/jquery.dataTables.min.js"></script>
<script type="text/javascript" src="<?php echo $PATH; ?>/data-admin/bootstrap/js/jquery.dataTables.delay.min.js"></script>

<script type="text/javascript" src="<?php echo $PATH; ?>/data-admin/vendors/tinymce/js/tinymce/tinymce.min.js"></script>

<script type="text/javascript" src="<?php echo $PATH; ?>/data-admin/assets/scripts.js"></script>
</body>

</html>