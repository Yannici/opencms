<?php
/*********************************************\
|****************** OPENCMS ******************|
|*********************************************|
|* @author Yannici                           *|
|* @copyright Yannici                        *|
|*********************************************|
|* @since 04.11.2013                         *|
\*********************************************/
?>

<div class="navbar navbar-fixed-top">
    <div class="navbar-inner">
        <div class="container-fluid">
            <a class="btn btn-navbar" data-toggle="collapse" data-target=".nav-collapse"> <span class="icon-bar"></span>
                <span class="icon-bar"></span>
                <span class="icon-bar"></span>
            </a>
            <a class="brand" href="#"><?php echo $this->lang->line('hotel_admin_name'); ?></a>
            <div class="nav-collapse collapse">
                <ul class="nav pull-right">
                    <li class="dropdown">
                        <a href="#" role="button" class="dropdown-toggle" data-toggle="dropdown"> <i class="icon-user"></i> <?php echo $this->user->username; ?> <i class="caret"></i>

                        </a>
                        <ul class="dropdown-menu">
                            <li>
                                <a tabindex="-1" href="<?php echo $PATH; ?>/game"><?php echo $this->lang->line('hotel_admin_client'); ?></a>
                            </li>
                            <li class="divider"></li>
                            <li>
                                <a tabindex="-1" href="<?php echo $PATH; ?>/openadmin/logout"><?php echo $this->lang->line('hotel_admin_logout'); ?></a>
                            </li>
                        </ul>
                    </li>
                </ul>
                <ul class="nav">
                    <?php foreach ($this->config->item('hotel_admin_navigation') As $key => $value): ?>
                        <li <?php if (is_array($value)): ?>class="dropdown"<?php endif; ?>>
                            <?php $k = explode(';', $key); ?>
                            <a <?php if (is_array($value)): ?>href="#" data-toggle="dropdown" class="dropdown-toggle"<?php else: ?> href="<?php echo $PATH; ?>/openadmin/<?php echo $value; ?>"<?php endif; ?>><i class="<?php echo $k[1]; ?>"></i> <?php echo $this->core_model->get_string_language($k[0]); ?><?php if (is_array($value)): ?><b class="caret"></b><?php endif; ?></a>

                            <?php if (is_array($value)): ?>
                                <ul class="dropdown-menu">
                                    <?php foreach ($value As $subkey => $subvalue): ?>
                                        <li>
                                            <a href="<?php echo $PATH; ?>/openadmin/<?php echo $subkey; ?>"><?php echo $this->core_model->get_string_language($subvalue); ?></a>
                                        </li>
                                    <?php endforeach; ?>
                                </ul>
                            <?php endif; ?>
                        </li>
                    <?php endforeach; ?>
                </ul>
            </div>
            <!--/.nav-collapse -->
        </div>
    </div>
</div>