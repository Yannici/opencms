<?php
/*********************************************\
|****************** OPENCMS ******************|
|*********************************************|
|* @author Yannici                           *|
|* @copyright Yannici                        *|
|*********************************************|
|* @since 04.11.2013                         *|
\*********************************************/
?>
<div class="row">
    <div class="span12-hero">
        <div class="box">
            <div class="header red">
                <?php echo $this->lang->line('hotel_community_employee'); ?>
            </div>
            <div class="content">
                <?php foreach ($employee As $group => $users): ?>
                    <script type="text/javascript">
                        $(document).ready(function() {
                            $('.info').fancybox({
                                minWidth        : 550,
                                maxWidth	: 550,
                                maxHeight	: 600,
                                fitToView	: true,
                                autoSize	: true,
                                closeClick	: true,
                                openEffect	: 'elastic',
                                closeEffect	: 'elastic'
                            });
                        });
                    </script>
                    <?php if (count($users) > 0): ?>
                        <div style="display:none;" id="<?php echo utf8_decode($group); ?>"><h3><?php echo utf8_decode($group); ?></h3><?php echo utf8_decode($this->employee_model->get_group_description($group)); ?></div>
                        <span class="employee-group-headline"><?php echo utf8_decode($group); ?></span> <a href="#<?php echo utf8_decode($group); ?>" class="info"><?php echo $this->lang->line('hotel_plugin_employee_information'); ?></a>
                        <hr style="margin:0;margin-bottom: 10px;border-color:#ccc;" />
                        <div class="employee">
                            <?php foreach ($users As $user): ?>
                                <div class="employee-user">
                                    <div class="half-avatar"><img style="margin-top:-20px;" src="http://www.habbo.de/habbo-imaging/avatarimage?figure=<?php echo $user['look']; ?>&amp;direction=4&amp;head_direction=3&amp;gesture=sml&amp;action=wlk&amp;size=l" /></div>
                                    <div class="employee-details">
                                        <span class="employee-online"><img src="<?php echo $PATH; ?>/data/img/<?php echo ($user['online'] == '0') ? 'offline.gif' : 'online.gif'; ?>" /></span>
                                        <span class="username"><?php echo $user['username']; ?></span><br />
                                        <?php echo $user['motto']; ?><br />
                                        <?php echo $this->lang->line('hotel_plugin_employee_registered') . ' ' . $this->core_model->pretty_date($user['account_created']); ?>
                                    </div>
                                </div>
                            <?php endforeach; ?>
                        </div>
                        <br clear="all" />
                    <?php endif; ?>
                <?php endforeach; ?>
            </div>
        </div>
    </div>
</div>