<?php
/*********************************************\
|****************** OPENCMS ******************|
|*********************************************|
|* @author Yannici                           *|
|* @copyright Yannici                        *|
|*********************************************|
|* @since 04.11.2013                         *|
\*********************************************/
?>
<div class="row">
    <div class="span12-hero">
        <div class="box">
            <div class="header red">
                <?php echo $this->lang->line('hotel_gameerror_header'); ?>
            </div>
            <div class="content">
                <h1><?php echo $this->lang->line('hotel_gameerror_clienterror'); ?></h1>
                <h4><?php echo $this->lang->line('hotel_gameerror_clienterror_description'); ?></h4>
                <a href="<?php echo $PATH; ?>/game" class="btn btn-success"><?php echo $this->lang->line('hotel_gameerror_back_in_hotel'); ?></a>
            </div>
        </div>
    </div>
</div>