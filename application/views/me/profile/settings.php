<?php
/* * *******************************************\
  |****************** OPENCMS ******************|
  |*********************************************|
  |* @author Yannici                           *|
  |* @copyright Yannici                        *|
  |*********************************************|
  |* @since 04.11.2013                         *|
  \******************************************** */
?>

<div class="span8">
    <div class="box">
        <div class="header red">
            <?php echo $this->lang->line('hotel_profile_settings'); ?>
        </div>
        <div class="content">
            <?php echo form_open('profile/settings/save_settings'); ?>
            <div class="control-group">
                <table cellpadding="0" cellspacing="0" class="inputtable">
                    <tr>
                        <td><?php echo $this->lang->line('hotel_motto'); ?></td>
                        <td><?php echo form_input(array('name' => 'settings_motto', 'value' => $this->user->motto, 'maxlength' => '50', 'style' => 'width:97%;')); ?></td>
                    </tr>
                    <tr>
                        <td><?php echo $this->lang->line('hotel_email'); ?></td>
                        <td><?php echo form_input(array('name' => 'settings_email', 'value' => $this->user->mail, 'maxlength' => '100', 'style' => 'width:97%;')); ?></td>
                    </tr>
                    <tr>
                        <td valign="top"><?php echo $this->lang->line('hotel_profile_settings_new_friends'); ?></td>
                        <td class="roc"><label class="checkbox toggle candy">
					<input id="view" value="set" <?php echo ($this->user->block_newfriends == '0') ? 'checked="checked"' : ''; ?> name="settings_block_newfriends" type="checkbox" />
					<p>
						<span><?php echo $this->lang->line('hotel_yes'); ?></span>
						<span><?php echo $this->lang->line('hotel_no'); ?></span>
					</p>
					
					<a class="slide-button"></a>
				</label></td>
                    </tr>
                    <tr>
                        <td valign="top"><?php echo $this->lang->line('hotel_profile_settings_hide_online'); ?></td>
                        <td class="roc"><label class="checkbox toggle candy">
					<input id="view" value="set" <?php echo ($this->user->hide_online == '1') ? 'checked="checked"' : ''; ?> name="settings_hide_online" type="checkbox" />
					<p>
						<span><?php echo $this->lang->line('hotel_yes'); ?></span>
						<span><?php echo $this->lang->line('hotel_no'); ?></span>
					</p>
					
					<a class="slide-button"></a>
				</label></td>
                    </tr>
                    <tr>
                        <td valign="top"><?php echo $this->lang->line('hotel_profile_settings_trading'); ?></td>
                        <td class="roc"><label class="checkbox toggle candy">
					<input id="view" value="set" <?php echo ($this->user->accept_trading == '1') ? 'checked="checked"' : ''; ?> name="settings_trading" type="checkbox" />
					<p>
						<span><?php echo $this->lang->line('hotel_yes'); ?></span>
						<span><?php echo $this->lang->line('hotel_no'); ?></span>
					</p>
					
					<a class="slide-button"></a>
				</label></td>
                    </tr>
                    <tr>
                        <td valign="top"><?php echo $this->lang->line('hotel_profile_settings_volume'); ?></td>
                        <td class="roc"><div style="margin:0;" id="slider">
                                <input class="bar" type="range" id="rangeinput" name="settings_volume" value="<?php echo $this->user->volume; ?>" onchange="rangevalue.value = value"/>
                                <span class="highlight"></span>
                                <output id="rangevalue"><?php echo $this->user->volume; ?></output>
                            </div></td>
                    </tr>
                    <tr>
                        <td colspan="2"><input type="submit" value="Speichern &raquo;" class="btn btn-success" style="width:100%;" /></td>
                    </tr>
                </table>
            </div>
            <?php echo form_close(); ?>
        </div>
    </div>
</div>