<?php
/* * *******************************************\
  |****************** OPENCMS ******************|
  |*********************************************|
  |* @author Yannici                           *|
  |* @copyright Yannici                        *|
  |*********************************************|
  |* @since 04.11.2013                         *|
  \******************************************** */
?>

<div class="span8">
    <div class="box">
        <div class="header red">
            <?php echo $this->lang->line('hotel_profile_settings'); ?>
        </div>
        <div class="content">
            <?php echo form_open('profile/newpassword/save_password'); ?>
            <div class="control-group">
                <table cellpadding="0" cellspacing="0" class="inputtable">
                    <tr>
                        <td><?php echo $this->lang->line('hotel_settings_newpassword_active_password'); ?></td>
                        <td><?php echo form_password(array('name' => 'input_password', 'maxlength' => '100', 'style' => 'width:97%;')); ?></td>
                    </tr>
                    <tr>
                        <td><?php echo $this->lang->line('hotel_settings_newpassword_new_password'); ?></td>
                        <td><?php echo form_password(array('name' => 'input_newpassword', 'maxlength' => '100', 'style' => 'width:97%;')); ?></td>
                    </tr>
                    <tr>
                        <td><?php echo $this->lang->line('hotel_settings_newpassword_new_password_wdh'); ?></td>
                        <td><?php echo form_password(array('name' => 'input_newpassword_wdh', 'maxlength' => '100', 'style' => 'width:97%;')); ?></td>
                    </tr>
                    <tr>
                        <td colspan="2"><input type="submit" value="Speichern &raquo;" class="btn btn-success" style="width:100%;" /></td>
                    </tr>
                </table>
            </div>
            <?php echo form_close(); ?>
        </div>
    </div>
</div>