<?php
/* * *******************************************\
  |****************** OPENCMS ******************|
  |*********************************************|
  |* @author Yannici                           *|
  |* @copyright Yannici                        *|
  |*********************************************|
  |* @since 04.11.2013                         *|
  \******************************************** */
?>
<div class="row">
    <div class="span4-right">
        <div class="box">
            <div class="header orange">
                <?php echo $this->lang->line('hotel_latest_news'); ?>
            </div>
            <div class="content">
                <?php if (count($all_news) == 0): ?>
                    <h5><?php echo $this->lang->line('hotel_no_news_exists'); ?></h5>
                <?php else: ?>
                    <?php $i = 0; ?>
                    <?php foreach ($all_news As $datenews => $news_set): ?>
                        <?php if ($i != 0): ?>
                            <br />
                        <?php endif; ?>
                        <?php $i++; ?>
                        <span class='small-news-header'>
                            <?php if ($datenews == date($this->config->item('hotel_default_date'), time())): ?>
                                <?php echo ucfirst($this->lang->line('hotel_pretty_date_today')); ?>
                            <?php elseif ($datenews == date($this->config->item('hotel_default_date'), time() - 86400)): ?>
                                <?php echo ucfirst($this->lang->line('hotel_pretty_date_yesterday')); ?>
                            <?php elseif ($datenews == date($this->config->item('hotel_default_date'), time() - 172800)): ?>
                                <?php echo ucfirst($this->lang->line('hotel_pretty_date_before_yesterday')); ?>
                            <?php else: ?>
                                <?php echo $datenews; ?>
                            <?php endif; ?>
                        </span>
                        <hr style='border-style:dashed;margin:0;margin-bottom:10px;border-color:#bbb;' />
                        <?php foreach ($news_set As $n): ?>
                            <span class="small-news"><?php echo utf8_decode($n['title']); ?></span> <a href="<?php echo $PATH; ?>/news/<?php echo $n['id']; ?>"><?php echo $this->lang->line('hotel_read'); ?></a><br />
                        <?php endforeach; ?>
                    <?php endforeach; ?>
                <?php endif; ?>
            </div>
        </div>
    </div>

    <div class="span8-left">
        <div class="box">
            <?php if (count($all_news) == 0): ?>
            <div class="header red">
                <?php echo $this->lang->line('hotel_no_news_exists'); ?>
            </div>
            <div class="content" style='padding-bottom:3px;'>
                <h3><?php echo $this->lang->line('hotel_create_first_news'); ?></h3>
            </div>
            <?php else: ?>
            <div class="header red">
                <?php echo utf8_decode($news['title']); ?>
            </div>
            <div class="content" style='padding-bottom:3px;'>
                <strong><?php echo utf8_decode($news['shortstory']); ?></strong>
                <span class="news-author-text"><?php echo $this->lang->line('hotel_news_author_date', User::get_username_by_id($news['author']), date($this->config->item('hotel_default_date'), $news['published']), date($this->config->item('hotel_default_time'), $news['published'])); ?></span>
                <br /><br />
                <?php echo utf8_decode($news['longstory']); ?>
            </div>
            <?php endif; ?>
        </div>
    </div>

    <?php if (count($all_news) > 0): ?>
    <div class="span8-left" style="margin-top:20px;">
        <div class="box">
            <div class="header green">
                <?php echo $this->lang->line('hotel_news_comments', $COMMENTS_COUNT); ?>
            </div>
            <div class="content" style='padding-bottom:3px;'>
                <script type="text/javascript">
                    $(document).ready(function() {
                        CKEDITOR.replace('comment', { extraPlugins: 'confighelper' });
                    });
                </script>

                <?php if ($COMMENTS_COUNT == 0): ?>
                    <?php echo $this->lang->line('hotel_news_no_comments'); ?>
                <?php else: ?>
                    <?php foreach ($COMMENTS As $c): ?>
                        <?php
                        $user = new User(false);
                        $user->set_user_by_id($c['user_id']);
                        ?>
                        <div class="comments-user">
                            <div class="half-avatar" style="max-height:100px;"><img style="margin-top:-20px;" src="http://www.habbo.de/habbo-imaging/avatarimage?figure=<?php echo $user->look; ?>&amp;direction=4&amp;head_direction=3&amp;gesture=sml&amp;action=wlk&amp;size=l" /></div>
                            <div class="comments-details">
                                <span class="comments-online"><img src="<?php echo $PATH; ?>/data/img/<?php echo ($user->online == '0') ? 'offline.gif' : 'online.gif'; ?>" /></span>
                                <span class="username"><?php echo $user->username; ?></span><br />

                                <span class="comments-message"><?php echo $c['message']; ?></span>
                            </div>
                            <span style="float:right;font-size:11px;"><?php echo $this->core_model->pretty_date($c['timestamp']); ?></span>
                            <br clear="all" />
                        </div>
                    <?php endforeach; ?>
                    <hr style="border-style: dashed; border-color:#ccc; margin:0;" />
                    <?php echo $this->pagination->create_links(); ?>
                    <br /><br />
                <?php endif; ?>

                <?php echo form_open('news/' . $news['id'] . '/write', array('class' => 'form-horizontal')); ?>
                <?php echo form_textarea(array('name' => 'textarea_comment', 'placeholder' => $this->lang->line('hotel_news_write_comment'), 'style' => 'width:98%;height:120px;max-height:150px;', 'id' => 'comment')); ?>
                <input type="submit" value="<?php echo $this->lang->line('hotel_news_send_comment'); ?>" class="btn btn-success" style="float:right;margin-top:10px;" name="comment_submit" />
                <?php echo form_close(); ?>
                <br clear="all" />
            </div>
        </div>
    </div>
    <?php endif; ?>
</div>