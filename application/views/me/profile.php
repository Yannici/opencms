<?php
/*********************************************\
|****************** OPENCMS ******************|
|*********************************************|
|* @author Yannici                           *|
|* @copyright Yannici                        *|
|*********************************************|
|* @since 04.11.2013                         *|
\*********************************************/
?>

<div class="row">
    <?php if(file_exists('./application/views/me/profile/' . $MODE . '.php')) include('profile/' . $MODE . '.php'); ?>

    <div class="span4-right">
        <div class="box">
            <div class="header darkgrey">
                <?php echo $this->lang->line('hotel_profile_settings_navigation'); ?>
            </div>
            <div class="content">
                <a href="<?php echo $PATH; ?>/profile/settings"><?php echo $this->lang->line('hotel_settings'); ?> &raquo;</a><br />
                <a href="<?php echo $PATH; ?>/profile/newpassword"><?php echo $this->lang->line('hotel_profile_edit_password'); ?> &raquo;</a>
            </div>
        </div>
    </div>
</div>
