<?php
/*********************************************\
|****************** OPENCMS ******************|
|*********************************************|
|* @author Yannici                           *|
|* @copyright Yannici                        *|
|*********************************************|
|* @since 04.11.2013                         *|
\*********************************************/
?>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN"
    "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml" xml:lang="en" lang="en" xmlns:og="http://opengraphprotocol.org/schema/">
    <head>
        <meta http-equiv="content-type" content="text/html; charset=ISO-8859-1" />
        <title><?php echo $title; ?> - <?php echo $this->config->item('hotel_name') ?></title>

        <link rel="shortcut icon" type="image/png" href="<?php echo $PATH; ?>/data/favicon.png" />

        <script src="<?php echo $PATH; ?>/data/client/libs2.js" type="text/javascript"></script>
        <script src="<?php echo $PATH; ?>/data/client/visual.js" type="text/javascript"></script>
        <script src="<?php echo $PATH; ?>/data/client/libs.js" type="text/javascript"></script>
        <script src="<?php echo $PATH; ?>/data/client/common.js" type="text/javascript"></script>

        <link rel="stylesheet" href="<?php echo $PATH; ?>/data/client/common.css" type="text/css" />
        <link rel="stylesheet" href="<?php echo $PATH; ?>/data/client/process.css" type="text/css" />
        <link rel="stylesheet" href="<?php echo $PATH; ?>/data/client/style.css" type="text/css" />

        <link rel="stylesheet" href="<?php echo $PATH; ?>/data/client/habboflashclient.css" type="text/css" />
        <script src="<?php echo $PATH; ?>/data/client/habboflashclient.js" type="text/javascript"></script>

        <script type="text/javascript">
            document.habboLoggedIn = true;
            var habboName = "<?php echo $this->user->username; ?>";
            var habboId = <?php echo $this->user->id; ?>;
            var facebookUser = false;
            var habboReqPath = "<?php echo $PATH; ?>/game";
            var habboStaticFilePath = "http://images.habbo.com/habboweb/<?php echo $this->config->item('hotel_webbuild'); ?>/web-gallery";
            var habboImagerUrl = "http://www.habbo.com/habbo-imaging/";
            var habboPartner = "";
            var habboDefaultClientPopupUrl = "<?php echo $PATH; ?>/game";
            window.name = "3981c0260af76a9eb267d9b2dd7cc602d4c7b7bf";
            if (typeof HabboClient != "undefined") {
                HabboClient.windowName = "3981c0260af76a9eb267d9b2dd7cc602d4c7b7bf";
            }
        </script>

        <link rel="stylesheet" href="<?php echo $PATH; ?>/habblet/client/C-Loader/css/habboflashclient.css" type="text/css" />
        <script src="<?php echo $PATH; ?>/data/client/Loader.js" type="text/javascript"></script>
        <script src="<?php echo $PATH; ?>/data/client/habboflashclient.js" type="text/javascript"></script>

        <script type="text/javascript">
            var flashvars = {
                "client.allow.cross.domain": "1",
                "client.notify.cross.domain": "0",
                "connection.info.host": "<?php echo $this->config->item('hotel_host'); ?>",
                "connection.info.port": "<?php echo $this->config->item('hotel_port'); ?>",
                "site.url": "<?php echo $PATH; ?>",
                "url.prefix": "<?php echo $PATH; ?>",
                "client.reload.url": "<?php echo $PATH; ?>/game",
                "client.fatal.error.url": "<?php echo $PATH; ?>/gameerror",
                "client.connection.failed.url": "<?php echo $PATH; ?>/gameerror",
                "external.variables.txt": "<?php echo $this->config->item('hotel_external_variables'); ?>",
                "external.texts.txt": "<?php echo $this->config->item('hotel_external_flash_texts'); ?>",
                "productdata.load.url": "<?php echo $this->config->item('hotel_external_productdata'); ?>",
                "furnidata.load.url": "<?php echo $this->config->item('hotel_external_furnidata'); ?>",
                "use.sso.ticket": "1",
                "sso.ticket": "<?php echo $this->core_model->guid(); ?>",
                "processlog.enabled": "1",
                "account_id": "1",
                "client.starting": "<?php echo $this->lang->line('hotel_startup', $this->config->item('hotel_name')); ?>",
                "flash.client.url": "<?php echo $this->config->item('hotel_flash_client_url'); ?>",
                "user.hash": "31385693ae558a03d28fc720be6b41cb1ccfec02",
                "has.identity": "1",
                "flash.client.origin": "popup",
                "forward.type" : "<?php echo ($FORWARD_TYPE == 0) ? 2 : $FORWARD_TYPE; ?>",
                "forward.id" : "<?php echo ($FORWARD_ID == 0) ? $this->config->item('hotel_welcome_lounge_id') : $FORWARD_TYPE; ?>"
            };
            var params = {
                "base": "<?php echo $this->config->item('hotel_swf_folder'); ?>",
                "allowScriptAccess": "always",
                "menu": "false"
            };

            if (!(HabbletLoader.needsFlashKbWorkaround())) {
                params["wmode"] = "opaque";
            }
            FlashExternalInterface.signoutUrl = "<?php echo $PATH; ?>/logout";

            var clientUrl = "<?php echo $this->config->item('hotel_swf'); ?>";

            swfobject.embedSWF(clientUrl, "flash-container", "100%", "100%", "10.0.0", "http://images.habbo.com/habboweb/<?php echo $this->config->item('hotel_webbuild'); ?>/web-gallery/flash/expressInstall.swf", flashvars, params);
            window.onbeforeunload = unloading;
            function unloading() {
                var clientObject;
                if (navigator.appName.indexOf("Microsoft") != -1) {
                    clientObject = window["flash-container"];
                } else {
                    clientObject = document["flash-container"];
                }
                try {
                    clientObject.unloading();
                } catch (e) {
                }
            }
        </script> 

    </head>  

    <script type="text/javascript">
        var andSoItBegins = (new Date()).getTime();
    </script>
    <noscript>
        <meta http-equiv="refresh" content="0;url=/game" />
    </noscript>

    <body id="client" class="flashclient">
        <script type="text/javascript">
            jjLoader.init('client', 6, '<?php echo $PATH; ?>/web-gallery/v2/images/habbo.png', '<?php echo $PATH; ?>/habblet/client/C-Loader/images/view.png');
        </script>
        <div id="client-ui">               
            <div id="flash-wrapper">
                <div id="flash-container">
                    <div id="content" style="width: 400px; margin: 20px auto 0 auto; display: none">
                        <div class="cbb clearfix">
                            <h2 class="title">Bitte besorg dir die neueste Adobe Flash Player Version.</h2>

                            <div class="box-content">
                                <p>Hier kannst Du den Adobe Flashplayer herunterladen und installieren:  <a href="http://get.adobe.com/flashplayer/">Install flash player</a>. Mehr Informationen und Anweisungen zur Installation sind hier zu finden:  <a href="http://www.adobe.com/products/flashplayer/productinfo/instructions/">Mehr Informationen</a></p>
                                <p><a href="http://www.adobe.com/go/getflashplayer"><img src="http://images.habbo.com/habboweb/63_1dc60c6d6ea6e089c6893ab4e0541ee0/317/web-gallery/v2/images/client/get_flash_player.gif" alt="Get Adobe Flash player" /></a></p>
                            </div>
                        </div>
                    </div>
                    <script type="text/javascript">
                        $(document).ready(function() {
                            $('#content').show();
                        });
                    </script>
                    <noscript>
                        <div style="width: 400px; margin: 20px auto 0 auto; text-align: center">
                            <p>If you are not automatically redirected, please <a href="/client/nojs">click here</a></p>
                        </div>
                    </noscript>
                </div>
            </div>
        </div>
        <script src="https://ajax.googleapis.com/ajax/libs/jquery/1.7.1/jquery.min.js"></script>
    </body>   
</html>