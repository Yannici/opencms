<?php
/* * *******************************************\
  |****************** OPENCMS ******************|
  |*********************************************|
  |* @author Yannici                           *|
  |* @copyright Yannici                        *|
  |*********************************************|
  |* @since 04.11.2013                         *|
  \******************************************** */
?>
<div class="row">
    <div class="span8">
        <?php if (count($NEWS) == 0): ?>
            <h3><?php echo $this->lang->line('hotel_create_first_news'); ?></h3>
        <?php else: ?>
            <div style="height:250px;">
                <div id="wowslider-container1">
                    <div class="ws_images"><ul>
                            <?php foreach ($NEWS As $n): ?>
                                <li><a href="<?php echo $PATH; ?>/news/<?php echo $n['id']; ?>"><img src="<?php echo $n['image']; ?>" alt="<?php echo utf8_decode($n['title']); ?>" title="<?php echo utf8_decode($n['title']); ?>" id="wows1_0"/></a><?php echo utf8_decode($n['shortstory']); ?></li>
                            <?php endforeach; ?>
                        </ul></div>
                    <div class="ws_shadow"></div>
                    <div class="ws_bullets"><div>
                            <?php for ($i = 0; $i < count($NEWS); $i++): ?>
                                <a href="#" title="<?php echo utf8_decode($NEWS[$i]['title']); ?>"><?php echo utf8_decode($NEWS[$i]['title']); ?></a>
                            <?php endfor; ?>
                        </div></div>
                </div>

                <script type="text/javascript" src="<?php echo $PATH ?>/data/js/wowslider/engine/wowslider.js"></script>
                <script type="text/javascript" src="<?php echo $PATH ?>/data/js/wowslider/engine/script.js"></script>
            </div>
        <?php endif; ?>

        <a href="<?php echo $PATH; ?>/game" target="eac955c8dbc88172421193892a3e98fc7402021a" onclick="HabboClient.openOrFocus(this); return false;" style="margin-top:20px;text-align:center;" class="checkin2 btn btn-success-animated fullwidth animated"><?php echo $this->lang->line('hotel_goto_client') ?></a>

    </div>

    <div class="span4-right">
        <div class="box">
            <div class="header orange">
                <?php echo $this->lang->line('hotel_latest_news'); ?>
            </div>
            <div class="content">
                <?php if (count($NEWS) == 0): ?>
                    <h5><?php echo $this->lang->line('hotel_no_news_exists'); ?></h5>
                <?php else: ?>
                    <?php $i = 0; ?>
                    <?php foreach ($NEWS As $n): ?>
                        <?php $i++; ?>
                        <span style="float:right;margin-top:10px;font-size:12px;"><?php echo date($this->config->item('hotel_default_datetime'), $n['published']); ?></span><h4><?php echo utf8_decode($n['title']); ?></h4>
                        <p><?php echo utf8_decode($n['shortstory']); ?> <br/><a href="<?php echo $PATH; ?>/news/<?php echo $n['id']; ?>"><?php echo $this->lang->line('hotel_readmore'); ?> &raquo;</a></p>
                        <?php echo ($i < count($NEWS)) ? '<hr style="border-color:#ccc;">' : '<br clear="all" />'; ?>
                    <?php endforeach; ?>
                <?php endif; ?>
            </div>
        </div>
    </div>
</div>
