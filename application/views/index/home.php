<?php
/*********************************************\
|****************** OPENCMS ******************|
|*********************************************|
|* @author Yannici                           *|
|* @copyright Yannici                        *|
|*********************************************|
|* @since 04.11.2013                         *|
\*********************************************/
?>

<div class="row">
    <div class="span12-hero" id="hero" style="display:none;">
        <div class="online_users">
                <?php echo $this->lang->line('hotel_online_users', $ONLINE_USERS); ?>
        </div>
        <div class="hero-unit">
            <h1><?php echo $this->lang->line('hotel_welcome', $this->config->item('hotel_name')) ?></h1>
            <p><?php echo $this->lang->line('hotel_welcome_message', $this->config->item('hotel_name')) ?></p>
            <a class="btn btn-success" id="reg" href="#register">
                <?php echo $this->lang->line('hotel_register') ?>
            </a>
        </div>
    </div>
</div>

<div class="row" style="margin-top:-50px;">
    <div class="span4" id="thumb1" style="display:none;">
        <div class="thumbnail">
            <img src="<?php echo $PATH ?>/data/img/index_1.gif" alt="<?php echo $this->lang->line('hotel_thumbnail_label_1') ?>">
            <div class="caption">
                <h3><?php echo $this->lang->line('hotel_thumbnail_label_1') ?></h3>
                <p><?php echo $this->lang->line('hotel_thumbnail_text_1') ?></p>
            </div>
        </div>
    </div>

    <div class="span4" id="thumb2" style="display:none;">
        <div class="thumbnail">
            <img src="<?php echo $PATH ?>/data/img/index_2.gif" alt="<?php echo $this->lang->line('hotel_thumbnail_label_2') ?>">
            <div class="caption">
                <h3><?php echo $this->lang->line('hotel_thumbnail_label_2') ?></h3>
                <p><?php echo $this->lang->line('hotel_thumbnail_text_2') ?></p>
            </div>
        </div>
    </div>

    <div class="span4" id="thumb3" style="display:none;">
        <div class="thumbnail">
            <img src="<?php echo $PATH ?>/data/img/index_3.gif" alt="<?php echo $this->lang->line('hotel_thumbnail_label_3') ?>">
            <div class="caption">
                <h3><?php echo $this->lang->line('hotel_thumbnail_label_3') ?></h3>
                <p><?php echo $this->lang->line('hotel_thumbnail_text_3') ?></p>
            </div>
        </div>
    </div>
</div> <!-- /row -->

<noscript>
<div class="row">
    <div class="span12-hero" id="hero">
        <div class="hero-unit">
            <h1><?php echo $this->lang->line('hotel_welcome', $this->config->item('hotel_name')) ?></h1>
            <p><?php echo $this->lang->line('hotel_welcome_message', $this->config->item('hotel_name')) ?></p>
            <a class="btn btn-success" id="reg" href="#register">
                <?php echo $this->lang->line('hotel_register') ?>
            </a>
        </div>
    </div>
</div>

<div class="row" style="margin-top:-50px;">
    <div class="span4-right" id="thumb1">
        <div class="thumbnail">
            <img src="<?php echo $PATH ?>/data/img/index_1.gif" alt="<?php echo $this->lang->line('hotel_thumbnail_label_1') ?>">
            <div class="caption">
                <h3><?php echo $this->lang->line('hotel_thumbnail_label_1') ?></h3>
                <p><?php echo $this->lang->line('hotel_thumbnail_text_1') ?></p>
            </div>
        </div>
    </div>

    <div class="span4-right" id="thumb2">
        <div class="thumbnail">
            <img src="<?php echo $PATH ?>/data/img/index_2.gif" alt="<?php echo $this->lang->line('hotel_thumbnail_label_2') ?>">
            <div class="caption">
                <h3><?php echo $this->lang->line('hotel_thumbnail_label_2') ?></h3>
                <p><?php echo $this->lang->line('hotel_thumbnail_text_2') ?></p>
            </div>
        </div>
    </div>

    <div class="span4-right" id="thumb3">
        <div class="thumbnail">
            <img src="<?php echo $PATH ?>/data/img/index_3.gif" alt="<?php echo $this->lang->line('hotel_thumbnail_label_3') ?>">
            <div class="caption">
                <h3><?php echo $this->lang->line('hotel_thumbnail_label_3') ?></h3>
                <p><?php echo $this->lang->line('hotel_thumbnail_text_3') ?></p>
            </div>
        </div>
    </div>
</div> <!-- /row -->
</noscript>