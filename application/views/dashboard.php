<?php
/* * *******************************************\
  |****************** OPENCMS ******************|
  |*********************************************|
  |* @author Yannici                           *|
  |* @copyright Yannici                        *|
  |*********************************************|
  |* @since 04.11.2013                         *|
  \******************************************** */
?>
<link href="<?php echo $PATH; ?>/data-admin/assets/base.css" rel="stylesheet" media="screen">
<div class="row-fluid">
    <!-- block -->
    <div class="block span6">
        <div class="navbar navbar-inner block-header">
            <div class="muted pull-left"><?php echo $this->lang->line('hotel_admin_last_online_users'); ?></div>
        </div>
        <div class="block-content collapse in">
            <div class="span12">
                <table cellpadding="0" cellspacing="0" border="0" class="table table-bordered table-hover table-striped">
                    <thead>
                        <tr>
                            <th><?php echo $this->lang->line('hotel_username'); ?></th>
                            <th><?php echo $this->lang->line('hotel_admin_dashboard_last_ip'); ?></th>
                            <th><?php echo $this->lang->line('hotel_admin_dashboard_date'); ?></th>
                            <th><?php echo $this->lang->line('hotel_admin_dashboard_online'); ?></th>
                            <th></th>
                        </tr>
                    </thead>
                    <tbody>
                        <?php foreach ($LAST_USER_ONLINE As $row): ?>
                            <tr>
                        <td id="vcenter"><strong><?php echo $row['username']; ?></strong></td>
                        <td id="vcenter"><?php echo $row['ip_last']; ?></td>
                        <td id="vcenter"><?php echo $row['last_online']; ?></td>
                        <td id="vcenter_tcenter"><?php echo $row['online']; ?></td>
                        <td id="tcenter"><a href="javascript:;" class="btn btn-warning"><i class="icon-pencil"></i> <?php echo $this->lang->line('hotel_admin_edit'); ?></a></td>
                        </tr>
                    <?php endforeach; ?>
                    </tbody>
                </table>
            </div>
        </div>
    </div>
    <!-- /block -->

    <!-- block -->
    <div class="block span6">
        <div class="navbar navbar-inner block-header">
            <div class="muted pull-left"><?php echo $this->lang->line('hotel_admin_last_staff_activities'); ?></div>
        </div>
        <div class="block-content collapse in">
            <div class="span12">
                <table cellpadding="0" cellspacing="0" border="0" class="table table-bordered table-hover table-striped">
                    <thead>
                        <tr>
                            <th><?php echo $this->lang->line('hotel_username'); ?></th>
                            <th><?php echo $this->lang->line('hotel_admin_dashboard_message'); ?></th>
                            <th><?php echo $this->lang->line('hotel_admin_dashboard_date'); ?></th>
                        </tr>
                    </thead>
                    <tbody>
                        <?php foreach ($LAST_STAFF_ACTIVITIES As $row): ?>
                            <tr>
                        <td id="vcenter"><strong><?php echo $row['username']; ?></strong></td>
                        <td id="vcenter"><?php echo $row['message']; ?></td>
                        <td id="vcenter"><?php echo $row['stamp']; ?></td>
                        </tr>
                    <?php endforeach; ?>
                    </tbody>
                </table>
            </div>
        </div>
    </div>
    <!-- /block -->
</div>