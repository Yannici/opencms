<?php
/*********************************************\
|****************** OPENCMS ******************|
|*********************************************|
|* @author Yannici                           *|
|* @copyright Yannici                        *|
|*********************************************|
|* @since 04.11.2013                         *|
\*********************************************/
?>
<!doctype html>
<html lang="de">
    <head>
        <meta charset="ISO-8859-1">
        <meta name="viewport" content="width=device-width; initial-scale=0.5; maximum-scale=1.0; user-scalable=1;" />
        <title><?php echo $title; ?> - <?php echo $this->config->item('hotel_name') ?></title>
        <link rel="stylesheet" type="text/css" href="<?php echo $PATH; ?>/data/js/wowslider/engine/style.css" />
        <link rel="stylesheet" href="<?php echo $PATH; ?>/data/css/bootstrap.css">
        <link rel="stylesheet" href="<?php echo $PATH; ?>/data/css/animate/animate.min.css">
        <!-- <link rel="stylesheet" href="<?php echo $PATH; ?>/data/css/bootstrap-responsive.css"> -->
        <link href='http://fonts.googleapis.com/css?family=Source+Sans+Pro:400,700,400italic,700italic' rel='stylesheet' type='text/css'>
        <link rel="stylesheet" href="<?php echo $PATH; ?>/data/css/toggle-switch.css">
        <link rel="stylesheet" href="<?php echo $PATH; ?>/data/css/style.css">
        <link rel="shortcut icon" type="image/png" href="<?php echo $PATH; ?>/data/favicon.png">
        <!--[if lt IE 9]>
        <script src="<?php echo $PATH ?>/data/dist/html5shiv.js"></script>
        <![endif]-->
        <link href="<?php echo $PATH; ?>/data/js/icheck/skins/line/grey.css" rel="stylesheet">
        <script src="<?php echo $PATH; ?>/data/js/jquery-1.9.1.js"></script>
        <script src="<?php echo $PATH; ?>/data/js/icheck/jquery.icheck.js"></script>
        <link rel="stylesheet" href="<?php echo $PATH; ?>/data/js/fancybox/source/jquery.fancybox.css?v=2.1.5" type="text/css" media="screen" />
        <script type="text/javascript" src="<?php echo $PATH; ?>/data/js/fancybox/source/jquery.fancybox.pack.js?v=2.1.5"></script>
        <script type="text/javascript" src="<?php echo $PATH; ?>/data/client/common.js"></script>
    </head>
    <body>
        <div class="container">
            <script type="text/javascript" src="<?php echo $PATH ?>/data/js/base.js"></script>
            <?php if (!$this->user->logged_in()): ?>
                <script type="text/javascript" src="<?php echo $PATH ?>/data/js/base_notloggedin.js"></script>
            <?php endif; ?>
            <?php if ($this->user->logged_in()): ?><h1 style="float:left;"><?php echo $this->config->item('hotel_name') ?></h1> <a href="<?php echo $PATH; ?>/game" target="eac955c8dbc88172421193892a3e98fc7402021a" onclick="HabboClient.openOrFocus(this); alert('test'); return false;" class="checkin head btn shadow btn-success-animated animated"><?php echo $this->lang->line('hotel_goto_client') ?></a> <?php if($this->user->rank >= $this->config->item('admin_min_rank')): ?> <a href="<?php echo $PATH; ?>/openadmin/login" class="checkin head btn shadow btn-info-shadows"><?php echo $this->lang->line('hotel_admin_name') ?></a> <?php endif; endif; ?>
            <a href="<?php echo $PATH ?>"><img style="float:right;margin-top:12px;" src="<?php echo $PATH ?>/data/img/logo.gif" /></a>
            <?php if ($this->user->logged_in()): ?>
                <?php include('navigation.php'); ?>
            <?php else: ?>
                <?php echo form_open('index/login'); ?>
                <div class="control-group login">
                    <?php echo form_input($LOGIN_FORM['login_username']); ?>
                    <?php echo form_password($LOGIN_FORM['login_password']); ?>
                    <input type="submit" value="<?php echo $this->lang->line('hotel_login'); ?>" class="btn btn-primary" style="margin-top:15px;" name="login_submit" />
                </div>
                <?php echo form_close(); ?>

                <div id="register" style="display: none;text-align:center;width:500px;">
                    <img style="margin-bottom:10px;" src="<?php echo $PATH ?>/data/img/logo.gif" />
                    <?php echo form_open('index/register'); ?>
                    <div class="control-group">
                        <table cellpadding="5" style="width:100%;">
                            <tr>
                                <td><?php echo $this->lang->line('hotel_username') ?></td>
                                <td><?php echo form_input($REGISTER_FORM['register_username']); ?></td>
                            </tr>
                            <tr>
                                <td><?php echo $this->lang->line('hotel_password') ?></td>
                                <td><?php echo form_password($REGISTER_FORM['register_password']); ?></td>
                            </tr>
                            <tr>
                                <td><?php echo $this->lang->line('hotel_password_again') ?></td>
                                <td><?php echo form_password($REGISTER_FORM['register_password_wdh']); ?></td>
                            </tr>
                            <tr>
                                <td><?php echo $this->lang->line('hotel_email') ?></td>
                                <td><?php echo form_input($REGISTER_FORM['register_email']); ?></td>
                            </tr>
                            <tr>
                                <td><a tabindex="-1" style="float:right; margin-left:5px; border-style: none;" href="javascript:;" title="Refresh Image" onclick="document.getElementById('siimage').src = '<?php echo $PATH ?>/data/scripts/securimage/securimage_show.php?sid=' + Math.random(); this.blur(); return false;"><img src="<?php echo $PATH ?>/data/scripts/securimage/images/refresh.png" alt="Reload Image" height="16" width="16" onclick="this.blur()" align="bottom" border="0" /></a><img id="siimage" style="float:right; margin-left:-60px; border: 1px solid #000; height:40px; width:100px;" src="<?php echo $PATH ?>/data/scripts/securimage/securimage_show.php?sid=<?php echo md5(uniqid()) ?>" alt="CAPTCHA Image" align="left" /></td>
                                <td><?php echo form_input($REGISTER_FORM['register_captcha']); ?></td>
                            </tr>
                            <tr>
                                <td colspan="2"><input type="submit" class="btn btn-success" value="<?php echo $this->lang->line('hotel_registration_complete'); ?>" name="register_submit" style="margin-top:5px;width:90%;" /></td>
                            </tr>
                        </table>
                    </div>
                    <?php echo form_close(); ?>
                </div>
            <?php endif; ?>

            <?php if ((isset($REGISTRATION) && $REGISTRATION)
                    || (isset($LOGIN) && $LOGIN)
                    || (isset($FORM_ERROR) && $FORM_ERROR)): ?>
                <?php if (validation_errors() != ''): ?>
                    <div class="row" id="lr_error">
                        <div class="span12-hero">
                            <div class="alert alert-error">
                                <button type="button" class="close" data-dismiss="alert">&times;</button>
                                <h4><?php echo $this->lang->line('hotel_error_message_error') ?></h4>
                                <p><?php echo validation_errors() ?></p>
                            </div>
                        </div>
                    </div>
                <?php endif; ?>
            <?php endif; ?>

            <?php if (isset($SUCCESS_TEXT)): ?>
                <?php if ($SUCCESS_TEXT !== ''): ?>
                    <div class="row" id="lr_error">
                        <div class="span12-hero">
                            <div class="alert alert-success">
                                <button type="button" class="close" data-dismiss="alert">&times;</button>
                                <strong><?php echo $this->lang->line('hotel_error_message_success') ?></strong>
                                <?php echo $SUCCESS_TEXT ?>
                            </div>
                        </div>
                    </div>
                <?php endif; ?>
            <?php endif; ?>