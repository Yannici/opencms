<?php
/*
 * @author Yannic Schnetz
 * @copyright Yannic Schnetz
 * 
 * @since 29.04.2013
 */
?>
<hr style="height:0;border-color:#ddd;">
<footer style="float:left;">
    <p>&copy; 2013 OpenCMS by <strong>Yannici</strong> - All rights reserved</p>
</footer>
<p style="float:right;">
    <?php echo $this->lang->line('hotel_online_users_small', $this->core_model->users_online()); ?>
</p>
</div>
</div>
<!-- JavaScript -->
<script type="text/javascript" src="<?php echo $PATH; ?>/data/js/bootstrap.js"></script>
<script type="text/javascript" src="<?php echo $PATH; ?>/data/ckeditor/ckeditor.js"></script>
<script type="text/javascript" src="<?php echo $PATH; ?>/data/js/jquery-ui.js"></script>
</body>
</html>