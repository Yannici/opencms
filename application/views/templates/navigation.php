<?php
/*********************************************\
|****************** OPENCMS ******************|
|*********************************************|
|* @author Yannici                           *|
|* @copyright Yannici                        *|
|*********************************************|
|* @since 04.11.2013                         *|
\*********************************************/
?>

<div class="row navRow">
    <div class="span12">
        <div class="navbar navbar-inverse">
            <div class="navbar-inner">
                <ul class="nav">
                    <?php foreach($this->config->item('hotel_navigation') As $key => $value): ?>
                    <li <?php if(is_array($value)): ?>class="dropdown"<?php endif; ?>>
                        <a <?php if(is_array($value)): ?>href="javascript:;"<?php else: ?>href="<?php echo $PATH; ?>/<?php echo $value; ?>"<?php endif; ?> <?php if(is_array($value)): ?> class="dropdown-toggle" data-toggle="dropdown" <?php endif; ?>>
                            <?php echo $this->core_model->get_string_language($key); ?>
                            <?php if(is_array($value)): ?><b class="caret"></b><?php endif; ?>
                        </a>
                        <?php if(is_array($value)): ?>
                        <ul class="dropdown-menu">
                            <?php $first = true; ?>
                            <?php foreach($value As $url => $name): ?>
                           <li><a href="<?php echo $PATH ?>/<?php echo $url; ?>" <?php if($first): ?>class="first"<?php endif; ?> tabindex="-1"><?php echo $this->core_model->get_string_language($name); ?></a></li>
                            <?php $first = false; ?>
                            <?php endforeach; ?>
                        </ul>
                        <?php endif; ?>
                    </li>
                    <li class="divider-vertical"></li>
                    <?php endforeach; ?>
                </ul>
                <span class="user-small-profile">
                    <span class="username"><?php echo $this->lang->line('hotel_hello_message', $this->user->username) ?></span> <div id="user-profile" style="background-image:url('http://www.habbo.de/habbo-imaging/avatarimage?figure=<?php echo $this->user->look; ?>&amp;direction=4&amp;head_direction=3&amp;gesture=sml&amp;action=wav&amp;size=m');"></div>
                    <span id="user-details">
                        <div class="user-avatar-body" style="background-image:url('http://www.habbo.de/habbo-imaging/avatarimage?figure=<?php echo $this->user->look; ?>&amp;direction=4&amp;head_direction=3&amp;gesture=sml&amp;action=wav&amp;size=m');"></div>
                    <span class="user-values">
                            <img style="width:20px;height:20px;margin-right:3px;" src="<?php echo $PATH ?>/data/img/coins.gif" /><strong><?php echo number_format($this->user->credits, 0, '.', '.') ?></strong> <?php echo $this->lang->line('hotel_credits') ?>
                            <div class="divider-horizontal-first"></div>
                            <img style="width:20px;height:20px;margin-right:3px;" src="<?php echo $PATH ?>/data/img/pixel.png" /><strong><?php echo number_format($this->user->activity_points, 0, '.', '.') ?></strong> <?php echo $this->lang->line('hotel_activity_points') ?>
                            <div class="divider-horizontal"></div>
                            <img style="width:20px;height:20px;margin-right:3px;" src="<?php echo $PATH ?>/data/img/diamond.png" /><strong><?php echo number_format($this->user->vip_points, 0, '.', '.') ?></strong> <?php echo ($this->user->vip_points != 1) ? $this->lang->line('hotel_vip_points', 'en') : $this->lang->line('hotel_vip_points', '') ?>
                        </span>
                    </span>
                </span>
            </div> <!-- /.navbar-inner -->
        </div> <!-- /.navbar -->
    </div> <!-- /span12 -->
</div> <!-- /row -->