<?php
/*********************************************\
|****************** OPENCMS ******************|
|*********************************************|
|* @author Yannici                           *|
|* @copyright Yannici                        *|
|*********************************************|
|* @since 04.11.2013                         *|
\*********************************************/
?>
<link href="<?php echo $PATH; ?>/data-admin/assets/base.css" rel="stylesheet" media="screen">
<?php if (isset($UPDATE_AVAILABLE) && $UPDATE_AVAILABLE): ?>
    <div class="row-fluid" id="lr_error">
        <div class="span12">
            <div class="alert alert-success">
                <button type="button" class="close" data-dismiss="alert">&times;</button>
                <strong><?php echo $this->lang->line('hotel_admin_update') ?></strong>
                <?php echo $this->lang->line('hotel_admin_update_available', $this->update_model->get_latest_version(), $this->update_model->get_version(), $this->update_model->get_update_link()); ?>
            </div>
        </div>
    </div>
<?php endif; ?>
<div class="row-fluid">
    <!-- block -->
    <div class="block span6">
        <div class="navbar navbar-inner block-header">
            <div class="muted pull-left"><i class="icon-eye-open"></i> <?php echo $this->lang->line('hotel_admin_last_online_users'); ?></div>
        </div>
        <div class="block-content collapse in">
            <div class="span12">
                <table cellpadding="0" cellspacing="0" border="0" class="table table-bordered table-hover table-striped">
                    
                    <tbody>
                        <?php foreach ($LAST_USER_ONLINE As $row): ?>
                            <tr>
                        <td id="vcenter"><strong><?php echo $row['username']; ?></strong></td>
                        <td id="vcenter"><?php echo $row['ip_last']; ?></td>
                        <td id="vcenter"><?php echo $row['last_online']; ?></td>
                        <td id="vcenter_tcenter"><?php echo $row['online']; ?></td>
                        <td id="tcenter"><a href="<?php echo $PATH; ?>/openadmin/useredit/edit/<?php echo $row['username']; ?>" class="btn btn-warning"><i class="icon-pencil"></i> <?php echo $this->lang->line('hotel_admin_edit'); ?></a></td>
                        </tr>
                    <?php endforeach; ?>
                    </tbody>
                </table>
            </div>
        </div>
    </div>
    <!-- /block -->

    <!-- block -->
    <div class="block span6">
        <div class="navbar navbar-inner block-header">
            <div class="muted pull-left"><i class="icon-comment"></i> <?php echo $this->lang->line('hotel_admin_last_staff_activities'); ?></div>
        </div>
        <div class="block-content collapse in">
            <div class="span12">
                <table cellpadding="0" cellspacing="0" border="0" class="table table-bordered table-hover table-striped">
                    <tbody>
                        <?php foreach ($LAST_STAFF_ACTIVITIES As $row): ?>
                            <tr>
                        <td id="vcenter"><strong><?php echo $row['username']; ?></strong></td>
                        <td id="vcenter"><?php echo $row['message']; ?></td>
                        <td id="vcenter"><?php echo $row['stamp']; ?></td>
                        </tr>
                    <?php endforeach; ?>
                    </tbody>
                </table>
            </div>
        </div>
    </div>
    <!-- /block -->
</div>