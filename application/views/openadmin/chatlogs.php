<?php
/*********************************************\
|****************** OPENCMS ******************|
|*********************************************|
|* @author Yannici                           *|
|* @copyright Yannici                        *|
|*********************************************|
|* @since 04.11.2013                         *|
\*********************************************/
?>

<link href="<?php echo $PATH; ?>/data-admin/assets/base.css" rel="stylesheet" media="screen">
<div class="row-fluid">
    <div class="block span12">
        <div class="navbar navbar-inner block-header">
            <div class="muted pull-left"><i class="icon-comment"></i> <?php echo $this->lang->line('hotel_admin_chatlogs'); ?></div>
        </div>
        <div class="block-content collapse in">
            <div class="span12">
                <?php echo form_open('openadmin/chatlogs/show', array('class' => 'form-horizontal')); ?>
                <fieldset>
                    <legend><?php echo $this->lang->line('hotel_admin_chatlogs_search'); ?></legend>
                    <div class="control-group">
                        <?php echo form_label($this->lang->line('hotel_admin_chatlogs_inputfield'), 'input_chatlogs', array('class' => 'control-label')); ?>
                        <div class="controls">
                            <?php echo form_input(array('name' => 'input_chatlogs', 'class' => 'input-xlarge')); ?>
                            <p class="help-block"><?php echo $this->lang->line('hotel_admin_chatlogs_inputfield_description'); ?></p>
                        </div>
                    </div>
                    <div class="control-group">
                        <?php echo form_label($this->lang->line('hotel_admin_chatlogs_dropdown_type'), 'dropdown_chatlogstype', array('class' => 'control-label')); ?>
                        <div class="controls">
                            <?php echo form_dropdown('dropdown_chatlogstype', array('user' => $this->lang->line('hotel_user'), 'rooms' => $this->lang->line('hotel_rooms')), 'user'); ?>
                            <p class="help-block"><?php echo $this->lang->line('hotel_admin_chatlogs_chatlogstype_description'); ?></p>
                        </div>
                    </div>
                    <div class="form-actions">
                        <button type="reset" class="btn"><?php echo $this->lang->line('hotel_form_reset'); ?></button>
                        <button type="submit" class="btn btn-primary"><?php echo $this->lang->line('hotel_admin_chatlogs_submit_input'); ?></button>
                    </div>
                </fieldset>
                <?php echo form_close(); ?>
            </div>
        </div>
    </div>
</div>