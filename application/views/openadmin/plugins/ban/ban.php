<?php
/* * *******************************************\
  |****************** OPENCMS ******************|
  |*********************************************|
  |* @author Yannici                           *|
  |* @copyright Yannici                        *|
  |*********************************************|
  |* @since 04.11.2013                         *|
  \******************************************** */
?>

<link href="<?php echo $PATH; ?>/data-admin/assets/base.css" rel="stylesheet" media="screen">
<script type="text/javascript" language="JavaScript">
    $(document).ready(function() {
        $('.banounban').change(function() {
            $('.banounban option:selected').each(function(){
                if($(this).val() == 'ban') {
                    $('select[name="dropdown_length"]').parent().parent().slideDown(1000);
                    $('input[name="input_reason"]').parent().parent().slideDown(1000);
                    $('button[type="submit"]').html('<?php echo $this->lang->line('hotel_admin_ban_submit_ban_input'); ?>');
                } else if($(this).val() == 'unban') {
                    $('select[name="dropdown_length"]').parent().parent().slideUp(1000);
                    $('input[name="input_reason"]').parent().parent().slideUp(1000);
                    $('button[type="submit"]').html('<?php echo $this->lang->line('hotel_admin_ban_submit_unban_input'); ?>');
                }
            });
        });
    });
</script>
<div class="row-fluid">
    <div class="block span12">
        <div class="navbar navbar-inner block-header">
            <div class="muted pull-left"><i class="icon-search"></i> <?php echo $this->lang->line('hotel_admin_ban'); ?></div>
        </div>
        <div class="block-content collapse in">
            <div class="span12">
                <?php echo form_open('openadmin/ban/banhim', array('class' => 'form-horizontal banounban', 'id' => 'form')); ?>
                <fieldset>
                    <legend><?php echo $this->lang->line('hotel_admin_ban'); ?></legend>
                    <div class="control-group">
                        <?php echo form_label($this->lang->line('hotel_admin_ban_dropdown_ban'), 'dropdown_ban_or_unban', array('class' => 'control-label')); ?>
                        <div class="controls">
                            <?php echo form_dropdown('dropdown_ban_or_unban', array('ban' => $this->lang->line('hotel_admin_ban_dropdown_ban_ban'), 'unban' => $this->lang->line('hotel_admin_ban_dropdown_ban_unban')), 'ban'); ?>
                            <p class="help-block"><?php echo $this->lang->line('hotel_admin_ban_dropdown_ban_description'); ?></p>
                        </div>
                    </div>
                    <div class="control-group">
                        <?php echo form_label($this->lang->line('hotel_admin_ban_input_value'), 'input_value', array('class' => 'control-label')); ?>
                        <div class="controls">
                            <?php echo form_input(array('name' => 'input_value', 'class' => 'input-xlarge')); ?>
                            <p class="help-block"><?php echo $this->lang->line('hotel_admin_ban_input_value_description'); ?></p>
                        </div>
                    </div>
                    <div class="control-group">
                        <?php echo form_label($this->lang->line('hotel_admin_ban_dropdown_length'), 'dropdown_length', array('class' => 'control-label')); ?>
                        <div class="controls">
                            <?php
                            echo form_dropdown('dropdown_length', array(
                                '3600' => $this->lang->line('hotel_hour'),
                                '7200' => $this->lang->line('hotel_hours', 2),
                                '14400' => $this->lang->line('hotel_hours', 4),
                                '28800' => $this->lang->line('hotel_hours', 8),
                                '43200' => $this->lang->line('hotel_hours', 12),
                                '86400' => $this->lang->line('hotel_day'),
                                '172800' => $this->lang->line('hotel_days', 2),
                                '432000' => $this->lang->line('hotel_days', 5),
                                '864000' => $this->lang->line('hotel_days', 10),
                                '1728000' => $this->lang->line('hotel_days', 20),
                                '2592000' => $this->lang->line('hotel_month'),
                                '7776000' => $this->lang->line('hotel_months', 3),
                                '15552000' => $this->lang->line('hotel_months', 6),
                                '20736000' => $this->lang->line('hotel_months', 9),
                                '31536000' => $this->lang->line('hotel_year'),
                                '63072000' => $this->lang->line('hotel_years', 2),
                                '999999999' => $this->lang->line('hotel_permanent')), 'user');
                            ?>
                            <p class="help-block"><?php echo $this->lang->line('hotel_admin_ban_dropdown_length_description'); ?></p>
                        </div>
                    </div>
                    <div class="control-group">
                            <?php echo form_label($this->lang->line('hotel_admin_ban_input_reason'), 'input_reason', array('class' => 'control-label')); ?>
                        <div class="controls">
<?php echo form_input(array('name' => 'input_reason', 'class' => 'input-xlarge')); ?>
                            <p class="help-block"><?php echo $this->lang->line('hotel_admin_ban_input_reason_description'); ?></p>
                        </div>
                    </div>
                    <div class="control-group">
                        <?php echo form_label($this->lang->line('hotel_admin_ban_dropdown_bantype'), 'dropdown_bantype', array('class' => 'control-label')); ?>
                        <div class="controls">
                            <?php
                            echo form_dropdown('dropdown_bantype', array(
                                'user' => $this->lang->line('hotel_ban_dropdown_bantype_user'),
                                'ip' => $this->lang->line('hotel_ban_dropdown_bantype_ip')), 'user');
                            ?>
                            <p class="help-block"><?php echo $this->lang->line('hotel_admin_ban_dropdown_bantype_description'); ?></p>
                        </div>
                    </div>
                    <div class="form-actions">
                        <button type="reset" class="btn"><?php echo $this->lang->line('hotel_form_reset'); ?></button>
                        <button type="submit" class="btn btn-primary"><?php echo $this->lang->line('hotel_admin_ban_submit_ban_input'); ?></button>
                    </div>
                </fieldset>
<?php echo form_close(); ?>
            </div>
        </div>
    </div>
</div>