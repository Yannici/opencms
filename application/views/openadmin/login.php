<?php
/* * *******************************************\
  |****************** OPENCMS ******************|
  |*********************************************|
  |* @author Yannici                           *|
  |* @copyright Yannici                        *|
  |*********************************************|
  |* @since 04.11.2013                         *|
  \******************************************** */
?>

<?php echo form_open('openadmin/loginsubmit', array('class' => 'form-signin')); ?>
<h2 class="form-signin-heading"><?php echo $this->lang->line('hotel_admin_login'); ?></h2>
<?php echo form_input($ADMINLOGIN_FORM['adminlogin_username']); ?>
<?php echo form_password($ADMINLOGIN_FORM['adminlogin_password']); ?>
<?php echo form_password($ADMINLOGIN_FORM['adminlogin_security']); ?>
<input type="submit" style="width:100%;" class="btn btn-large btn-primary" value="<?php echo $this->lang->line('hotel_admin_login_submit'); ?>" />
<?php echo form_close(); ?>