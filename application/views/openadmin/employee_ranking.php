<?php
/*********************************************\
|****************** OPENCMS ******************|
|*********************************************|
|* @author Yannici                           *|
|* @copyright Yannici                        *|
|*********************************************|
|* @since 04.11.2013                         *|
\*********************************************/
?>

<link href="<?php echo $PATH; ?>/data-admin/assets/base.css" rel="stylesheet" media="screen">
<link type="text/css" rel="stylesheet" href="<?php echo $PATH; ?>/data-admin/vendors/datatables/css/jquery.dataTables.css" />
<div class="row-datatable">
    <div class="block span12">
        <div class="navbar navbar-inner block-header">
            <div class="muted pull-left"><i class="icon-headphones"></i> <?php echo $this->lang->line('hotel_admin_employee_ranking'); ?></div>
        </div>
        <div class="block-content collapse in">

            <a href="<?php echo $PATH; ?>/openadmin/employee_ranking/add" class="btn btn-success">
                <i class="icon-plus"></i> <?php echo $this->lang->line('hotel_admin_employee_ranking_add'); ?>
            </a><br /><br />
            <div class="span12">
                <table width="100%" cellpadding="0" cellspacing="0" border="0" class="table table-bordered table-hover table-striped display" style="width:100%;" id="news">
                    <thead>
                        <tr>
                            <th><?php echo $this->lang->line('hotel_identification'); ?></th>
                            <th><?php echo $this->lang->line('hotel_admin_employee_ranking_rank'); ?></th>
                            <th><?php echo $this->lang->line('hotel_admin_employee_ranking_name'); ?></th>
                            <th width="10%"><?php echo $this->lang->line('hotel_admin_actions'); ?></th>
                        </tr>
                    </thead>
                    <tbody>
                        <?php foreach($RANKING As $rank): ?>
                        <tr>
                            <td width="10%"><?php echo $rank['id']; ?></td>
                            <td width="10%"><?php echo $rank['rank']; ?></td>
                            <td><?php echo utf8_decode($rank['name']); ?></td>
                            <td><?php echo form_open('openadmin/employee_ranking/edit/' . $rank['id'], array('style' => 'float:left;margin-right:5px;')); ?><button type="submit" class="btn btn-warning"><i class="icon-pencil"></i></button><?php echo form_close(); ?>
                                <?php echo form_open('openadmin/employee_ranking/delete/' . $rank['id'], array('style' => 'float:left;')); ?><button type="submit" class="btn btn-danger"><i class="icon-trash"></i></button><?php echo form_close(); ?>
                        </tr>
                        <?php endforeach; ?>
                    </tbody>
                </table>
            </div>
        </div>
    </div>
</div>