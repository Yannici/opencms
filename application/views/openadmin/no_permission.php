<?php
/*********************************************\
|****************** OPENCMS ******************|
|*********************************************|
|* @author Yannici                           *|
|* @copyright Yannici                        *|
|*********************************************|
|* @since 04.11.2013                         *|
\*********************************************/
?>
<link href="<?php echo $PATH; ?>/data-admin/assets/base.css" rel="stylesheet" media="screen">
<div class="row-fluid">
    <!-- block -->
    <div class="block span12">
        <div class="navbar navbar-inner block-header">
            <div class="muted pull-left"><i class="icon-eye-open"></i> <?php echo $this->lang->line('hotel_admin_wrong_permissions'); ?></div>
        </div>
        <div class="block-content collapse in">
            <div class="span12">
                <fieldset>
                    <legend><?php echo $this->lang->line('hotel_admin_wrong_permissions'); ?></legend>
                    <?php echo $this->lang->line('hotel_admin_no_permission', $title); ?>
                </fieldset>
            </div>
        </div>
    </div>
    <!-- /block -->
</div>