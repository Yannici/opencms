<?php
/*********************************************\
|****************** OPENCMS ******************|
|*********************************************|
|* @author Yannici                           *|
|* @copyright Yannici                        *|
|*********************************************|
|* @since 04.11.2013                         *|
\*********************************************/
?>

<link href="<?php echo $PATH; ?>/data-admin/assets/base.css" rel="stylesheet" media="screen">
<div class="row-fluid">
    <div class="block span12">
        <div class="navbar navbar-inner block-header">
            <div class="muted pull-left"><i class="icon-pencil"></i> <?php echo $this->lang->line('hotel_admin_password_tool'); ?></div>
        </div>
        <div class="block-content collapse in">
            <div class="span12">
                <?php echo form_open('openadmin/password_tool/submit', array('class' => 'form-horizontal')); ?>
                <fieldset>
                    <legend><?php echo $this->lang->line('hotel_admin_password_tool_edit'); ?></legend>
                    <div class="control-group">
                        <?php echo form_label($this->lang->line('hotel_admin_password_tool_input_user'), 'input_user', array('class' => 'control-label')); ?>
                        <div class="controls">
                            <?php echo form_input(array('name' => 'input_user', 'class' => 'input-xlarge')); ?>
                            <p class="help-block"><?php echo $this->lang->line('hotel_admin_password_tool_input_desc'); ?></p>
                        </div>
                    </div>
                    <div class="control-group">
                        <?php echo form_label($this->lang->line('hotel_admin_password_tool_password_new'), 'password_new', array('class' => 'control-label')); ?>
                        <div class="controls">
                            <?php echo form_password(array('name' => 'password_new', 'class' => 'input-xlarge')); ?>
                            <p class="help-block"><?php echo $this->lang->line('hotel_admin_password_tool_password_new_desc'); ?></p>
                        </div>
                    </div>
                    <div class="form-actions">
                        <button type="reset" class="btn"><?php echo $this->lang->line('hotel_form_reset'); ?></button>
                        <button type="submit" class="btn btn-primary"><?php echo $this->lang->line('hotel_admin_useredit_input_submit'); ?></button>
                    </div>
                </fieldset>
                <?php echo form_close(); ?>
            </div>
        </div>
    </div>
</div>