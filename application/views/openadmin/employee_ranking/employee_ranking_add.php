<?php
/*********************************************\
|****************** OPENCMS ******************|
|*********************************************|
|* @author Yannici                           *|
|* @copyright Yannici                        *|
|*********************************************|
|* @since 04.11.2013                         *|
\*********************************************/
?>

<link href="<?php echo $PATH; ?>/data-admin/assets/base.css" rel="stylesheet" media="screen">
<script type="text/javascript">
    $(document).ready(function() {
       tinymce.init({
            selector: ".textarea",
            width: "800px",
            force_br_newlines : false,
            force_p_newlines : false,
            forced_root_block : 'p',
            toolbar: "insertfile undo redo | styleselect | bold underline italic | alignleft aligncenter alignright alignjustify | bullist numlist outdent indent | link image"
        });
    });
</script>
<div class="row-fluid">
    <div class="block span12">
        <div class="navbar navbar-inner block-header">
            <div class="muted pull-left"><i class="icon-headphones"></i> <?php echo $this->lang->line('hotel_admin_employee_ranking'); ?></div>
        </div>
        <div class="block-content collapse in">
            <div class="span12">
                <a class="btn btn-danger" href="<?php echo $PATH; ?>/openadmin/employee_ranking">
                    <?php echo $this->lang->line('hotel_back'); ?>
                </a>
                <?php echo form_open('openadmin/employee_ranking/add_submit', array('class' => 'form-horizontal')); ?>
                <fieldset>
                    <legend><?php echo $this->lang->line('hotel_admin_employee_ranking_add'); ?></legend>
                    <div class="control-group">
                        <?php echo form_label($this->lang->line('hotel_admin_employee_ranking_rank'), 'input_rank', array('class' => 'control-label')); ?>
                        <div class="controls">
                            <?php echo form_input(array('name' => 'input_rank', 'class' => 'input-small', 'maxlength' => '2', 'value' => set_value('input_rank'))); ?>
                        </div>
                    </div>
                    <div class="control-group">
                        <?php echo form_label($this->lang->line('hotel_admin_employee_ranking_name'), 'input_name', array('class' => 'control-label')); ?>
                        <div class="controls">
                            <?php echo form_input(array('name' => 'input_name', 'class' => 'input-xlarge', 'maxlength' => '255', 'value' => set_value('input_name'))); ?>
                        </div>
                    </div>
                    <div class="control-group">
                        <?php echo form_label($this->lang->line('hotel_admin_employee_ranking_description'), 'textarea_description', array('class' => 'control-label')); ?>
                        <div class="controls">
                            <?php echo form_textarea(array('name' => 'textarea_description', 'class' => 'input-xlarge textarea', 'value' => set_value('textarea_description'))); ?>
                        </div>
                    </div>
                    <div class="form-actions">
                        <button type="reset" class="btn"><?php echo $this->lang->line('hotel_form_reset'); ?></button>
                        <button type="submit" class="btn btn-primary"><?php echo $this->lang->line('hotel_admin_news_submit'); ?></button>
                    </div>
                </fieldset>
                <?php echo form_close(); ?>
            </div>
        </div>
    </div>
</div>