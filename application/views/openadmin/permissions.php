<?php
/*********************************************\
|****************** OPENCMS ******************|
|*********************************************|
|* @author Yannici                           *|
|* @copyright Yannici                        *|
|*********************************************|
|* @since 04.11.2013                         *|
\*********************************************/
?>

<link href="<?php echo $PATH; ?>/data-admin/assets/base.css" rel="stylesheet" media="screen">
<div class="row-fluid">
    <div class="block span12">
        <div class="navbar navbar-inner block-header">
            <div class="muted pull-left"><i class="icon-ban-circle"></i> <?php echo $this->lang->line('hotel_admin_permissions'); ?></div>
        </div>
        <div class="block-content collapse in">
            <div class="span12">
                <?php echo form_open('openadmin/permissions/edit', array('class' => 'form-horizontal')); ?>
                <fieldset>
                    <legend><?php echo $this->lang->line('hotel_admin_permissions_edit'); ?></legend>
                    <div class="control-group">
                        <?php echo form_label($this->lang->line('hotel_admin_permissions_dropdown_rank'), 'dropdown_rank', array('class' => 'control-label')); ?>
                        <div class="controls">
                            <?php echo form_dropdown('dropdown_rank', $GROUPS, 1); ?>
                            <p class="help-block"><?php echo $this->lang->line('hotel_admin_permissions_dropdown_rank_description'); ?></p>
                        </div>
                    </div>
                    <div class="form-actions">
                        <button type="reset" class="btn"><?php echo $this->lang->line('hotel_form_reset'); ?></button>
                        <button type="submit" class="btn btn-primary"><?php echo $this->lang->line('hotel_admin_permissions_submit_input'); ?></button>
                    </div>
                </fieldset>
                <?php echo form_close(); ?>
            </div>
        </div>
    </div>
</div>