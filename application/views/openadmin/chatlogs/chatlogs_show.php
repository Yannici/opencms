<?php
/* * *******************************************\
  |****************** OPENCMS ******************|
  |*********************************************|
  |* @author Yannici                           *|
  |* @copyright Yannici                        *|
  |*********************************************|
  |* @since 04.11.2013                         *|
  \******************************************** */
?>

<link href="<?php echo $PATH; ?>/data-admin/assets/base.css" rel="stylesheet" media="screen">
<link type="text/css" rel="stylesheet" href="<?php echo $PATH; ?>/data-admin/vendors/datatables/css/jquery.dataTables.css" />
<div class="row-datatable">
    <div class="block span12">
        <div class="navbar navbar-inner block-header">
            <div class="muted pull-left"><i class="icon-comment"></i> <?php echo $this->lang->line('hotel_admin_chatlogs'); ?></div>
        </div>
        <div class="block-content collapse in">
            <a class="btn btn-danger" href="<?php echo $PATH; ?>/openadmin/chatlogs">
                <?php echo $this->lang->line('hotel_back'); ?>
            </a><br /><br />
            <div class="span12">
                <script type="text/javascript">
                    $(document).ready(function() {
                        $('#chatlogs').dataTable({
                            "oLanguage": {
                                "sUrl": "<?php echo $PATH; ?>/tabledata/get_translations"
                            },
                            "bProcessing": true,
                            "bServerSide": true,
                            "sServerMethod": "GET",
                            "sAjaxSource": "<?php echo $PATH; ?>/tabledata/get_chatlogs/<?php echo $sType; ?>/<?php echo $sString; ?>",
                            "iDisplayLength": 50,
                            "aLengthMenu": [[10, 25, 50, -1], [10, 25, 50, 100]],
                            "aaSorting": [[2, 'desc']],
                            "aoColumns": [
                                {"bVisible": true, "bSearchable": false, "bSortable": true},
                                {"bVisible": true, "bSearchable": false, "bSortable": false},
                                {"bVisible": true, "bSearchable": false, "bSortable": true},
                                {"bVisible": true, "bSearchable": false, "bSortable": true},
                                {"bVisible": true, "bSearchable": false, "bSortable": true},
                                {"bVisible": true, "bSearchable": true, "bSortable": false}
                            ]
                        }).fnSetFilteringDelay(700);
                    });
                </script>
                <table width="100%" cellpadding="0" cellspacing="0" border="0" class="table table-bordered table-hover table-striped display" style="width:100%;" id="chatlogs">
                    <thead>
                        <tr>
                            <th><?php echo $this->lang->line('hotel_identification'); ?></th>
                            <th><?php echo $this->lang->line('hotel_room'); ?></th>
                            <th width="15%"><?php echo $this->lang->line('hotel_date'); ?></th>
                            <th><?php echo $this->lang->line('hotel_user') . '-' . $this->lang->line('hotel_identification'); ?></th>
                            <th><?php echo $this->lang->line('hotel_username'); ?></th>
                            <th width="45%"><?php echo $this->lang->line('hotel_message'); ?></th>
                        </tr>
                    </thead>
                    <tbody></tbody>
                    <tfoot></tfoot>
                </table>
            </div>
        </div>
    </div>
</div>