<?php
/*********************************************\
|****************** OPENCMS ******************|
|*********************************************|
|* @author Yannici                           *|
|* @copyright Yannici                        *|
|*********************************************|
|* @since 04.11.2013                         *|
\*********************************************/
?>

<link href="<?php echo $PATH; ?>/data-admin/assets/base.css" rel="stylesheet" media="screen">
<div class="row-fluid">
    <div class="block span12">
        <div class="navbar navbar-inner block-header">
            <div class="muted pull-left"><i class="icon-search"></i> <?php echo $this->lang->line('hotel_admin_clonecheck'); ?></div>
        </div>
        <div class="block-content collapse in">
            <div class="span12">
                <?php echo form_open('openadmin/clonecheck/check', array('class' => 'form-horizontal')); ?>
                <fieldset>
                    <legend><?php echo $this->lang->line('hotel_admin_clonecheck_check'); ?></legend>
                    <div class="control-group">
                        <?php echo form_label($this->lang->line('hotel_admin_clonecheck_input_checkstring'), 'input_checkstring', array('class' => 'control-label')); ?>
                        <div class="controls">
                            <?php echo form_input(array('name' => 'input_checkstring', 'class' => 'input-xlarge')); ?>
                            <p class="help-block"><?php echo $this->lang->line('hotel_admin_clonecheck_input_checkstring_description'); ?></p>
                        </div>
                    </div>
                    <div class="control-group">
                        <?php echo form_label($this->lang->line('hotel_admin_clonecheck_dropdown_checktype'), 'dropdown_checktype', array('class' => 'control-label')); ?>
                        <div class="controls">
                            <?php echo form_dropdown('dropdown_checktype', array('user' => $this->lang->line('hotel_user'), 'ip' => $this->lang->line('hotel_ip')), 'user'); ?>
                            <p class="help-block"><?php echo $this->lang->line('hotel_admin_clonecheck_dropdown_checktype_description'); ?></p>
                        </div>
                    </div>
                    <div class="form-actions">
                        <button type="reset" class="btn"><?php echo $this->lang->line('hotel_form_reset'); ?></button>
                        <button type="submit" class="btn btn-primary"><?php echo $this->lang->line('hotel_admin_clonecheck_submit_input'); ?></button>
                    </div>
                </fieldset>
                <?php echo form_close(); ?>
            </div>
        </div>
    </div>
</div>