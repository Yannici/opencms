<?php
/* * *******************************************\
  |****************** OPENCMS ******************|
  |*********************************************|
  |* @author Yannici                           *|
  |* @copyright Yannici                        *|
  |*********************************************|
  |* @since 04.11.2013                         *|
  \******************************************** */
?>

<link href="<?php echo $PATH; ?>/data-admin/assets/base.css" rel="stylesheet" media="screen">
<?php echo form_open($PATH . '/openadmin/useredit/edit_submit'); ?>
<div class="row-fluid">
    <fieldset>
        <legend><?php echo $this->lang->line('hotel_admin_useredit_edit', $USERDATA->username . ' (ID: ' . $USERDATA->id . ')'); ?></legend>
    </fieldset>
</div>
<div class="row-fluid">
    <!-- block -->
    <div class="block span6">
        <div class="navbar navbar-inner block-header">
            <div class="muted pull-left"><i class="icon-home"></i> <?php echo $this->lang->line('hotel_admin_useredit_general_settings'); ?></div>
        </div>
        <div class="block-content collapse in">
            <div class="span12">
                <fieldset>
                    <div class="control-group">
                        <?php if($this->user->rank > $USERDATA->rank): ?><input type="submit" name="delete_user" style="float:right;margin-right:10px;" onclick="return confirm('<?php echo $this->lang->line('hotel_admin_useredit_want_delete'); ?>');" value="<?php echo $this->lang->line('hotel_admin_useredit_delete_user'); ?>" class="btn btn-danger" /> <?php endif; ?>
                        <?php echo form_label($this->lang->line('hotel_admin_useredit_input_id'), 'input_id', array('class' => 'control-label')); ?>
                        <div class="controls">
                            <?php echo form_input(array('name' => 'input_id', 'class' => 'input-small', 'value' => $USERDATA->id, 'readonly' => 'readonly')); ?>
                        </div>
                    </div>
                    <div class="control-group">
                        <?php echo form_label($this->lang->line('hotel_admin_useredit_input_username'), 'input_username', array('class' => 'control-label')); ?>
                        <div class="controls">
                            <?php echo form_input(array('name' => 'input_username', 'style' => 'width:98%;', 'class' => 'input-xxlarge', 'value' => $USERDATA->username, 'readonly' => 'readonly')); ?>
                        </div>
                    </div>
                    <div class="control-group">
                        <?php echo form_label($this->lang->line('hotel_admin_useredit_input_password'), 'input_password', array('class' => 'control-label')); ?>
                        <div class="controls">
                            <?php echo form_input(array('name' => 'input_password', 'style' => 'width:98%;', 'title' => $this->lang->line('hotel_admin_useredit_input_password_description'), 'class' => 'input-xxlarge', 'value' => $USERDATA->password, 'readonly' => 'readonly')); ?>
                        </div>
                    </div>
                    <div class="control-group">
                        <?php echo form_label($this->lang->line('hotel_admin_useredit_input_reg_ip'), 'input_reg_ip', array('class' => 'control-label')); ?>
                        <div class="controls">
                            <?php echo form_input(array('name' => 'input_reg_ip', 'style' => 'width:98%;', 'class' => 'input-xxlarge', 'value' => $USERDATA->ip_reg, 'readonly' => 'readonly')); ?>
                        </div>
                    </div>
                    <div class="control-group">
                        <?php echo form_label($this->lang->line('hotel_admin_useredit_input_last_ip'), 'input_last_ip', array('class' => 'control-label')); ?>
                        <div class="controls">
                            <?php echo form_input(array('name' => 'input_last_ip', 'style' => 'width:98%;', 'class' => 'input-xxlarge', 'value' => $USERDATA->ip_last, 'readonly' => 'readonly')); ?>
                        </div>
                    </div>
                    <div class="control-group">
                        <?php echo form_label($this->lang->line('hotel_admin_useredit_input_email'), 'input_email', array('class' => 'control-label')); ?>
                        <div class="controls">
                            <?php echo form_input(array('type' => 'email', 'name' => 'input_email', 'style' => 'width:98%;', 'class' => 'input-xxlarge', 'value' => $USERDATA->mail)); ?>
                        </div>
                    </div>
                    <div class="control-group">
                        <?php echo form_label($this->lang->line('hotel_credits'), 'input_credits', array('class' => 'control-label')); ?>
                        <div class="controls">
                            <?php echo form_input(array('type' => 'number', 'min' => '0', 'max' => '2147483647', 'name' => 'input_credits', 'class' => 'input-large', 'value' => $USERDATA->credits)); ?>
                        </div>
                    </div>
                    <div class="control-group">
                        <?php echo form_label($this->lang->line('hotel_activity_points'), 'input_activity_points', array('class' => 'control-label')); ?>
                        <div class="controls">
                            <?php echo form_input(array('type' => 'number', 'min' => '0', 'max' => '2147483647', 'name' => 'input_activity_points', 'class' => 'input-large', 'value' => $USERDATA->activity_points)); ?>
                        </div>
                    </div>
                    <div class="control-group">
                        <?php echo form_label($this->lang->line('hotel_vip_points', $this->lang->line('hotel_plural')), 'input_vip_points', array('class' => 'control-label')); ?>
                        <div class="controls">
                            <?php echo form_input(array('type' => 'number', 'min' => '0', 'max' => '2147483647', 'name' => 'input_vip_points', 'class' => 'input-large', 'value' => $USERDATA->vip_points)); ?>
                        </div>
                    </div>
                </fieldset>
            </div>
        </div>
    </div>
    <div class="block span3">
        <div class="navbar navbar-inner block-header">
            <div class="muted pull-left"><i class="icon-user"></i> <?php echo $this->lang->line('hotel_admin_useredit_rank_and_subscriptions_settings'); ?></div>
        </div>
        <div class="block-content collapse in">
            <div class="span12">
                <fieldset>
                    <div class="control-group">
                        <?php echo form_label($this->lang->line('hotel_admin_useredit_dropdown_rank'), 'dropdown_rank', array('class' => 'control-label')); ?>
                        <div class="controls">
                            <?php echo form_dropdown('dropdown_rank', $GROUPS, $USERDATA->rank, 'style="width:98%;"'); ?>
                        </div>
                    </div>
                    <div class="control-group">
                        <?php echo form_label($this->lang->line('hotel_admin_useredit_checkbox_vip'), 'checkbox_vip', array('class' => 'control-label')); ?>
                        <label class="checkbox toggle candy">
                            <input id="view" value="set" <?php echo ($USERDATA->vip == '1') ? 'checked="checked"' : ''; ?> name="checkbox_vip" value="1" type="checkbox" />
                            <p>
                                <span><?php echo $this->lang->line('hotel_yes'); ?></span>
                                <span><?php echo $this->lang->line('hotel_no'); ?></span>
                            </p>

                            <a class="slide-button"></a>
                        </label>
                    </div>
                    <div class="control-group">
                        <?php echo form_label($this->lang->line('hotel_admin_useredit_checkbox_block_newfriends'), 'checkbox_block_newfriends', array('class' => 'control-label')); ?>
                        <label class="checkbox toggle candy">
                            <input id="view" value="set" <?php echo ($USERDATA->block_newfriends == '1') ? 'checked="checked"' : ''; ?> name="checkbox_block_newfriends" value="1" type="checkbox" />
                            <p>
                                <span><?php echo $this->lang->line('hotel_yes'); ?></span>
                                <span><?php echo $this->lang->line('hotel_no'); ?></span>
                            </p>

                            <a class="slide-button"></a>
                        </label>
                    </div>
                    <div class="control-group">
                        <?php echo form_label($this->lang->line('hotel_admin_useredit_checkbox_hide_online'), 'checkbox_hide_online', array('class' => 'control-label')); ?>
                        <label class="checkbox toggle candy">
                            <input id="view" value="set" <?php echo ($USERDATA->hide_online == '1') ? 'checked="checked"' : ''; ?> name="checkbox_hide_online" value="1" type="checkbox" />
                            <p>
                                <span><?php echo $this->lang->line('hotel_yes'); ?></span>
                                <span><?php echo $this->lang->line('hotel_no'); ?></span>
                            </p>

                            <a class="slide-button"></a>
                        </label>
                    </div>
                    <div class="control-group">
                        <?php echo form_label($this->lang->line('hotel_admin_useredit_checkbox_trading'), 'checkbox_trading', array('class' => 'control-label')); ?>
                        <label class="checkbox toggle candy">
                            <input id="view" value="set" <?php echo ($USERDATA->accept_trading == '1') ? 'checked="checked"' : ''; ?> name="checkbox_trading" value="1" type="checkbox" />
                            <p>
                                <span><?php echo $this->lang->line('hotel_yes'); ?></span>
                                <span><?php echo $this->lang->line('hotel_no'); ?></span>
                            </p>

                            <a class="slide-button"></a>
                        </label>
                    </div>
                </fieldset>
            </div>
        </div>
    </div>
    <div class="block span3">
        <div class="navbar navbar-inner block-header">
            <div class="muted pull-left"><i class="icon-user"></i> <?php echo $this->lang->line('hotel_admin_useredit_look'); ?></div>
        </div>
        <div class="block-content collapse in">
            <div class="span12">
                <img style="margin-left:25%;" src="http://www.habbo.de/habbo-imaging/avatarimage?figure=<?php echo $USERDATA->look; ?>&amp;direction=4&amp;head_direction=3&amp;gesture=sml&amp;action=wav&amp;size=l" />
            </div>
        </div>
    </div>
    <?php if($this->user_model->get_bans($USERDATA->username) != null): ?>
    <div class="block span6">
        <div class="navbar navbar-inner block-header">
            <div class="muted pull-left"><i class="icon-ban-circle"></i> <?php echo $this->lang->line('hotel_admin_useredit_bans'); ?></div>
        </div>
        <div class="block-content collapse in">
            <div class="span12">
                <table cellpadding="0" cellspacing="0" border="0" class="table table-bordered table-hover table-striped">
                    <thead>
                        <td><?php echo $this->lang->line('hotel_admin_useredit_bans_date'); ?></td>
                        <td><?php echo $this->lang->line('hotel_admin_useredit_bans_reason'); ?></td>
                        <td><?php echo $this->lang->line('hotel_admin_useredit_bans_added_by'); ?></td>
                        <td><?php echo $this->lang->line('hotel_admin_useredit_bans_expire'); ?></td>
                    </thead>
                    <tbody>
                        <?php foreach ($this->user_model->get_bans($USERDATA->username) As $row): ?>
                        <tr>
                            <td id="vcenter"><?php echo (is_numeric($row['added_date'])) ? date($this->config->item('hotel_default_datetime'), $row['added_date']) : $row['added_date'] ; ?></td>
                            <td id="vcenter"><strong><?php echo $row['reason']; ?></strong></td>
                            <td id="vcenter"><?php echo $row['added_by']; ?></td>
                            <td id="vcenter"><?php echo date($this->config->item('hotel_default_datetime'), $row['expire']); ?></td>
                        </tr>
                    <?php endforeach; ?>
                    </tbody>
                </table>
            </div>
        </div>
    </div>
    <?php endif; ?>
    <!-- /block -->
</div>
<div class="row-fluid">
    <div class="form-actions">
        <button type="reset" class="btn"><?php echo $this->lang->line('hotel_form_reset'); ?></button>
        <button type="submit" class="btn btn-primary"><?php echo $this->lang->line('hotel_admin_useredit_input_submit'); ?></button>
    </div>
</div>
<?php echo form_close(); ?>