<?php
/* * *******************************************\
  |****************** OPENCMS ******************|
  |*********************************************|
  |* @author Yannici                           *|
  |* @copyright Yannici                        *|
  |*********************************************|
  |* @since 04.11.2013                         *|
  \******************************************** */
?>
<link href="<?php echo $PATH; ?>/data-admin/assets/base.css" rel="stylesheet" media="screen">
<div class="row-fluid">
    <div class="block span12">
        <div class="navbar navbar-inner block-header">
            <div class="muted pull-left"><i class="icon-share"></i> <?php echo $this->lang->line('hotel_admin_backup'); ?></div>
        </div>
        <div class="block-content collapse in">
            <div class="span12">
                <a class="btn btn-info" href="<?php echo $PATH; ?>/openadmin/backup/do">
                    <?php echo $this->lang->line('hotel_admin_create_backup'); ?>
                </a>
            </div>
        </div>
    </div>
</div>
