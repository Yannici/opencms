<?php
/*********************************************\
|****************** OPENCMS ******************|
|*********************************************|
|* @author Yannici                           *|
|* @copyright Yannici                        *|
|*********************************************|
|* @since 04.11.2013                         *|
\*********************************************/
?>

<link href="<?php echo $PATH; ?>/data-admin/assets/base.css" rel="stylesheet" media="screen">
<div class="row-fluid">
    <div class="block span12">
        <div class="navbar navbar-inner block-header">
            <div class="muted pull-left"><i class="icon-search"></i> <?php echo $this->lang->line('hotel_admin_clonecheck'); ?></div>
        </div>
        <div class="block-content collapse in">

            <a href="<?php echo $PATH; ?>/openadmin/clonecheck" class="btn btn-danger">
                <?php echo $this->lang->line('hotel_back'); ?>
            </a><br /><br />
            <div class="span12">
                <table width="100%" cellpadding="0" cellspacing="0" border="0" class="table table-bordered table-hover table-striped display" style="width:100%;" id="news">
                    <thead>
                        <tr>
                            <th><?php echo $this->lang->line('hotel_identification'); ?></th>
                            <th><?php echo $this->lang->line('hotel_username'); ?></th>
                            <th><?php echo $this->lang->line('hotel_admin_members_login_ipaddress'); ?></th>
                            <th><?php echo $this->lang->line('hotel_admin_members_register_ipaddress'); ?></th>
                            <th><?php echo $this->lang->line('hotel_admin_members_last_online'); ?></th>
                            <th><?php echo $this->lang->line('hotel_online') . '?'; ?></th>
                            <th><?php echo $this->lang->line('hotel_admin_actions'); ?></th>
                        </tr>
                    </thead>
                    <tbody>
                        <?php foreach($CLONES As $clone): ?>
                        <tr>
                            <td><?php echo $clone['id']; ?></td>
                            <td><strong><?php echo $clone['username']; ?></strong></td>
                            <td><strong><?php echo $clone['ip_last']; ?></strong></td>
                            <td><strong><?php echo $clone['ip_reg']; ?></strong></td>
                            <td><?php echo date($this->config->item('hotel_default_datetime'), $clone['last_online']); ?></td>
                            <td><img src="<?php echo $PATH; ?>/data/img/<?php echo ($clone['online'] == '1') ? 'online' : 'offline'; ?>.gif" /></td>
                            <td><?php echo form_open('openadmin/useredit/edit/' . $clone['id'], array('style' => 'float:left;margin-right:5px;')); ?><button type="submit" class="btn btn-warning"><i class="icon-pencil"></i></button><?php echo form_close(); ?>
                        </tr>
                        <?php endforeach; ?>
                    </tbody>
                </table>
            </div>
            
        </div>
    </div>
</div>