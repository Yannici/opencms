<?php
/*********************************************\
|****************** OPENCMS ******************|
|*********************************************|
|* @author Yannici                           *|
|* @copyright Yannici                        *|
|*********************************************|
|* @since 04.11.2013                         *|
\*********************************************/
?>
<link type="text/css" rel="stylesheet" href="<?php echo $PATH; ?>/data-admin/vendors/datatables/css/jquery.dataTables.css" />
<link href="<?php echo $PATH; ?>/data-admin/assets/base.css" rel="stylesheet" media="screen" />
<div class="row-datatable">
    <script type="text/javascript">
        $(document).ready(function() {
            $('#members').dataTable({
                "oLanguage": {
                    "sUrl": "<?php echo $PATH; ?>/tabledata/get_translations"
                },
                "bProcessing": true,
                "bServerSide": true,
                "sServerMethod": "GET",
                "sAjaxSource": "<?php echo $PATH; ?>/tabledata/get_users",
                "iDisplayLength": 50,
                "sPaginationType": "full_numbers",
                "aLengthMenu": [[10, 25, 50, -1], [10, 25, 50, 100]],
                "aaSorting": [[0, 'asc']],
                "aoColumns": [
                    { "bVisible": true, "bSearchable": true, "bSortable": true },
                    { "bVisible": true, "bSearchable": true, "bSortable": true },
                    { "bVisible": true, "bSearchable": true, "bSortable": true },
                    { "bVisible": true, "bSearchable": true, "bSortable": true },
                    { "bVisible": true, "bSearchable": true, "bSortable": true },
                    { "bVisible": true, "bSearchable": true, "bSortable": true },
                    { "bVisible": true, "bSearchable": false, "bSortable": true },
                    { "bVisible": true, "bSearchable": false, "bSortable": false },
                    { "bVisible": true, "bSearchable": false, "bSortable": false }
                ]
            }).fnSetFilteringDelay(700);
        });
    </script>
    <div class="block span12">
        <div class="navbar navbar-inner block-header">
            <div class="muted pull-left"><i class="icon-list"></i> <?php echo $this->lang->line('hotel_admin_members'); ?></div>
        </div>
        <div class="block-content collapse in">
            <div class="span12">
                <table cellpadding="0" cellspacing="0" border="0" class="table table-bordered table-hover table-striped" id="members">
                    <thead>
                        <tr>
                            <th><?php echo $this->lang->line('hotel_identification'); ?></th>
                            <th><?php echo $this->lang->line('hotel_username'); ?></th>
                            <th><?php echo $this->lang->line('hotel_email'); ?></th>
                            <th><?php echo $this->lang->line('hotel_admin_members_rank'); ?></th>
                            <th><?php echo $this->lang->line('hotel_admin_members_login_ipaddress'); ?></th>
                            <th><?php echo $this->lang->line('hotel_admin_members_register_ipaddress'); ?></th>
                            <th><?php echo $this->lang->line('hotel_admin_members_last_online'); ?></th>
                            <th><?php echo $this->lang->line('hotel_online'); ?>?</th>
                            <th><?php echo $this->lang->line('hotel_admin_actions'); ?></th>
                        </tr>
                    </thead>
                    <tbody></tbody>
                    <tfoot></tfoot>
                </table>
            </div>
        </div>
    </div>
</div>