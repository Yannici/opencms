<?php
/*********************************************\
|****************** OPENCMS ******************|
|*********************************************|
|* @author Yannici                           *|
|* @copyright Yannici                        *|
|*********************************************|
|* @since 04.11.2013                         *|
\*********************************************/
?>

<link href="<?php echo $PATH; ?>/data-admin/assets/base.css" rel="stylesheet" media="screen">
<div class="row-fluid">
    <div class="block span12">
        <div class="navbar navbar-inner block-header">
            <div class="muted pull-left"><i class="icon-ban-circle"></i> <?php echo $this->lang->line('hotel_admin_permissions_edit'); ?></div>
        </div>
        <div class="block-content collapse in">
            <a class="btn btn-danger" href="<?php echo $PATH; ?>/openadmin/permissions">
                <?php echo $this->lang->line('hotel_back'); ?>
            </a><br /><br />
            <div class="span12">
                <?php echo form_open('openadmin/permissions/edit_submit', array('class' => 'form-horizontal')); ?>
                <?php echo form_hidden('input_rank', $RANK); ?>
                <fieldset>
                    <legend>
                        <?php echo $this->employee_model->get_rank_name($RANK); ?>
                    </legend>
                    <?php foreach ($PERMISSIONS As $site): ?>
                        <div class="permission">
                            <?php echo $site; ?>
                            <label class="checkbox toggle candy">
                                <input id="view" value="set" <?php echo ($this->permission_model->have_permission($RANK, $site)) ? 'checked="checked"' : ''; ?> name="permission[<?php echo $site; ?>]" value="1" type="checkbox" />
                                <p>
                                    <span><?php echo $this->lang->line('hotel_yes'); ?></span>
                                    <span><?php echo $this->lang->line('hotel_no'); ?></span>
                                </p>

                                <a class="slide-button"></a>
                            </label>
                        </div>
                    <?php endforeach; ?>
                    <br clear="all" />
                </fieldset>
                <div class="form-actions">
                    <button type="reset" class="btn"><?php echo $this->lang->line('hotel_form_reset'); ?></button>
                    <button type="submit" class="btn btn-primary"><?php echo $this->lang->line('hotel_admin_permissions_submit_input'); ?></button>
                </div>
                <?php echo form_close(); ?>
            </div>
        </div>
    </div>
</div>