<?php
/*********************************************\
|****************** OPENCMS ******************|
|*********************************************|
|* @author Yannici                           *|
|* @copyright Yannici                        *|
|*********************************************|
|* @since 04.11.2013                         *|
\*********************************************/
?>

<link href="<?php echo $PATH; ?>/data-admin/assets/base.css" rel="stylesheet" media="screen">
<link type="text/css" rel="stylesheet" href="<?php echo $PATH; ?>/data-admin/vendors/datatables/css/jquery.dataTables.css" />
<div class="row-datatable">
    <div class="block span12">
        <div class="navbar navbar-inner block-header">
            <div class="muted pull-left"><i class="icon-info-sign"></i> <?php echo $this->lang->line('hotel_admin_news'); ?></div>
        </div>
        <div class="block-content collapse in">

            <a href="<?php echo $PATH; ?>/openadmin/news/add" class="btn btn-success">
                <i class="icon-plus"></i> <?php echo $this->lang->line('hotel_admin_news_add'); ?>
            </a><br /><br />
            <div class="span12">
                <script type="text/javascript">
                    $(document).ready(function() {
                        $('#news').dataTable({
                            "oLanguage": {
                                "sUrl": "<?php echo $PATH; ?>/tabledata/get_translations"
                            },
                            "bProcessing": true,
                            "bServerSide": true,
                            "sServerMethod": "GET",
                            "sAjaxSource": "<?php echo $PATH; ?>/tabledata/get_news",
                            "iDisplayLength": 50,
                            "sPaginationType": "full_numbers",
                            "aLengthMenu": [[10, 25, 50, -1], [10, 25, 50, 100]],
                            "aaSorting": [[4, 'desc']],
                            "aoColumns": [
                                {"bVisible": true, "bSearchable": false, "bSortable": true},
                                {"bVisible": true, "bSearchable": true, "bSortable": true},
                                {"bVisible": true, "bSearchable": true, "bSortable": true},
                                {"bVisible": true, "bSearchable": true, "bSortable": true},
                                {"bVisible": true, "bSearchable": false, "bSortable": true},
                                {"bVisible": true, "bSearchable": false, "bSortable": false},
                                {"bVisible": true, "bSearchable": false, "bSortable": false}
                            ]
                        }).fnSetFilteringDelay(700);
                    });
                </script>
                <table width="100%" cellpadding="0" cellspacing="0" border="0" class="table table-bordered table-hover table-striped display" style="width:100%;" id="news">
                    <thead>
                        <tr>
                            <th><?php echo $this->lang->line('hotel_identification'); ?></th>
                            <th><?php echo $this->lang->line('hotel_admin_news_title'); ?></th>
                            <th><?php echo $this->lang->line('hotel_admin_news_short_story'); ?></th>
                            <th><?php echo $this->lang->line('hotel_admin_news_author'); ?></th>
                            <th><?php echo $this->lang->line('hotel_admin_news_published'); ?></th>
                            <th><?php echo $this->lang->line('hotel_admin_news_image'); ?></th>
                            <th><?php echo $this->lang->line('hotel_admin_actions'); ?></th>
                        </tr>
                    </thead>
                    <tbody></tbody>
                    <tfoot></tfoot>
                </table>
            </div>
        </div>
    </div>
</div>