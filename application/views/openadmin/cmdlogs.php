<?php
/*********************************************\
|****************** OPENCMS ******************|
|*********************************************|
|* @author Yannici                           *|
|* @copyright Yannici                        *|
|*********************************************|
|* @since 04.11.2013                         *|
\*********************************************/
?>

<link href="<?php echo $PATH; ?>/data-admin/assets/base.css" rel="stylesheet" media="screen">
<div class="row-fluid">
    <div class="block span12">
        <div class="navbar navbar-inner block-header">
            <div class="muted pull-left"><i class="icon-search"></i> <?php echo $this->lang->line('hotel_admin_cmdlogs'); ?></div>
        </div>
        <div class="block-content collapse in">
            <div class="span12">
                <?php echo form_open('openadmin/cmdlogs/show', array('class' => 'form-horizontal')); ?>
                <fieldset>
                    <legend><?php echo $this->lang->line('hotel_admin_cmdlogs_search'); ?></legend>
                    <div class="control-group">
                        <?php echo form_label($this->lang->line('hotel_admin_cmdlogs_input_cmdlogs'), 'input_cmdlogs', array('class' => 'control-label')); ?>
                        <div class="controls">
                            <?php echo form_input(array('name' => 'input_cmdlogs', 'class' => 'input-xlarge')); ?>
                            <p class="help-block"><?php echo $this->lang->line('hotel_admin_cmdlogs_input_cmdlogs_description'); ?></p>
                        </div>
                    </div>
                    <div class="control-group">
                        <?php echo form_label($this->lang->line('hotel_admin_chatlogs_dropdown_cmdlogstype'), 'dropdown_cmdlogstype', array('class' => 'control-label')); ?>
                        <div class="controls">
                            <?php echo form_dropdown('dropdown_cmdlogstype', array('user' => $this->lang->line('hotel_user'), 'command' => $this->lang->line('hotel_admin_cmdlogs_command')), 'user'); ?>
                            <p class="help-block"><?php echo $this->lang->line('hotel_admin_chatlogs_dropdown_cmdlogstype_description'); ?></p>
                        </div>
                    </div>
                    <div class="form-actions">
                        <button type="reset" class="btn"><?php echo $this->lang->line('hotel_form_reset'); ?></button>
                        <button type="submit" class="btn btn-primary"><?php echo $this->lang->line('hotel_admin_chatlogs_submit_input'); ?></button>
                    </div>
                </fieldset>
                <?php echo form_close(); ?>
            </div>
        </div>
    </div>
</div>