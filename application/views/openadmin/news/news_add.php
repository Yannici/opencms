<?php
/*********************************************\
|****************** OPENCMS ******************|
|*********************************************|
|* @author Yannici                           *|
|* @copyright Yannici                        *|
|*********************************************|
|* @since 04.11.2013                         *|
\*********************************************/
?>

<link href="<?php echo $PATH; ?>/data-admin/assets/base.css" rel="stylesheet" media="screen">
<script type="text/javascript">
    $(document).ready(function() {
       $('.imagepreview').click(function() {
          var value = $('input[name="input_image"]').val();
          if(value !== '') {
              window.open(value, '_blank');
          }
       });
       
       tinymce.init({
            selector: ".textarea",
            width: "800px",
            force_br_newlines : false,
            force_p_newlines : false,
            forced_root_block : 'p',
            plugins: [
                "advlist autolink lists link image charmap print preview anchor",
                "searchreplace visualblocks code fullscreen",
                "insertdatetime media table contextmenu paste"
            ],
            toolbar: "insertfile undo redo | styleselect | bold underline italic | alignleft aligncenter alignright alignjustify | bullist numlist outdent indent | link image"
        });
    });
</script>
<div class="row-fluid">
    <div class="block span12">
        <div class="navbar navbar-inner block-header">
            <div class="muted pull-left"><i class="icon-info-sign"></i> <?php echo $this->lang->line('hotel_admin_news'); ?></div>
        </div>
        <div class="block-content collapse in">
            <div class="span12">
                <?php echo form_open('openadmin/news/add_submit', array('class' => 'form-horizontal')); ?>
                <fieldset>
                    <legend><?php echo $this->lang->line('hotel_admin_news_add'); ?></legend>
                    <div class="control-group">
                        <?php echo form_label($this->lang->line('hotel_admin_news_title'), 'input_title', array('class' => 'control-label')); ?>
                        <div class="controls">
                            <?php echo form_input(array('name' => 'input_title', 'class' => 'input-xlarge', 'maxlength' => '40', 'value' => set_value('input_title'))); ?>
                        </div>
                    </div>
                    <div class="control-group">
                        <?php echo form_label($this->lang->line('hotel_admin_news_short_story'), 'input_shortstory', array('class' => 'control-label')); ?>
                        <div class="controls">
                            <?php echo form_input(array('name' => 'input_shortstory', 'class' => 'input-xlarge', 'maxlength' => '70', 'value' => set_value('input_shortstory'))); ?>
                        </div>
                    </div>
                    <div class="control-group">
                        <?php echo form_label($this->lang->line('hotel_admin_news_image'), 'input_image', array('class' => 'control-label')); ?>
                        <div class="controls">
                            <?php echo form_input(array('name' => 'input_image', 'class' => 'input-xlarge', 'maxlength' => '255', 'value' => set_value('input_image'))); ?> <a href="javascript:;" class="imagepreview"><i class="icon-search"></i></a>
                        </div>
                    </div>
                    <div class="control-group">
                        <?php echo form_label($this->lang->line('hotel_admin_news_long_story'), 'textarea_longstory', array('class' => 'control-label')); ?>
                        <div class="controls">
                            <?php echo form_textarea(array('name' => 'textarea_longstory', 'class' => 'input-xlarge textarea', 'value' => set_value('textarea_longstory'))); ?>
                        </div>
                    </div>
                    <div class="form-actions">
                        <button type="reset" class="btn"><?php echo $this->lang->line('hotel_form_reset'); ?></button>
                        <button type="submit" class="btn btn-primary"><?php echo $this->lang->line('hotel_admin_news_submit'); ?></button>
                    </div>
                </fieldset>
                <?php echo form_close(); ?>
            </div>
        </div>
    </div>
</div>