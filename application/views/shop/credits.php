<?php
/* * *******************************************\
  |****************** OPENCMS ******************|
  |*********************************************|
  |* @author Yannici                           *|
  |* @copyright Yannici                        *|
  |*********************************************|
  |* @since 04.11.2013                         *|
  \******************************************** */
?>

<div class="row" style="margin-top:-50px;">
    <div class="span12-hero">
        <div class="box">
            <div class="header red">
                <?php echo $this->lang->line('hotel_shop_credits'); ?>
            </div>
            <div class="content">

                <h1 class="overhead"><?php echo $this->lang->line('hotel_shop_what_can_i_do'); ?></h1>

                <div class="thumb">
                    <div class="thumbnail-credits first">
                        <div class="caption">
                            <img class="capimage" align="right" src="<?php echo $PATH; ?>/data/img/story_trade.png" />
                            <h3><?php echo $this->lang->line('hotel_shop_thumbnail_label_1') ?></h3>
                            <p><?php echo $this->lang->line('hotel_shop_thumbnail_text_1') ?></p>
                            <br clear="all" />
                        </div>
                    </div>
                </div>

                <div class="thumb">
                    <div class="thumbnail-credits">
                        <div class="caption">
                            <img class="capimage" align="right" src="<?php echo $PATH; ?>/data/img/story_build.gif" />
                            <h3><?php echo $this->lang->line('hotel_shop_thumbnail_label_2') ?></h3>
                            <p><?php echo $this->lang->line('hotel_shop_thumbnail_text_2') ?></p>
                            <br clear="all" />
                        </div>
                    </div>
                </div>

                <div class="thumb">
                    <div class="thumbnail-credits last">
                        <div class="caption">
                            <img class="capimage" align="right" src="<?php echo $PATH; ?>/data/img/story_manycredits.png" />
                            <h3><?php echo $this->lang->line('hotel_shop_thumbnail_label_3') ?></h3>
                            <p><?php echo $this->lang->line('hotel_shop_thumbnail_text_3') ?></p>
                            <br clear="all" />
                        </div>
                    </div>
                </div>

            </div>
        </div>
    </div>
</div>