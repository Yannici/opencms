<?php
/*********************************************\
|****************** OPENCMS ******************|
|*********************************************|
|* @author Yannici                           *|
|* @copyright Yannici                        *|
|*********************************************|
|* @since 04.11.2013                         *|
\*********************************************/


abstract class Abstract_admin_plugin extends Abstract_plugin {
    
    // You can forward to a custom view in the plugin-folder by setting 'CUSTOM_FORWARDING' in the data-array.
    public function view($data) {
        $data['PATH'] = $this->config->item('path');
        
        $data['WORKERS_ONLINE'] = $this->core_model->get_count_online_workers();
        $data['USER_ONLINE'] = $this->core_model->get_count_online_users();
        $data['USER_RECORD'] = $this->core_model->get_user_record();
        $data['USER_TOTAL'] = $this->core_model->get_user_count();
        
        if(!file_exists(APPPATH . '/views/openadmin/plugins/' . strtolower($this->get_plugin_technical_name()) . '/' . strtolower($this->get_plugin_technical_name()) . '.php')) {
            show_404();
        }
        
        $data['title'] = $this->get_plugin_name(); 
       
        $this->load->view('openadmin-templates/header', $data);
        $this->load->view('openadmin/plugins/' . strtolower($this->get_plugin_technical_name()) . '/' . (( ! isset($data['CUSTOM_FORWARDING'])) ? strtolower($this->get_plugin_technical_name()) : $data['CUSTOM_FORWARDING']), $data);
        $this->load->view('openadmin-templates/footer');
    }
    
}
?>
