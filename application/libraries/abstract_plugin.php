<?php
/*********************************************\
|****************** OPENCMS ******************|
|*********************************************|
|* @author Yannici                           *|
|* @copyright Yannici                        *|
|*********************************************|
|* @since 04.11.2013                         *|
\*********************************************/


abstract class Abstract_plugin {
    
    public abstract function get_plugin_name();
    public abstract function get_plugin_technical_name();
    public abstract function get_plugin_description();
    public abstract function get_plugin_author();
    public abstract function get_plugin_version();
    
    public function action() {
        $this->view('404');
    }
    
    // You can forward to a custom view in the plugin-folder by setting 'CUSTOM_FORWARDING' in the data-array.
    public function view($data) {
        if($data == '404') {
            show_404();
        }
        
        if( ! is_array($data)) {
            $data = array('data' => $data);
        }
        
        $data['PATH'] = $this->config->item('path');
        
        if(!file_exists(APPPATH . '/views/plugins/' . strtolower($this->get_plugin_technical_name()) . '/' . strtolower($this->get_plugin_technical_name()) . '.php')) {
            show_404();
        }
        
        $data['title'] = ucfirst($this->get_plugin_name()); 
       
        $this->load->view('templates/header', $data);
        $this->load->view('plugins/' . strtolower($this->get_plugin_technical_name()) . '/' . (( ! isset($data['CUSTOM_FORWARDING'])) ? strtolower($this->get_plugin_technical_name()) : $data['CUSTOM_FORWARDING']), $data);
        $this->load->view('templates/footer');
    }
    
    public function __get($key) {
        if(property_exists($this, $key)) {
            return $this->$key;
        }
        
        $CI =& get_instance();
        return $CI->$key;
    }
}

?>
