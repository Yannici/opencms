<?php
/*********************************************\
|****************** OPENCMS ******************|
|*********************************************|
|* @author Yannici                           *|
|* @copyright Yannici                        *|
|*********************************************|
|* @since 04.11.2013                         *|
\*********************************************/

class Plugin_system {
    
    private $_plugins = array();
    
    public function __construct() {
        $this->load_abstract_class();
        $this->load_plugins();
    }
    
    private function load_abstract_class() {
        require_once(APPPATH . '/libraries/abstract_plugin.php');
        require_once(APPPATH . '/libraries/abstract_admin_plugin.php');
    }
    
    public function load_plugin($name) {
        $ref_class = new ReflectionClass(ucfirst($name . '_plugin'));
        return $ref_class->newInstance();
    }
    
    private function load_plugin_array($arr, $admin = FALSE) {
        foreach($arr As $plugin => $class) {
            if( ! file_exists(APPPATH . '/plugins/' . $plugin . '/' . $plugin . '.php')) {
                show_error('Unable to load your plugin \'' . $plugin . '\'. Please check if this plugin exists.');
            } else {
                require_once(APPPATH . '/plugins/' . $plugin . '/' . $plugin . '.php');
                if( ! class_exists($plugin . '_plugin')
                   OR (get_parent_class($plugin . '_plugin') !== 'Abstract_plugin' && get_parent_class($plugin . '_plugin') !== 'Abstract_admin_plugin')) {
                    
                    show_error('Unable to load your plugin \'' . $plugin . '\'. Please check if the plugin is a class and Abstract_plugin or Abstract_admin_plugin is it parent');
                } else {
                    $this->_plugins[strtolower($plugin)] = array();
                    $this->_plugins[strtolower($plugin)]['class'] = ($class == '' && !$admin) ? 'me' : (($admin) ? 'openadmin' : strtolower($class));
                    $this->_plugins[strtolower($plugin)]['type']  = ($admin) ? 'admin' : 'home';
                    
                    $this->set_plugin_properties(strtolower($plugin));
                }
            }
        }
    }
    
    private function load_plugins() {
        $this->load_plugin_array($this->config->item('plugins'));
        $this->load_plugin_array($this->config->item('admin_plugins'), TRUE);
    }
    
    private function set_plugin_properties($name) {
        $plugin = $this->load_plugin($name);
        
        $this->_plugins[$name]['technical_name'] = $plugin->get_plugin_technical_name();
        $this->_plugins[$name]['name'] = $plugin->get_plugin_name();
        $this->_plugins[$name]['description'] = $plugin->get_plugin_description();
        $this->_plugins[$name]['author'] = $plugin->get_plugin_author();
        $this->_plugins[$name]['version'] = $plugin->get_plugin_version();
    }
    
    public function is_registered_plugin($name, $class, $admin) {
        return (isset($this->_plugins[$name]) 
           && ($this->_plugins[$name]['class'] == strtolower($class))
           && (($this->_plugins[$name]['type'] == 'admin' && $admin) || !$admin));
    }

    public function __get($key) {
        if(property_exists($this, $key)) {
            return $this->$key;
        }
        
        $CI =& get_instance();
        return $CI->$key;
    }
}
?>
