<?php
/*********************************************\
|****************** OPENCMS ******************|
|*********************************************|
|* @author Yannici                           *|
|* @copyright Yannici                        *|
|*********************************************|
|* @since 04.11.2013                         *|
\*********************************************/

class User {
    
    private $id = null;
    private $username = 'Unbekannter';
    public $password = 'secret';
    public $mail = 'unbekannter@user.de';
    private $auth_ticket = null;
    private $rank = 1;
    private $credits = 10000;
    private $vip_points = 0;
    private $activity_points = 0;
    public $look = 'hr-115-42.hd-190-1.ch-215-62.lg-285-91.sh-290-62';
    private $gender = 'M';
    public $motto = 'Unbekannter User';
    public $online = '0';
    public $ip_last = '0.0.0.0';
    public $ip_reg = '0.0.0.0';
    private $home_room = null;
    private $respect = 0;
    public $block_newfriends = '0';
    public $hide_online = '0';
    private $hide_inroom = '0';
    public $vip = '0';
    public $volume = 100;
    public $accept_trading = '1';
    private $account_created = 0;
    private $last_online = 0;
    
    public function __construct($default = TRUE) {
        if($default) {
            $this->load->database();
            $this->load->model('core_model');
            
            if($this->logged_in()) {
                if($this->check($this->session->userdata('username'), $this->session->userdata('password'))) {
                    $this->set_user_by_username($this->session->userdata('username'));
                }
            }
        }
    }
    
    public static function get_username_by_id($id) {
        $CI =& get_instance();
        
        $CI->db->select('username');
        $result = $CI->db->get_where('users', array('id' => $id), 1);
        if(!$result || $CI->db->count_all_results() == 0) {
            return $CI->lang->line('hotel_unknown_user');
        }
        
        $r = $result->result_array(); // CAUSE OF OLD PHP-VERSION
		
		if(count($r) == 0) return '*UNKNOWN*';
		
        return $r[0]['username'];
    }
    
    public static function get_id_by_username($username) {
        $CI =& get_instance();
        
        $CI->db->select('id');
        $result = $CI->db->get_where('users', array('username' => $username), 1);
        if(!$result || $CI->db->count_all_results() == 0) {
            return 0;
        }
        
        $r = $result->result_array(); // CAUSE OF OLD PHP-VERSION
        return $r[0]['id'];
    }
    
    public function set_user_by_id($id) {
        $result = $this->db->get_where('users', array('id' => $id), 1);
        $array = $result->result_array();

        if(count($array) == 0) {
            return FALSE;
        }
        
        foreach($array[0] As $key => $value) {
            if(property_exists($this, $key)) {
                $this->$key = $value;
            }
        }
        
        return TRUE;
    }
    
    public function set_user_by_username($username) {
        $result = $this->db->get_where('users', array('username' => $username), 1);

        if(!$result || $this->db->count_all_results() == 0) {
            return false;
        }
        
        $array = $result->result_array();
        foreach($array[0] As $key => $value) {
            if(property_exists($this, $key)) {
                $this->$key = $value;
            }
        }
        
        return true;
    }
    
    public function is_guest() {
        return ($this->id == null || $this->id == 0);
    }
    
    public function logged_in() {
        return ($this->session->userdata('userid') === FALSE ||
           $this->session->userdata('username') === FALSE ||
           $this->session->userdata('password') === FALSE) ? FALSE : TRUE;
    }
    
    public function admin_logged_in() {
        return (!$this->logged_in() || $this->session->userdata('admin') === FALSE) ? FALSE : TRUE;
    }
    
    public function admin_security_check() {
        return ($this->session->userdata('security') === $this->config->item('admin_security_code'));
    }
    
    public function check($username, $password) {
        $this->db->limit(1);
        $this->db->where(array('LOWER(username)' => strtolower($username), 'password' => $password));
        $result = $this->db->get('users');
        
        if(!$result || count($result->result_array()) == 0) {
            return FALSE;
        }
        $this->db->flush_cache();

        $bans_user = count($this->db->get_where('bans', array('LOWER(value)' => strtolower($username), 'expire >=' => time(), 'bantype' => 'user'))->result_array());
        $bans_ip = count($this->db->get_where('bans', array('LOWER(value)' => $this->input->ip_address(), 'expire >=' => time(), 'bantype' => 'ip'))->result_array());
        if($bans_user > 0 || $bans_ip > 0) {
            return FALSE;
        }
        $this->db->flush_cache();

        return TRUE;
    }
    
    public function correct() {
        if($this->is_guest()) {
            return FALSE;
        }
        
        if($this->session->userdata('userid') !== $this->id) {
            return FALSE;
        }
        
        if($this->session->userdata('username') !== $this->username) {
            return FALSE;
        }
        
        if($this->session->userdata('password') !== $this->password) {
            return FALSE;
        }
        
        return TRUE;
    }
    
    public function edit_password($password) {
        $this->db->where('id', $this->id);
        $this->user->password = $this->core_model->open_hash($password, $this->username);
        $this->session->unset_userdata('password');
        $this->session->set_userdata('password', $this->user->password);
        return $this->store();
    }
    
    public function admin_correct() {
        if(!$this->correct()) {
            return FALSE;
        }
        
        if($this->rank < $this->config->item('admin_min_rank')) {
            return FALSE;
        }
        
        return TRUE;
    }
    
    public function __get($key) {
        if(property_exists($this, $key)) {
            return $this->$key;
        }
        
        $CI =& get_instance();
        return $CI->$key;
    }
    
    public function store() {
        // CAN HANDLE OBJECTS
        $this->db->where('id', $this->id);
        return $this->db->update('users', $this);
    }
}
?>
