<?php
/*********************************************\
|****************** OPENCMS ******************|
|*********************************************|
|* @author Yannici                           *|
|* @copyright Yannici                        *|
|*********************************************|
|* @since 04.11.2013                         *|
\*********************************************/

if (!defined('BASEPATH'))
    exit('No direct script access allowed');

// GENERAL SETTINGS
$config['path'] = 'http://localhost/opencms'; // WITHOUT SLASH AT THE END
$config['hotel_name'] = 'Open Hotel';
$config['short_hotel_name'] = 'Open';

// REGISTER SETTINGS
$config['register_credits'] = 50000;
$config['register_pixels'] = 1000;
$config['register_vip_points'] = 1;

// UPDATE-CONFIGURATION
// TRUE OR FALSE TO CONFIGURE, IF IT SHOULD CHECK FOR UPDATES
$config['check_for_updates'] = TRUE;
// LEAVE EMPTY TO CHECK FOR UPDATES WITHOUT PROXY
$config['update_proxy'] = '';
// LEAVE USERNAME EMPTY TO CONNECT WITH PROXY WITHOUT AUTHENTICATION
$config['update_proxy_username'] = '';
$config['update_proxy_password'] = '';

// NEWS-SETTINGS
// Comments per page on news
$config['news_comments_per_page'] = 5;

// GLOBALIZATION
$config['hotel_default_date'] = 'd.m.Y'; // SEE (English): http://php.net/manual/en/function.date.php
$config['hotel_default_datetime'] = 'd.m.Y H:i'; // SEE (German): http://php.net/manual/de/function.date.php
$config['hotel_default_datetimesecond'] = 'd.m.Y H:i:s';
$config['hotel_default_time'] = 'H:i';

// MUS-CONNECTION
$config['mus_host'] = '127.0.0.1';
$config['mus_port'] = '30001';
// Here you can deactivate and activate MUS commands
$config['mus_activated'] = TRUE;

// OPEN-ADMIN
// At what rank users can login in open admin
$config['admin_min_rank'] = 11;
// The security code which should only known by the hotel staffs/employee
$config['admin_security_code'] = 'CHANGETHIS';

/**
 * HOTEL-KONFIGURATION
 */
// ID of the welcome room
$config['hotel_welcome_lounge_id'] = '0';

$config['hotel_host'] = 'YOURHOSST';
$config['hotel_port'] = '3000';

$config['hotel_webbuild'] = '61_c0f99b7b02ed27ad5d4d5661fe02784f';
$config['hotel_external_variables'] = 'EXTERNAL_VARIABLES';
$config['hotel_external_flash_texts'] = 'FLASH_TEXTS';
$config['hotel_external_productdata'] = 'PRODUCT_DATA';
$config['hotel_external_furnidata'] = 'FURNIDATA';
$config['hotel_flash_client_url'] = 'FLASH_CLIENT_URL';
$config['hotel_swf_folder'] = 'SWF_FOLDER';
$config['hotel_swf'] = 'SWF_FILE';

// NAVIGATION
/**
 * Explaination:
 * One Array-Element stands for one navigation element.
 * 
 * Key:
 * In the Key you can define the display name of the navigation element
 * For that you can use language keys. To give out a language item just write 'lang:' for marking
 * and after that the language key.
 * 
 * Value:
 * The value defines the forwarding. For example 'home' forwards you to 'hotel.com/home'
 * If you want to display a dropdown, you just have to put a array instead of a string to the value.
 * In a dropdown you have to inverse key and value. So display name in the value and forwarding in the key.
 */
$config['hotel_navigation'] = array(
    'lang:hotel_me' => array(
        'me' => 'lang:hotel_home',
        'profile/settings' => 'lang:hotel_settings',
        'logout' => 'lang:hotel_logout'),
    'lang:hotel_community' => array(
        'news' => 'lang:hotel_news_news',
        'community/employee' => 'lang:hotel_community_employee'
    ),
    'lang:hotel_shop_credits' => 'shop/credits'
);

/**
 * Explaination:
 * One Array-Element stands for one navigation element.
 * 
 * Key:
 * In the key you can define the display name of the navigation element
 * For that you can use language keys. To give out a language item just write 'lang:' for marking
 * and after that the language key.
 * After the display name you can add a icon-class by seperating the key with a semicolon.
 * 
 * Value:
 * The value defines the forwarding. For example 'dashboard' forwards you to 'hotel.com/openadmin/dashboard'
 * If you want to display a dropdown, you just have to put a array instead of a string to the value.
 * In a dropdown you have to inverse key and value. So display name in the value and forwarding in the key.
 */
$config['hotel_admin_navigation'] = array(
    'lang:hotel_admin_dashboard;icon-home' => 'dashboard',
    'lang:hotel_admin_manage;icon-pencil' => array(
        'news' => 'lang:hotel_admin_news',
        'employee_ranking' => 'lang:hotel_admin_employee_ranking',
        'password_tool' => 'lang:hotel_admin_password_tool',
        'permissions' => 'lang:hotel_admin_permissions'
    ),
    'lang:hotel_admin_members;icon-user' => array(
        'members' => 'lang:hotel_admin_members',
        'useredit' => 'lang:hotel_admin_useredit',
        'ban' => 'lang:hotel_admin_ban',
        'clonecheck' => 'lang:hotel_admin_clonecheck'
    ),
    'lang:hotel_admin_logs;icon-list' => array(
        'chatlogs' => 'lang:hotel_admin_chatlogs',
        'cmdlogs' => 'lang:hotel_admin_cmdlogs',
        'backup' => 'lang:hotel_admin_backup'
    )
);

// PLUGINS
/**
 * Explaination:
 * 
 * The Key of the Array-Element:
 * The Key of one Array-Element have to be a Technical-Plugin-Name of your plugin.
 * 
 * The Value:
 * The Value is the Controller name .
 * Leave it empty, if you don't want to open your plugin over 
 * eg. hotel.com/community/pluginname, but eg. hotel.com/pluginname
 */
$config['plugins'] = array(
    'employee' => 'community'
);

/**
 * Explaination:
 * 
 * Here you just have to set your Plugin-Technical-Name as key.
 * So, set the key and leave the value empty. Like the example below.
 */
$config['admin_plugins'] = array(
    'ban' => ''
);
?>