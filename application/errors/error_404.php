<!DOCTYPE html>
<html lang="en">
<head>
<title>404 Page Not Found</title>
<style type="text/css">

::selection{ background-color: #E13300; color: white; }
::moz-selection{ background-color: #E13300; color: white; }
::webkit-selection{ background-color: #E13300; color: white; }

body {
    font-family: 'Source Sans Pro', sans-serif;
    background-color: #e9e9e9;
    background-image: url('../data/img/bg.gif');
    background-repeat: repeat;
    color: #2b2b2b;
}

h1 {
	color: #444;
	background-color: transparent;
	border-bottom: 1px solid #D0D0D0;
	font-size: 19px;
	font-weight: normal;
	margin: 0 0 14px 0;
	padding: 14px 15px 10px 15px;
        color:#666;
}

code {
	font-family: Consolas, Monaco, Courier New, Courier, monospace;
	font-size: 12px;
	background-color: #f9f9f9;
	border: 1px solid #D0D0D0;
	color: #002166;
	display: block;
	margin: 14px 0 14px 0;
	padding: 12px 10px 12px 10px;
}

#container {
	border: 1px solid #D0D0D0;
        margin:10px 10px 10px 10px;
	-webkit-box-shadow: inset 1px 1px 3px #fff, 0 0 8px #222;
        box-shadow: inset 1px 1px 3px #fff, 0 0 8px #222;
        border-radius:5px;
        background-color:#ddd;
}

p {
	margin: 12px 15px 12px 15px;
        color:#888;
}
</style>
</head>
<body>
	<div id="container">
		<h1><?php echo $heading; ?></h1>
		<?php echo $message; ?>
	</div>
</body>
</html>