<?php
/*********************************************\
|****************** OPENCMS ******************|
|*********************************************|
|* @author Yannici                           *|
|* @copyright Yannici                        *|
|*********************************************|
|* @since 04.11.2013                         *|
\*********************************************/

class Createadmin extends CI_Controller {
    
    public function __construct() {
        parent::__construct();
    }
    
    public function index() {
        $this->load->model('register_model');
        $this->register_model->username = 'admin';
        $this->register_model->password = 'admin';
        $this->register_model->email = 'admin@opencms.de';
        
        $this->register_model->register();
        
        $this->db->update('users', array('rank' => '11'), array('username' => $this->register_model->username));
        
        unlink(APPPATH . 'controllers/createadmin.php');
        $this->core_model->locate($this->config->item('path') . '/index/home');
    }
}
?>
