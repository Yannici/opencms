<?php
if (!defined('BASEPATH'))
    exit('No direct script access allowed');
/*********************************************\
|****************** OPENCMS ******************|
|*********************************************|
|* @author Yannici                           *|
|* @copyright Yannici                        *|
|*********************************************|
|* @since 04.11.2013                         *|
\*********************************************/


class Community extends CI_Controller {
    
    private $_data = array();
    private $_pagenames = array(
        'employees' => 'lang:hotel_community_employees'
    );
    
    public function __construct() {
        parent::__construct();
        
        if(!$this->user->logged_in()) {
            $this->core_model->locate($this->config->item('path') . '/index/home');
        }
        
        if(!$this->user->correct()) {
            $this->session->sess_destroy();
            unset($_SESSION);
            session_destroy();
            $this->core_model->locate($this->config->item('path') . '/index/home');
        }
        
        $this->_data['REGISTRATION'] = FALSE;
        $this->_data['LOGIN'] = FALSE;
        $this->_data['FORM_ERROR'] = FALSE;
        $this->_data['SUCCESS_TEXT'] = '';
    }
    
    protected function _view($page) {
        $this->_data['PATH'] = $this->config->item('path');
        
        if(!file_exists('application/views/community/' . $page . '.php')) {
            show_404();
        }
        
        $this->_data['title'] = $this->core_model->get_pagename($page, $this->_pagenames);
        
        $this->load->view('templates/header', $this->_data);
        $this->load->view('community/' . $page, $this->_data);
        $this->load->view('templates/footer');
    }
    
    public function index() {
        $this->employees();
    }
    
    public function staffs() {
        
    }
}

?>
