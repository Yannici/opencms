<?php
/*********************************************\
|****************** OPENCMS ******************|
|*********************************************|
|* @author Yannici                           *|
|* @copyright Yannici                        *|
|*********************************************|
|* @since 04.11.2013                         *|
\*********************************************/

if (!defined('BASEPATH'))
    exit('No direct script access allowed');

class Openadmin extends CI_Controller {

    protected $_pagenames = array(
        'login' => 'lang:hotel_admin_login',
        'dashboard' => 'lang:hotel_admin_dashboard',
        'members' => 'lang:hotel_admin_members',
        'chatlogs' => 'lang:hotel_admin_chatlogs',
        'news' => 'lang:hotel_admin_news',
        'employee_ranking' => 'lang:hotel_admin_employee_ranking',
        'useredit' => 'lang:hotel_admin_useredit',
        'password_tool' => 'lang:hotel_admin_password_tool',
        'permissions' => 'lang:hotel_admin_permissions',
        'cmdlogs' => 'lang:hotel_admin_cmdlogs',
        'backup' => 'lang:hotel_admin_backup',
        'clonecheck' => 'lang:hotel_admin_clonecheck'
    );
    protected $_data = array();

    public function __construct() {
        parent::__construct();

        if (!$this->user->admin_correct()) {
            show_404();
        }

        $this->_data['WORKERS_ONLINE'] = $this->core_model->get_count_online_workers();
        $this->_data['USER_ONLINE'] = $this->core_model->get_count_online_users();
        $this->_data['USER_RECORD'] = $this->core_model->get_user_record();
        $this->_data['USER_TOTAL'] = $this->core_model->get_user_count();

        $this->load->helper(array('form', 'url'));
        $this->load->model('openadmin/stafflogs_model');
    }

    protected function _view($page = 'dashboard') {
        $this->load->model('openadmin/permission_model');
        $this->_data['PATH'] = $this->config->item('path');
        $this->_data['title'] = $this->core_model->get_pagename($page, $this->_pagenames);

        $base_site = explode("/", $page);

        if ($page == 'login') {
            if ($this->user->admin_logged_in() && $this->user->admin_security_check()) {
                $this->core_model->locate($this->config->item('path') . '/openadmin/dashboard');
            }
        } else {
            if ((!$this->user->admin_logged_in() || !$this->user->admin_security_check())) {
                $this->core_model->locate($this->config->item('path') . '/openadmin/login');
            }
        }

        if (!file_exists('application/views/openadmin/' . $page . '.php')) {
            show_404();
        }

        if (!$this->permission_model->have_permission($this->user->rank, $base_site[0]) && $base_site[0] != 'login') {
            $this->load->view('openadmin-templates/header', $this->_data);
            $this->load->view('openadmin/no_permission', $this->_data);
            $this->load->view('openadmin-templates/footer');
            return;
        }

        $this->load->view('openadmin-templates/header', $this->_data);
        $this->load->view('openadmin/' . $page, $this->_data);
        $this->load->view('openadmin-templates/footer');
    }

    public function index() {
        $this->login();
    }

    public function permissions($action = '', $detail = null) {
        $this->load->model('plugins/employee/employee_model');
        $aActions = array('edit', 'edit_submit');

        $this->_data['GROUPS'] = $this->employee_model->get_employee_ranks('rank', '>=', $this->config->item('admin_min_rank'));

        if ($action == '' OR !in_array($action, $aActions)) {
            $this->_view('permissions');
        } else {
            $this->_reflect_action('permissions', $action, $detail);
        }
    }

    public function backup($action = '') {
        if ($action == 'do') {
            $this->_backup_database();
        } else {
            $this->_view('backup');
        }
    }
    
    public function clonecheck($action = '') {
        if($action == 'check') {
            $this->_clonecheck_check();
        } else {
            $this->_view('clonecheck');
        }
    }
    
    private function _clonecheck_check() {
        $this->load->model('openadmin/clonecheck_model');
        $this->load->library('form_validation');
        
        $this->form_validation->set_rules('input_checkstring', $this->lang->line('hotel_admin_clonecheck_input_checkstring'), 'required|trim|max_length[50]|min_length[3]');
        $this->form_validation->set_rules('dropdown_checktype', $this->lang->line('hotel_admin_clonecheck_dropdown_checktype'), 'required');
        
        if($this->form_validation->run() === FALSE) {
            $this->_data['FORM_ERROR'] = TRUE;
            $this->_view('clonecheck');
        } else {
            $input = $this->input->get_post('input_checkstring', TRUE);
            $type = $this->input->get_post('dropdown_checktype', TRUE);
            $check = $this->clonecheck_model->check_for_clones($input, $type);
            if($check !== FALSE) {
                if(is_null($check)) {
                    $this->_data['FORM_ERROR'] = $this->lang->line('hotel_admin_clonecheck_no_clones_found');
                    $this->_view('clonecheck');
                } else {
                    $this->_data['FORM_SUCCESS'] = $this->lang->line('hotel_admin_clonecheck_found_clones', count($check));
                    $this->_data['CLONES'] = $check;
                    $this->_view('clonecheck/clonecheck_check');
                }
            } else {
                $this->_data['FORM_ERROR'] = $this->clonecheck_model->get_last_error();
                $this->_view('clonecheck');
            }
        }
    }

    private function _backup_database() {
        $this->load->dbutil();
        
        $backup = & $this->dbutil->backup_sql();

        $this->load->helper('file');
        $name = 'db_backup_' . date('d_m_Y', time()) . '.sql';
        $file_url = 'backup/' . $name;
        write_file($file_url, $backup);
        
        header('Content-Type: application/octet-stream');
        header("Content-Transfer-Encoding: Binary"); 
        header("Content-disposition: attachment; filename=\"".$name."\""); 
        readfile($file_url);
    }

    public function permissions_edit($r = 0) {
        $this->load->model('openadmin/permission_model');
        $this->load->model('plugins/employee/employee_model');

        $rank = (!$this->input->get_post('dropdown_rank', TRUE)) ? $r : $this->input->get_post('dropdown_rank', TRUE);
        $group = $this->employee_model->get_employee_group_by_rank($rank);

        if ($group == null) {
            $this->_data['FORM_ERROR'] = $this->lang->line('hotel_admin_permissions_error_rank_not_found');
            $this->permissions();
            return;
        }

        $this->_data['PERMISSIONS'] = $this->permission_model->get_permission_sites();
        $this->_data['RANK'] = $group['rank'];

        $this->_view('permissions/permissions_edit');
    }

    public function permissions_edit_submit() {
        $this->load->model('openadmin/permission_model');

        $rank = $this->input->get_post('input_rank', TRUE);
        $pers = $this->input->get_post('permission', TRUE);

        if (count($pers) == 0) {
            $this->_data['FORM_SUCCESS'] = $this->lang->line('hotel_admin_permissions_save_success');
            $this->permissions();
            return;
        }

        if ($this->permission_model->save_permissions($rank, $pers)) {
            $this->stafflogs_model->new_stafflog($this->lang->line('hotel_admin_permissions_saved', $rank), $this->user->id);
            $this->_data['FORM_SUCCESS'] = $this->lang->line('hotel_admin_permissions_save_success');
            $this->permissions();
            return;
        } else {
            $this->_data['FORM_ERROR'] = $this->permission_model->get_last_error();
            $this->permissions_edit($rank);
            return;
        }
    }

    public function members($a = '', $id = 0) {
        if ($a == 'delete' && is_numeric($id)) {
            $this->members_delete($id);
        }

        $this->_view('members');
    }

    public function useredit($action = '', $detail = null) {
        $aActions = array('edit', 'edit_submit');

        if ($action == '' OR !in_array($action, $aActions)) {
            $this->_view('useredit');
        } else {
            $this->_reflect_action('useredit', $action, $detail);
        }
    }

    public function password_tool($action = '') {
        if ($action == 'submit') {
            $this->_password_tool_submit();
            return;
        }

        $this->_view('password_tool');
    }

    private function _password_tool_submit() {
        $this->load->library('form_validation');
        $this->load->model('user_model');

        $this->form_validation->set_rules('input_user', $this->lang->line('hotel_admin_password_tool_input_user'), 'required|trim|callback_user_exists');
        $this->form_validation->set_rules('password_new', $this->lang->line('hotel_admin_password_tool_password_new'), 'required|min_length[3]|max_length[30]');

        if ($this->form_validation->run() === FALSE) {
            $this->_data['FORM_ERROR'] = TRUE;
        } else {
            $user = $this->user_model->search_user($this->input->get_post('input_user', TRUE));

            if ($user == null) {
                $this->_data['FORM_ERROR'] = $this->lang->line('hotel_admin_useredit_error_user_not_exists');
            }

            $user->password = $this->core_model->open_hash($this->input->get_post('password_new', TRUE), $user->username);
            if ($user->store()) {
                if ($user->id == $this->user->id) {
                    $this->session->unset_userdata('password');
                    $this->session->set_userdata('password', $user->password);
                }
                $this->stafflogs_model->new_stafflog($this->lang->line('hotel_admin_password_tool_password_changed', $user->username), $this->user->id);
                $this->_data['FORM_SUCCESS'] = $this->lang->line('hotel_admin_password_tool_success');
            } else {
                $this->_data['FORM_ERROR'] = 'MySQL-Error: ' . $this->db->_error_message();
            }
        }

        $this->_view('password_tool');
    }

    public function useredit_edit($id = null) {
        if ($id == null && !$this->input->get_post('input_user', TRUE)) {
            $this->_view('useredit');
            return;
        }

        $this->load->model('user_model');
        $this->load->model('plugins/employee/employee_model');
        $this->_data['GROUPS'] = $this->employee_model->get_employee_ranks('rank', '<=', $this->user->rank);

        if ($id != null && (is_numeric($id) || is_string($id))) {
            $user = new User(false);
            if (is_numeric($id)) {
                $user->set_user_by_id($id);
            } else {
                $user->set_user_by_username($id);
            }

            if ($user->id == null) {
                $this->_data['FORM_ERROR'] = $this->lang->line('hotel_admin_useredit_user_not_found');
                $this->useredit();
                return;
            }

            $this->_data['USERDATA'] = $user;
            $this->_view('useredit/useredit_edit');
            return;
        }

        $input = $this->input->get_post('input_user', TRUE);
        $user = $this->user_model->search_user($input);
        if ($user == null) {
            $this->_data['FORM_ERROR'] = $this->lang->line('hotel_admin_useredit_user_not_found');
            $this->_view('useredit');
            return;
        } else {
            $this->_data['USERDATA'] = $user;
            $this->_view('useredit/useredit_edit');
        }
    }

    public function useredit_edit_submit() {
        $id = $this->input->get_post('input_id', TRUE);

        $this->load->library('form_validation');
        $this->load->model('user_model');
        
        $this->form_validation->set_rules('input_id', $this->lang->line('hotel_admin_useredit_input_id'), 'required|numeric|callback_user_exists');
        
        if($this->input->get_post('delete_user', TRUE) !== FALSE) {
            if($this->form_validation->run() === TRUE) {
                $user = $this->user_model->search_user($this->input->get_post('input_id', TRUE));
                if($this->user_model->delete_user($user)) {
                    $this->stafflogs_model->new_stafflog($this->lang->line('hotel_admin_useredit_user_deleted', $user->username), $this->user->id);
                    $this->_data['FORM_SUCCESS'] = $this->lang->line('hotel_admin_useredit_delete_user_success');
                    
                    $this->core_model->MUS('reloadbans');
                    $this->core_model->MUS('signout', $id);
                    $this->useredit();
                    return;
                } else {
                    $this->_data['FORM_ERROR'] = $this->user_model->get_last_error();
                    $this->useredit_edit($id);
                    
                    return;
                }
            } else {
                $this->_data['FORM_ERROR'] = TRUE;
                $this->useredit_edit($id);
                
                return;
            }
        }

        $this->form_validation->set_rules('input_email', $this->lang->line('hotel_admin_useredit_input_email'), 'required|trim|xss_clean|valid_email');
        $this->form_validation->set_rules('input_credits', $this->lang->line('hotel_admin_useredit_input_credits'), 'numeric');
        $this->form_validation->set_rules('input_activity_points', $this->lang->line('hotel_admin_useredit_input_activity_points'), 'numeric');
        $this->form_validation->set_rules('input_vip_points', $this->lang->line('hotel_admin_useredit_input_vip_points'), 'numeric');
        $this->form_validation->set_rules('dropdown_ranks', $this->lang->line('hotel_admin_useredit_dropdown_rank'), 'callback_have_rights');

        if ($this->form_validation->run() === FALSE) {
            $this->_data['FORM_ERROR'] = TRUE;
            $this->useredit_edit($id);
        } else {
            if ($this->user_model->save_edit($id)) {
                $this->stafflogs_model->new_stafflog($this->lang->line('hotel_admin_useredit_user_edited', $id), $this->user->id);
                $this->_data['FORM_SUCCESS'] = $this->lang->line('hotel_admin_useredit_edit_success', $id);

                if ($this->user_model->is_online($id)) {
                    $this->core_model->MUS('updatecredits', $id);
                    $this->core_model->MUS('updatepixels', $id);
                    $this->core_model->MUS('updatepoints', $id);
                }

                $this->useredit();
            } else {
                $this->_data['FORM_ERROR'] = $this->user_model->get_last_error();
                $this->useredit_edit($id);
            }
        }
    }

    public function user_exists($id) {
        $this->db->where('id', $id)->or_where('LOWER(username)', strtolower($id))->limit(1);
        $c = count($this->db->get('users')->result_array());
        if ($c == 0) {
            $this->form_validation->set_message('user_exists', $this->lang->line('hotel_admin_useredit_error_user_not_exists'));
            return FALSE;
        }

        return TRUE;
    }

    public function have_rights() {
        $id = $this->input->get_post('input_id', TRUE);
        $this->db->select('rank')->where('id', $id)->limit(1);
        $result = $this->db->get('users')->result_array();

        if (count($result) == 0)
            return FALSE;

        if ($this->user->rank <= $result[0]['rank']
                && $this->user->id !== $id) {
            $this->form_validation->set_message('have_rights', $this->lang->line('hotel_admin_useredit_error_no_rights'));
            return FALSE;
        }

        return TRUE;
    }

    public function members_delete($id) {
        if (!is_numeric($id)) {
            return;
        }

        $this->load->model('user_model');

        $id = intval($id);
        $del_user = new User(false);
        $del_user->set_user_by_id($id);

        if (!$this->user_model->delete_user($del_user)) {
            $this->_data['FORM_ERROR'] = $this->user_model->get_last_error();
        } else {
            $this->stafflogs_model->new_stafflog($this->lang->line('hotel_admin_logs_user_deleted', $id), $this->user->id);
            $this->_data['FORM_SUCCESS'] = $this->lang->line('hotel_admin_members_deleted_success', $id);
        }
    }

    public function user() {
        $this->_view('user');
    }

    public function logout() {
        $this->session->unset_userdata('admin');
        $this->session->unset_userdata('security');
        $this->core_model->locate($this->config->item('path') . '/me');
    }

    public function loginsubmit() {
        $this->load->library('form_validation');
        $this->load->model('index/login_model');

        $this->form_validation->set_rules('adminlogin_username', $this->lang->line('hotel_username'), 'required|trim|xss_clean|callback_check_adminlogin');
        $this->form_validation->set_rules('adminlogin_password', $this->lang->line('hotel_password'), 'required');
        $this->form_validation->set_rules('adminlogin_security', $this->lang->line('hotel_admin_login_security_code'), 'required|xss_clean|trim|callback_check_security_code');

        if ($this->form_validation->run() === FALSE) {
            $this->_data['FORM_ERROR'] = TRUE;
            $this->login();
        } else {
            $this->stafflogs_model->new_stafflog($this->lang->line('hotel_admin_logs_loggedin', $this->lang->line('hotel_admin_name')), $this->user->id);
            $this->core_model->locate($this->config->item('path') . '/openadmin/dashboard');
        }
    }

    public function check_adminlogin() {
        return $this->login_model->check_admin_login($this->input->get_post('adminlogin_username', TRUE), $this->input->get_post('adminlogin_password', TRUE));
    }

    public function check_security_code() {
        if ($this->config->item('admin_security_code') == $this->input->get_post('adminlogin_security', TRUE)) {
            return TRUE;
        } else {
            $this->form_validation->set_message('check_security_code', $this->lang->line('hotel_admin_error_message_security_code_wrong'));
            return FALSE;
        }
    }

    public function cmdlogs($action = '') {
        $aActions = array('show');

        if ($action == '' OR !in_array($action, $aActions)) {
            $this->_view('cmdlogs');
        } else {
            $this->_reflect_action('cmdlogs', $action);
        }
    }

    public function cmdlogs_show() {
        $this->load->model('openadmin/cmdlogs_model');
        $this->load->library('form_validation');
        $this->form_validation->set_rules('input_cmdlogs', $this->lang->line('hotel_admin_chatlogs_inputfield'), 'required|trim|callback_check_cmdlog_input');
        $this->form_validation->set_rules('dropdown_cmdlogstype', $this->lang->line('hotel_admin_chatlogs_dropdown_type'), 'required');

        if ($this->form_validation->run() === FALSE) {
            $this->_data['FORM_ERROR'] = TRUE;
            $this->cmdlogs();
        } else {
            $this->_data['sType'] = $this->input->get_post('dropdown_cmdlogstype', TRUE);
            $this->_data['sString'] = $this->input->get_post('input_cmdlogs', TRUE);
            $this->_view('cmdlogs/cmdlogs_show');
        }
    }

    public function check_cmdlog_input() {
        return $this->cmdlogs_model->check_input($this->input->get_post('dropdown_cmdlogstype', TRUE), $this->input->get_post('input_cmdlogs', TRUE), TRUE);
    }

    public function chatlogs($action = '') {
        $aActions = array('show');
        if ($action == '' OR !in_array($action, $aActions)) {
            $this->_view('chatlogs');
        } else {
            $this->load->model('openadmin/chatlogs_model');

            if ($action == 'show') {
                $this->load->library('form_validation');
                $this->form_validation->set_rules('input_chatlogs', $this->lang->line('hotel_admin_chatlogs_inputfield'), 'required|trim|callback_check_chatlog_input');
                $this->form_validation->set_rules('dropdown_chatlogstype', $this->lang->line('hotel_admin_chatlogs_dropdown_type'), 'required');

                if ($this->form_validation->run() === FALSE) {
                    $this->_data['FORM_ERROR'] = TRUE;
                    $this->_view('chatlogs');
                } else {
                    $this->_data['sType'] = $this->input->get_post('dropdown_chatlogstype', TRUE);
                    $this->_data['sString'] = $this->input->get_post('input_chatlogs', TRUE);
                    $this->_view('chatlogs/chatlogs_' . $action);
                }
            } else {
                $this->_view('chatlogs');
            }
        }
    }

    public function check_chatlog_input() {
        return $this->chatlogs_model->check_input($this->input->get_post('dropdown_chatlogstype', TRUE), $this->input->get_post('input_chatlogs', TRUE), TRUE);
    }

    private function _reflect_action($sitename, $action, $detail = '') {
        $method = $sitename . '_' . $action;
        $ref = new ReflectionObject($this);
        if ($ref->hasMethod($method)) {
            $void = $ref->getMethod($method);
            if ($void->getNumberOfParameters() > 0) {
                $void->invokeArgs($this, array($detail));
            } else {
                $void->invoke($this);
            }
        } else {
            $this->$sitename();
        }
    }

    public function employee_ranking($action = '', $detail = '') {
        $aActions = array('add', 'edit', 'add_submit', 'edit_submit', 'delete');

        if ($action == '' OR !in_array($action, $aActions)) {
            $this->load->model('plugins/employee/employee_model');
            $this->_data['RANKING'] = $this->employee_model->get_employee_groups();
            $this->_view('employee_ranking');
        } else {
            $this->_reflect_action('employee_ranking', $action, $detail);
        }
    }

    public function employee_ranking_add() {
        $this->_view('employee_ranking/employee_ranking_add');
    }

    public function employee_ranking_add_submit() {
        $this->load->model('plugins/employee/employee_model');
        $this->load->library('form_validation');

        $this->form_validation->set_rules('input_rank', 'lang:hotel_admin_employee_ranking_rank', 'required|min_length[1]|max_length[2]|trim|xss_clean|numeric|is_unique[cms_employee_ranking.rank]');
        $this->form_validation->set_rules('input_name', 'lang:hotel_admin_employee_ranking_name', 'required|min_length[2]|max_length[255]|trim|xss_clean');
        $this->form_validation->set_rules('textarea_description', 'lang:hotel_admin_employee_ranking_description', 'required|min_length[20]|trim|xss_clean|max_length[6000]');

        if ($this->form_validation->run() === FALSE) {
            $this->_data['FORM_ERROR'] = TRUE;
            $this->_view('employee_ranking/employee_ranking_add');
        } else {
            if ($this->employee_model->add_group()) {
                $this->stafflogs_model->new_stafflog($this->lang->line('hotel_admin_logs_user_added_employee_ranking_group', $this->input->get_post('input_name', TRUE), $this->input->get_post('input_rank', TRUE)), $this->user->id);
                $this->_data['FORM_SUCCESS'] = $this->lang->line('hotel_admin_employee_ranking_add_success');
                $this->employee_ranking();
            } else {
                $this->_data['FORM_ERROR'] = $this->employee_model->get_last_error();
                $this->_view('employee_ranking/employee_ranking_add');
            }
        }
    }

    public function employee_ranking_edit($id) {
        $this->load->model('plugins/employee/employee_model');
        $this->_data['GROUP'] = $this->employee_model->get_employee_group($id);

        if ($this->_data['GROUP'] == null) {
            $this->_data['FORM_ERROR'] = $this->lang->line('hotel_admin_employee_ranking_not_found', $id);
            $this->employee_ranking();
        } else {
            $this->_data['GROUPID'] = $id;
            $this->_view('employee_ranking/employee_ranking_edit');
        }
    }

    public function employee_ranking_edit_submit($id) {
        $this->load->model('plugins/employee/employee_model');
        $this->load->library('form_validation');

        $this->form_validation->set_rules('input_rank', 'lang:hotel_admin_employee_ranking_rank', 'required|min_length[1]|max_length[2]|trim|xss_clean|is_numeric');
        $this->form_validation->set_rules('input_name', 'lang:hotel_admin_employee_ranking_name', 'required|min_length[2]|max_length[255]|trim|xss_clean');
        $this->form_validation->set_rules('textarea_description', 'lang:hotel_admin_employee_ranking_description', 'required|min_length[20]|trim|xss_clean|max_length[6000]');

        if ($this->form_validation->run() === FALSE) {
            $this->_data['FORM_ERROR'] = TRUE;
            $this->employee_ranking_edit($id);
        } else {
            if ($this->employee_model->edit_group($id)) {
                $this->_data['FORM_SUCCESS'] = $this->lang->line('hotel_admin_employee_ranking_edit_success', $id);
                $this->employee_ranking();
            } else {
                $this->_data['FORM_ERROR'] = $this->employee_model->get_last_error();
                $this->employee_ranking_edit($id);
            }
        }
    }

    public function employee_ranking_delete($id) {
        $this->load->model('plugins/employee/employee_model');

        $id = intval($id);

        if (!$this->employee_model->delete_group($id)) {
            $this->_data['FORM_ERROR'] = $this->employee_model->get_last_error();
        } else {
            $this->stafflogs_model->new_stafflog($this->lang->line('hotel_admin_logs_employee_ranking_delete', $id), $this->user->id);
            $this->_data['FORM_SUCCESS'] = $this->lang->line('hotel_admin_employee_ranking_delete_success', $id);
        }

        $this->employee_ranking();
    }

    public function news($action = '', $detail = '') {
        $aActions = array('add', 'edit', 'add_submit', 'edit_submit', 'delete');

        if ($action == '' OR !in_array($action, $aActions)) {
            $this->_view('news');
        } else {
            $this->_reflect_action('news', $action, $detail);
        }
    }

    public function news_add() {
        $this->load->model('news_model');
        $this->_view('news/news_add');
    }

    public function news_edit($id) {
        $this->load->model('news_model');
        $this->_data['NEWS'] = $this->news_model->get_by_id($id);

        if ($this->_data['NEWS'] == null) {
            $this->_data['FORM_ERROR'] = $this->lang->line('hotel_admin_news_not_found', $id);
            $this->_view('news');
        } else {
            $this->_data['NEWSID'] = $id;
            $this->_view('news/news_edit');
        }
    }

    public function news_delete($detail_id) {
        $this->load->model('news_model');

        $id = intval($detail_id);

        if (!$this->news_model->delete_news($id)) {
            $this->stafflogs_model->new_stafflog($this->lang->line('hotel_admin_logs_user_deleted_news', $id), $this->user->id);
            $this->_data['FORM_ERROR'] = $this->news_model->get_last_error();
        } else {
            $this->_data['FORM_SUCCESS'] = $this->lang->line('hotel_admin_news_delete_success');
        }

        $this->_view('news');
    }

    public function news_add_submit() {
        $this->load->model('news_model');
        $this->load->library('form_validation');

        $this->form_validation->set_rules('input_title', 'lang:hotel_admin_news_title', 'required|min_length[3]|max_length[40]|trim|xss_clean');
        $this->form_validation->set_rules('input_shortstory', 'lang:hotel_admin_news_short_story', 'required|min_length[5]|max_length[70]|trim|xss_clean');
        $this->form_validation->set_rules('input_image', 'lang:hotel_admin_news_image', 'required|min_length[1]|max_length[255]|trim|xss_clean');
        $this->form_validation->set_rules('textarea_longstory', 'lang:hotel_admin_news_long_story', 'required|min_length[20]|trim|xss_clean|max_length[6000]');

        if ($this->form_validation->run() === FALSE) {
            $this->_data['FORM_ERROR'] = TRUE;
            $this->_view('news/news_add');
        } else {
            if ($this->news_model->add_news()) {
                $this->stafflogs_model->new_stafflog($this->lang->line('hotel_admin_logs_user_added_news', htmlspecialchars($this->input->get_post('input_title', TRUE))), $this->user->id);
                $this->_data['FORM_SUCCESS'] = $this->lang->line('hotel_admin_news_add_success');
                $this->_view('news');
            } else {
                $this->_data['FORM_ERROR'] = $this->news_model->get_last_error();
                $this->_view('news/news_add');
            }
        }
    }

    public function news_edit_submit($id) {
        $this->load->model('news_model');
        $this->load->library('form_validation');

        $this->form_validation->set_rules('input_title', 'lang:hotel_admin_news_title', 'required|min_length[3]|max_length[40]|trim|xss_clean');
        $this->form_validation->set_rules('input_shortstory', 'lang:hotel_admin_news_short_story', 'required|min_length[5]|max_length[70]|trim|xss_clean');
        $this->form_validation->set_rules('input_image', 'lang:hotel_admin_news_image', 'required|min_length[1]|max_length[255]|trim|xss_clean');
        $this->form_validation->set_rules('textarea_longstory', 'lang:hotel_admin_news_long_story', 'required|min_length[20]|trim|xss_clean|max_length[6000]');

        if ($this->form_validation->run() === FALSE) {
            $this->_data['FORM_ERROR'] = TRUE;
            $this->news_edit($id);
        } else {
            if ($this->news_model->edit_news($id)) {
                $this->stafflogs_model->new_stafflog($this->lang->line('hotel_admin_logs_user_edited_news', htmlspecialchars($this->input->get_post('input_title', TRUE), ENT_COMPAT | ENT_HTML401, 'ISO-8859-1')), $this->user->id);
                $this->_data['FORM_SUCCESS'] = $this->lang->line('hotel_admin_news_edit_success', $id);
                $this->_view('news');
            } else {
                $this->_data['FORM_ERROR'] = $this->news_model->get_last_error();
                $this->news_edit($id);
            }
        }
    }

    public function login() {
        $this->load->model('index/login_model');
        $this->_data['ADMINLOGIN_FORM'] = $this->login_model->initialize_adminlogin();

        $this->_data['LOGIN'] = TRUE;
        $this->_view('login');
    }

    public function dashboard() {
        $this->load->model(array('openadmin/dashboard_model', 'update_model'));
        $this->_data['UPDATE_AVAILABLE'] = FALSE;

        if ($this->config->item('check_for_updates')) {
            $this->_data['UPDATE_AVAILABLE'] = $this->update_model->update_available();
            if( ! $this->_data['UPDATE_AVAILABLE']) {
                $this->_data['FORM_ERROR'] = $this->update_model->get_last_error();
            }
        }

        $this->_data['LAST_USER_ONLINE'] = $this->dashboard_model->get_last_online_users();
        $this->_data['LAST_STAFF_ACTIVITIES'] = $this->dashboard_model->get_last_staff_activities();
        $this->_view('dashboard');
    }

}

?>