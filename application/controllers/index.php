<?php if (!defined('BASEPATH')) exit('No direct script access allowed');
 /*********************************************\
 |****************** OPENCMS ******************|
 |*********************************************|
 |* @author Yannici                           *|
 |* @copyright Yannici                        *|
 |*********************************************|
 |* @since 04.11.2013                         *|
 \*********************************************/

class Index extends CI_Controller {
    
    protected $_pagenames = array(
        'home' => 'Home'
    );
    
    protected $_data = array();
    
    public function __construct() {
        parent::__construct();
        
        if($this->user->logged_in()) {
            $this->core_model->locate($this->config->item('path') . '/me');
        }
        
        $this->load->helper(array('form', 'url'));
        $this->load->library('form_validation');
        $this->load->model('index/login_model');
        $this->load->model('index/register_model');
        
        $this->_data['LOGIN_FORM'] = $this->login_model->initialize_login();
        $this->_data['REGISTER_FORM'] = $this->register_model->initialize_register();
        
        $this->_data['REGISTRATION'] = FALSE;
        $this->_data['LOGIN'] = FALSE;
        $this->_data['FORM_ERROR'] = FALSE;
    }
    
    protected function _view($page = 'home') {
        $this->_data['ONLINE_USERS'] = $this->core_model->users_online();
        $this->_data['PATH'] = $this->config->item('path');

        if(!file_exists('application/views/index/' . $page . '.php')) {
            show_404();
        }
        
        $this->_data['title'] = $this->_pagenames[$page];
        
        $this->load->view('templates/header', $this->_data);
        $this->load->view('index/' . $page, $this->_data);
        $this->load->view('templates/footer');
    }
    
    public function index() {
        $this->home();
    }
    
    public function home() {
        $this->_view('home');
    }
    
    public function register() {
        if($this->input->get_post('register_submit', TRUE) === FALSE) {
            $this->core_model->locate($this->config->item('path') . '/index/home');
        }
        
        $this->_data['REGISTRATION'] = TRUE;
        
        $this->form_validation->set_rules('register_username', $this->lang->line('hotel_username'), 'required|min_length[3]|max_length[12]|trim|is_unique[users.username]|xss_clean|callback_correct_username');
        $this->form_validation->set_rules('register_password', $this->lang->line('hotel_password'), 'required|min_length[3]|max_length[30]|matches[register_password_wdh]');
        $this->form_validation->set_rules('register_password_wdh', $this->lang->line('hotel_password_again'), 'required|min_length[3]|max_length[30]');
        $this->form_validation->set_rules('register_email', $this->lang->line('hotel_registration_email'), 'required|valid_email|xss_clean|trim');
        $this->form_validation->set_rules('register_captcha', 'Captcha', 'callback_captcha_validation');
        
        if($this->form_validation->run() === FALSE) {
            $this->_view('home');
        } else {
            $this->register_model->username = $this->input->get_post('register_username', TRUE);
            $this->register_model->password = $this->input->get_post('register_password', TRUE);
            $this->register_model->email = $this->input->get_post('register_email', TRUE);
            if($this->register_model->register()) {
                $this->core_model->locate($this->config->item('path') . '/me');
            }
        }
    }
    
    public function login() {
        if($this->input->get_post('login_submit', TRUE) === FALSE) {
           $this->core_model->locate($this->config->item('path') . '/index/home');
        }
        
        $this->_data['LOGIN'] = TRUE;
        $this->form_validation->set_rules('login_username', $this->lang->line('hotel_username'), 'required|trim|xss_clean|callback_check_login');
        $this->form_validation->set_rules('login_password', $this->lang->line('hotel_password'), 'required');
        
        if($this->form_validation->run() === TRUE ) {
            $this->core_model->locate($this->config->item('path') . '/me');
        } else {
            $this->_view('home');
        }
    }
    
    public function correct_username() {
        return $this->register_model->check_username($this->input->get_post('register_username', TRUE));
    }
    
    public function captcha_validation() {
        require_once 'data/scripts/securimage/securimage.php';
        $securimage = new Securimage();
        
        if($securimage->check($this->input->get_post('register_captcha', TRUE)) == FALSE) {
            $this->form_validation->set_message('captcha_validation', $this->lang->line('hotel_error_message_captcha_wrong'));
            return false;
        }
        
        return true;
    }
    
    public function check_login() {
        return $this->login_model->check_login($this->input->get_post('login_username', TRUE), $this->input->get_post('login_password', TRUE));
    } 
}
?>
