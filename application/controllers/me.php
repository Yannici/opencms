<?php

if (!defined('BASEPATH'))
    exit('No direct script access allowed');
/* * *******************************************\
  |****************** OPENCMS ******************|
  |*********************************************|
  |* @author Yannici                           *|
  |* @copyright Yannici                        *|
  |*********************************************|
  |* @since 04.11.2013                         *|
  \******************************************** */

class Me extends CI_Controller {

    protected $_pagenames = array(
        'home' => 'Home',
        'profile' => 'lang:hotel_profile_settings',
        'news' => 'lang:hotel_news_news',
        'game' => 'lang:hotel_game'
    );
    protected $_data = array();

    public function __construct() {
        parent::__construct();

        if (!$this->user->logged_in()) {
            $this->core_model->locate($this->config->item('path') . '/index/home');
        }

        if (!$this->user->correct()) {
            $this->session->sess_destroy();
            unset($_SESSION);
            session_destroy();
            $this->core_model->locate($this->config->item('path') . '/index/home');
        }

        $this->_data['REGISTRATION'] = FALSE;
        $this->_data['LOGIN'] = FALSE;
        $this->_data['FORM_ERROR'] = FALSE;
        $this->_data['SUCCESS_TEXT'] = '';
    }

    protected function _view($page = 'home') {
        $this->_data['PATH'] = $this->config->item('path');

        if (!file_exists('application/views/me/' . $page . '.php')) {
            show_404();
        }

        $this->_data['title'] = $this->core_model->get_pagename($page, $this->_pagenames);

        if($page != 'game') {
            $this->load->view('templates/header', $this->_data);
            $this->load->view('me/' . $page, $this->_data);
            $this->load->view('templates/footer');
        } else {
            $this->load->view('me/' . $page, $this->_data);
        }
    }
    
    public function game($f = '', $forwardid = 0) {
        $this->_data['FORWARD_ID'] = 0;
        $this->_data['FORWARD_TYPE'] = 0;
        
        if($f == 'room') {
            $this->_data['FORWARD_ID'] = $forwardid;
        }
        
        $this->_view('game');
    }
    
    public function gameerror() {
        $this->_view('gameerror');
    }

    public function index($created = FALSE) {
        if($created === TRUE) {
            $this->_data['FORM_SUCCESS'] = $this->lang->line('hotel_created_admin_success');
        }
        $this->home();
    }

    public function home() {
        $this->load->model('news_model');

        $this->_data['NEWS'] = $this->news_model->get_news(3);
        $this->_view('home');
    }

    public function logout() {
        $this->session->sess_destroy();
        session_destroy();
        unset($_SESSION);

        $this->core_model->locate($this->config->item('path') . '/index/home');
    }

    public function news($id = 0, $show_page = '', $page_id = 0) {
        $this->load->model('news_model');
        $this->load->library('pagination');
        $this->load->helper('form');
        
        $this->_data['all_news'] = $this->news_model->get_dategrouped_news(50);
        $this->_data['id'] = $id;

        if ($id == 0 || !is_numeric($id)) {
            $this->_data['news'] = $this->news_model->get_latest_news();
        } else {
            $this->_data['news'] = $this->news_model->get_by_id($id);
        }
        
        if($show_page == 'write') {
            $this->_news_comment_write($id);
        }
        
        $page_id = ($show_page != 'page') ? 1 : (($page_id < 1) ? 1 : $page_id+1);
        
        $this->_data['COMMENTS'] = $this->news_model->get_news_comments($this->_data['news']['id'], $page_id);
        $this->_data['COMMENTS_COUNT'] = $this->news_model->get_news_comments_count($this->_data['news']['id']);
        
        $config['base_url'] = $this->config->item('path') . '/news/' . $this->_data['news']['id'] . '/page/';
        $config['total_rows'] = $this->_data['COMMENTS_COUNT'];
        $config['per_page'] = $this->config->item('news_comments_per_page');
        $config['uri_segment'] = $page_id;
        $config['num_links'] = 4;
        
        $this->pagination->initialize($config);
        
        $this->_view('news');
    }
    
    public function _news_comment_write($news_id) {
        $this->load->library('form_validation');
        
        $this->form_validation->set_rules('textarea_comment', $this->lang->line('hotel_news_textarea_comment'), 'required|trim|min_length[25]|max_length[1000]|callback_comment_in_time');
        
        if($this->form_validation->run() == FALSE) {
            $this->_data['FORM_ERROR'] = TRUE;
        } else {
            if($this->news_model->save_comment($news_id)) {
                $this->_data['FORM_SUCCESS'] = $this->lang->line('hotel_news_comment_success');
            } else {
                $this->_data['FORM_ERROR'] = $this->news_model->get_last_error();
            }
        }
    }
    
    public function comment_in_time() {
        $c = $this->news_model->get_last_comment($this->user->id);

        if($c == null || $c['timestamp']+60 < time())
            return TRUE;
        
        $this->form_validation->set_message('comment_in_time', $this->lang->line('hotel_news_comment_in_time'));
        return FALSE;
    }

    public function profile($mode = '', $action = '') {
        $this->load->helper('form');
        $this->load->library('form_validation');

        $this->_data['MODE'] = strtolower($mode);
        if (!file_exists('./application/views/me/profile/' . $this->_data['MODE'] . '.php')) {
            // Fix Navigation-Dropdown-bug
            $this->core_model->locate($this->config->item('path') . '/profile/settings');
        }

        if (trim($action) !== '') {
            $this->load->model('me/profile_model');
            $reflectionObj = new ReflectionObject($this->profile_model);
            if ($reflectionObj->hasMethod($action)) {
                $reflectionMthd = $reflectionObj->getMethod($action);
                $r = $reflectionMthd->invoke($this->profile_model);

                switch ($action) {
                    case 'save_settings':
                        if (is_bool($r)) {
                            if ($r) {
                                $this->_data['SUCCESS_TEXT'] = $this->lang->line('hotel_profile_settings_success_saving');
                            } else {
                                $this->_data['FORM_ERROR'] = TRUE;
                            }
                        } else {
                            $this->form_validation->set_message('required', 'MySQLi-Error: ' . $r);
                        }
                        break;
                    case 'save_password':
                        if (is_bool($r)) {
                            if ($r) {
                                $this->_data['SUCCESS_TEXT'] = $this->lang->line('hotel_profile_newpassword_success_saving');
                            } else {
                                $this->_data['FORM_ERROR'] = TRUE;
                            }
                        } else {
                            $this->form_validation->set_message('required', 'MySQLi-Error: ' . $r);
                        }
                        break;
                }
            }
        }

        $this->_view('profile');
    }

    public function is_active_password() {
        if ($this->core_model->open_hash($this->input->get_post('input_password', TRUE), $this->user->username) != $this->user->password) {
            $this->form_validation->set_message('is_active_password', $this->lang->line('hotel_settings_newpassword_active_password_wrong'));
            return FALSE;
        }
        return TRUE;
    }

}