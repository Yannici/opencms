<?php

if (!defined('BASEPATH'))
    exit('No direct script access allowed');
/*********************************************\
|****************** OPENCMS ******************|
|*********************************************|
|* @author Yannici                           *|
|* @copyright Yannici                        *|
|*********************************************|
|* @since 04.11.2013                         *|
\********************************************/

class Tabledata extends CI_Controller {

    public function __construct() {
        parent::__construct();
        
        $this->load->model('tabledata_model');
    }

    public function index() {
        show_404();
    }

    /*
     * Get Users
     */
    public function get_users($filter = '') {

        if(!$this->user->admin_correct()) {
            show_404();
        }

        $this->tabledata_model->get_users($filter);
    }
    
    public function get_chatlogs($type = 'user', $searchString = 0) {
        if(!$this->user->admin_correct()) {
            show_404();
        }
        
        $this->tabledata_model->get_chatlogs($type, $searchString);
    }
    
    public function get_cmdlogs($type = 'user', $searchString = '') {
        if(!$this->user->admin_correct()) {
            show_404();
        }
        
        $this->tabledata_model->get_cmdlogs($type, $searchString);
    }
    
    public function get_translations() {
        if(!$this->user->admin_correct()) {
            show_404();
        }
        
        echo utf8_encode('{
                    "sLengthMenu": "' . $this->lang->line('hotel_admin_datatables_show_records') . '",
                    "sZeroRecords": "' . $this->lang->line('hotel_admin_datatables_no_records_found') . '",
                    "sInfo": "' . $this->lang->line('hotel_admin_datatables_records_showing'). '",
                    "sInfoEmpty": "' . $this->lang->line('hotel_admin_datatables_no_records_found') . '",
                    "sInfoFiltered": "' . $this->lang->line('hotel_admin_datatables_filtered_records') . '",
                    "sSearch": "' . $this->lang->line('hotel_admin_datatables_search_records') . '",
                    "sProcessing": "' . $this->lang->line('hotel_admin_datatables_processing_records') . '",
                    "oPaginate": {
                        "sFirst":    "' . $this->lang->line('hotel_admin_datatables_first_page') . '",
                        "sPrevious": "' . $this->lang->line('hotel_admin_datatables_previous_page') . '",
                        "sNext":     "' . $this->lang->line('hotel_admin_datatables_next_page') . '",
                        "sLast":     "' . $this->lang->line('hotel_admin_datatables_last_page') . '"
                    }
               }');
    }
    
    public function get_news() {
        if(!$this->user->admin_correct()) {
            show_404();
        }
        
        $this->tabledata_model->get_news();
    }
}
?>
