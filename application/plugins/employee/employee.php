<?php
/*********************************************\
|****************** OPENCMS ******************|
|*********************************************|
|* @author Yannici                           *|
|* @copyright Yannici                        *|
|*********************************************|
|* @since 04.11.2013                         *|
\*********************************************/


class Employee_plugin extends Abstract_plugin {
    
    private $_plugin_author = 'Yannici'; // Your name/nickname!
    private $_plugin_description = 'Shows the Hotel-Employees'; // Plugin description! Any description ... there's no dependency to other code
    private $_plugin_technical_name = 'employee'; // Must match with the file name (case insensitive)
    private $_plugin_version = '0.0.1'; // Any version
    
    public function get_plugin_author() {
        return $this->_plugin_author;
    }

    public function get_plugin_description() {
        return $this->_plugin_description;
    }

    public function get_plugin_name() { // Plugin name with multilanguage
        return $this->lang->line('hotel_admin_employee_ranking');
    }

    public function get_plugin_technical_name() {
        return $this->_plugin_technical_name;
    }

    public function get_plugin_version() {
        return $this->_plugin_version;
    }

    public function action() {
        $this->load->model('plugins/employee/employee_model');
        
        return array('employee' => $this->employee_model->get_employee());
    }
}

?>
