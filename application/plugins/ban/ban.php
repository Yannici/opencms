<?php

/* * *******************************************\
  |****************** OPENCMS ******************|
  |*********************************************|
  |* @author Yannici                           *|
  |* @copyright Yannici                        *|
  |*********************************************|
  |* @since 04.11.2013                         *|
  \******************************************** */

class Ban_plugin extends Abstract_admin_plugin {

    private $_plugin_author = 'Yannici';
    private $_plugin_description = 'Ban user or ip addresses';
    private $_plugin_technical_name = 'ban';
    private $_plugin_version = '0.0.1';

    public function get_plugin_author() {
        return $this->_plugin_author;
    }

    public function get_plugin_description() {
        return $this->_plugin_description;
    }

    public function get_plugin_name() {
        return $this->lang->line('hotel_admin_ban');
    }

    public function get_plugin_technical_name() {
        return $this->_plugin_technical_name;
    }

    public function get_plugin_version() {
        return $this->_plugin_version;
    }

    public function action($action = '') {
        $data = array();

        if ($action == 'banhim') {
            return $this->_ban();
        }
    }

    private function _ban() {
        $data = array();
        $data['WASBAN'] = TRUE;
        $this->load->model(array('user_model', 'plugins/ban/ban_model'));
        $this->load->library('form_validation');

        $this->form_validation->set_rules('dropdown_ban_or_unban', $this->lang->line('hotel_admin_ban_dropdown_ban'), 'required|trim');
        $this->form_validation->set_rules('input_value', $this->lang->line('hotel_admin_ban_input_value'), 'required|trim|callback_check_input_ban|xss_clean');
        $this->form_validation->set_rules('input_reason', $this->lang->line('hotel_admin_ban_input_reason'), 'trim|xss_clean|max_length[150]');
        $this->form_validation->set_rules('dropdown_length', $this->lang->line('hotel_admin_ban_input_length'), 'trim|numeric|xss_clean|max_length[20]');
        $this->form_validation->set_rules('dropdown_bantype', $this->lang->line('hotel_admin_ban_dropdown_bantype'), 'xss_clean');

        if ($this->form_validation->run() == FALSE) {
            $data['FORM_ERROR'] = TRUE;
        } else {
            if ($this->input->get_post('dropdown_ban_or_unban', TRUE) == 'ban') {
                if ($this->ban_model->ban_user($this->input->get_post('input_value', TRUE), $this->input->get_post('input_reason', TRUE), $this->input->get_post('input_length', TRUE), $this->input->get_post('dropdown_bantype', TRUE))) {
                    $data['FORM_SUCCESS'] = $this->lang->line('hotel_admin_ban_success');
                } else {
                    $data['FORM_ERROR'] = $this->ban_model->get_last_error();
                }
            } elseif($this->input->get_post('dropdown_ban_or_unban', TRUE) == 'unban') {
                if ($this->ban_model->unban_user($this->input->get_post('input_value', TRUE), $this->input->get_post('dropdown_bantype', TRUE))) {
                    $data['FORM_SUCCESS'] = $this->lang->line('hotel_admin_unban_success');
                } else {
                    $data['FORM_ERROR'] = $this->ban_model->get_last_error();
                }
            }
        }

        return $data;
    }

    public function check_input_ban() {
        $input = $this->input->get_post('input_value', TRUE);
        $length = $this->input->get_post('dropdown_length', TRUE);
        $type = $this->input->get_post('dropdown_bantype', TRUE);
        $u = $this->input->get_post('dropdown_ban_or_unban', TRUE);

        if ($u == 'ban' && (trim($input) == '' || !is_numeric($length) || trim($length) == '')) {
            $this->form_validation->set_message('check_input_ban', $this->lang->line('hotel_admin_ban_error_search'));
            return FALSE;
        }

        if ($u == 'unban' && trim($input) == '') {
            $this->form_validation->set_message('check_input_ban', $this->lang->line('hotel_admin_ban_error_search'));
            return FALSE;
        }

        if ((!is_string($input) && !is_int($input)) || empty($input)) {
            $this->form_validation->set_message('check_input_ban', $this->lang->line('hotel_admin_ban_error_search'));
            return FALSE;
        }

        if ($type != 'user' && $type != 'ip') {
            $this->form_validation->set_message('check_input_ban', $this->lang->line('hotel_admin_ban_error_search'));
            return FALSE;
        }

        return TRUE;
    }

}

?>
