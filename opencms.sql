SET SQL_MODE="NO_AUTO_VALUE_ON_ZERO";
SET time_zone = "+00:00";

/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8 */;


DROP TABLE IF EXISTS `cms_employee_ranking`;
CREATE TABLE IF NOT EXISTS `cms_employee_ranking` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `rank` int(32) NOT NULL,
  `name` varchar(255) NOT NULL,
  `description` text NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 AUTO_INCREMENT=4 ;

INSERT INTO `cms_employee_ranking` (`id`, `rank`, `name`, `description`) VALUES
(1, 11, 'Hotelbesitzer', 'HOTELBESITZER-BESCHREIBUNG'),
(2, 1, 'Normalo', 'NORMALO-BESCHREIBUNG'),
(3, 2, 'VIP', 'VIP-BESCHREIBUNG');

DROP TABLE IF EXISTS `cms_news`;
CREATE TABLE IF NOT EXISTS `cms_news` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `title` varchar(255) CHARACTER SET latin1 COLLATE latin1_general_ci DEFAULT NULL,
  `shortstory` text CHARACTER SET latin1 COLLATE latin1_general_ci,
  `longstory` text CHARACTER SET latin1 COLLATE latin1_general_ci,
  `published` int(10) NOT NULL DEFAULT '0',
  `image` varchar(255) CHARACTER SET latin1 COLLATE latin1_general_ci NOT NULL,
  `campaign` int(1) NOT NULL DEFAULT '0',
  `campaignimg` varchar(255) CHARACTER SET latin1 COLLATE latin1_general_ci DEFAULT NULL,
  `author` int(6) NOT NULL DEFAULT '1',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 AUTO_INCREMENT=1 ;

DROP TABLE IF EXISTS `cms_news_comments`;
CREATE TABLE IF NOT EXISTS `cms_news_comments` (
  `id` int(32) unsigned NOT NULL AUTO_INCREMENT,
  `news_id` int(32) NOT NULL,
  `user_id` int(32) NOT NULL,
  `message` text NOT NULL,
  `timestamp` int(16) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1 AUTO_INCREMENT=1 ;

DROP TABLE IF EXISTS `cms_permission`;
CREATE TABLE IF NOT EXISTS `cms_permission` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `rank` int(11) NOT NULL,
  `site` varchar(255) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB  DEFAULT CHARSET=latin1 AUTO_INCREMENT=80 ;

INSERT INTO `cms_permission` (`id`, `rank`, `site`) VALUES
(68, 11, 'dashboard'),
(69, 11, 'news'),
(70, 11, 'employee_ranking'),
(71, 11, 'password_tool'),
(72, 11, 'permissions'),
(73, 11, 'members'),
(74, 11, 'useredit'),
(75, 11, 'ban'),
(76, 11, 'clonecheck'),
(77, 11, 'chatlogs'),
(78, 11, 'cmdlogs'),
(79, 11, 'backup');

DROP TABLE IF EXISTS `cms_stafflogs`;
CREATE TABLE IF NOT EXISTS `cms_stafflogs` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `user_id` int(11) NOT NULL,
  `message` text NOT NULL,
  `stamp` int(16) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 AUTO_INCREMENT=1 ;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
