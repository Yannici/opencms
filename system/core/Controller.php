<?php  if ( ! defined('BASEPATH')) exit('No direct script access allowed');
/**
 * CodeIgniter
 *
 * An open source application development framework for PHP 5.1.6 or newer
 *
 * @package		CodeIgniter
 * @author		ExpressionEngine Dev Team
 * @copyright	Copyright (c) 2008 - 2011, EllisLab, Inc.
 * @license		http://codeigniter.com/user_guide/license.html
 * @link		http://codeigniter.com
 * @since		Version 1.0
 * @filesource
 */

// ------------------------------------------------------------------------

/**
 * CodeIgniter Application Controller Class
 *
 * This class object is the super class that every library in
 * CodeIgniter will be assigned to.
 *
 * @package		CodeIgniter
 * @subpackage	Libraries
 * @category	Libraries
 * @author		ExpressionEngine Dev Team
 * @link		http://codeigniter.com/user_guide/general/controllers.html
 */
class CI_Controller {

	private static $instance;

	/**
	 * Constructor
	 */
	public function __construct()
	{
		self::$instance =& $this;
		
		// Assign all the class objects that were instantiated by the
		// bootstrap file (CodeIgniter.php) to local class variables
		// so that CI can run as one big super object.
		foreach (is_loaded() as $var => $class)
		{
			$this->$var =& load_class($class);
		}

		$this->load =& load_class('Loader', 'core');

		$this->load->initialize();
		
		log_message('debug', "Controller Class Initialized");
	}

	public static function &get_instance()
	{
		return self::$instance;
	}
        
        public function __call($name, $arguments) {
            if($this->plugin_system->is_registered_plugin($name, get_class($this), (strtolower(get_class($this)) == 'openadmin'))) {
                $instance = $this->plugin_system->load_plugin($name);
                $ref_object = new ReflectionObject($instance);
                $data = $ref_object->getMethod('action')->invokeArgs($instance, $arguments);
                $instance->view($data);
            } else {
                show_404();
            }
        }
        
        public function _remap($name, $arguments) {
            // THANK YOU REMAP!
            // YOU MAKE IT POSSIBLE TO ADD MY PLUGIN SYSTEM!! :* Yannici
            // JUST MAKE A CALL AND THE __CALL METHOD WILL HANDLE
            call_user_func_array(array($this, $name), $arguments);
        }
}
// END Controller class

/* End of file Controller.php */
/* Location: ./system/core/Controller.php */