<?php
/*********************************************\
|****************** OPENCMS ******************|
|*********************************************|
|* @author Yannici                           *|
|* @copyright Yannici                        *|
|*********************************************|
|* @since 04.11.2013                         *|
\*********************************************/

// GENERAL
$lang['hotel_me'] = 'Ich';
$lang['hotel_home'] = 'Home';
$lang['hotel_settings'] = 'Einstellungen';
$lang['hotel_logout'] = 'Ausloggen';
$lang['hotel_community'] = 'Community';
$lang['hotel_goto_client'] = 'Einchecken &raquo;';
$lang['hotel_readmore'] = 'Lies mehr';
$lang['hotel_read'] = 'Lesen &raquo;';
$lang['hotel_online_users'] = '%d User online';
$lang['hotel_credits'] = 'Taler';
$lang['hotel_activity_points'] = 'Pixel';
$lang['hotel_vip_points'] = 'Diamant%s';
$lang['hotel_email'] = 'E-Mail';
$lang['hotel_motto'] = 'Mission';
$lang['hotel_yes'] = 'Ja';
$lang['hotel_no'] = 'Nein';
$lang['hotel_latest_news'] = 'Aktuelle News';
$lang['hotel_username'] = 'Username';
$lang['hotel_password'] = 'Passwort';
$lang['hotel_unknown_user'] = 'Unbekannter';
$lang['hotel_identification'] = 'ID';
$lang['hotel_online'] = 'Online';
$lang['hotel_offline'] = 'Offline';
$lang['hotel_users'] = 'User';
$lang['hotel_user'] = 'User';
$lang['hotel_rooms'] = 'R�ume';
$lang['hotel_room'] = 'Raum';
$lang['hotel_back'] = '&laquo; Zur�ck';
$lang['hotel_date'] = 'Datum';
$lang['hotel_message'] = 'Nachricht';
$lang['hotel_plural'] = 'en';

// INDEX
$lang['hotel_welcome'] = 'Welcome to %s';
$lang['hotel_welcome_message'] = 'Welcome to %s! Here you will have a lot of fun, a lot of credits, and constantly new Events! Check in and join us!';
$lang['hotel_register'] = 'Sign up now &raquo;';
$lang['hotel_login'] = 'Login &raquo;';
$lang['hotel_thumbnail_label_1'] = 'Friends';
$lang['hotel_thumbnail_label_2'] = 'Build rooms';
$lang['hotel_thumbnail_label_3'] = 'Have fun';
$lang['hotel_thumbnail_text_1'] = 'In the Hotel you will get alot of new friends for sure!';
$lang['hotel_thumbnail_text_2'] = 'You can build great rooms and let your imagination run wild!';
$lang['hotel_thumbnail_text_3'] = 'With our great community, we can guarantee you a lot of fun!';

// FORM-SPECIFIC
$lang['hotel_form_reset'] = 'Reset';

// PROFIL-SETTINGS
$lang['hotel_profile_settings'] = 'Profile settings';
$lang['hotel_profile_settings_navigation'] = 'Settings';
$lang['hotel_profile_settings_new_friends'] = 'Allow new friends?';
$lang['hotel_profile_settings_hide_online'] = 'Hide your online status?';
$lang['hotel_profile_settings_volume'] = 'Volume';
$lang['hotel_profile_settings_success_saving'] = 'Successfully saved!';
$lang['hotel_profile_settings_trading'] = 'Allow trading?';
$lang['hotel_profile_edit_password'] = 'Change Password';
$lang['hotel_settings_newpassword_active_password'] = 'Current Password';
$lang['hotel_settings_newpassword_new_password'] = 'New Password';
$lang['hotel_settings_newpassword_new_password_wdh'] = 'Repeat new Password';
$lang['hotel_profile_newpassword_success_saving'] = 'Password successfully changed!';
$lang['hotel_settings_newpassword_active_password_wrong'] = 'The current password is incorrect!';

// REGISTRATION
$lang['hotel_password_again'] = 'Passwort Wdh.';
$lang['hotel_registration_complete'] = 'Registrierung abschliessen &raquo;';

// ERROR-MESSAGES
$lang['hotel_error_message_captcha_wrong'] = 'Captcha-Code wurde falsch eingegeben!';
$lang['hotel_error_message_error'] = 'Fehler!';
$lang['hotel_error_message_check_login'] = '%s und %s stimmen nicht �berein!';
$lang['hotel_error_message_ip_check_failed'] = 'Multiaccount verd�chtig!';
$lang['hotel_error_message_username_incorrect'] = 'Username ung�ltig!';
$lang['hotel_error_message_success'] = 'Erfolgreich!';
$lang['hotel_error_message_check_login_bans'] = 'Du wurdest gebannt! Dein Banngrund lautet: "%s" und geht bis %s';

// NAVIGATION
$lang['hotel_hello_message'] = 'Hallo, %s';

// NEWS
$lang['hotel_news_author_date'] = '<strong>%s</strong> am %s um %s Uhr';
$lang['hotel_news_news'] = 'News';
$lang['hotel_news_comments'] = 'Kommentare (%s)';
$lang['hotel_news_send_comment'] = 'Kommentar abschicken &raquo;';
$lang['hotel_news_no_comments'] = 'Noch keine Kommentare vorhanden, sei der Erste!';
$lang['hotel_news_comment_in_time'] = 'Du kannst nur jede Minute einen Kommentar abgeben!';
$lang['hotel_news_comment_success'] = 'Kommentar erfolgreich gespeichert!';
$lang['hotel_news_write_comment'] = 'Schreibe einen Kommentar ...';

/*
 * COMMUNITY
 */

// MITARBEITER
$lang['hotel_community_employee'] = 'Mitarbeiter';

/*
 * OPENADMIN-AREA
 */
// GENERAL
$lang['hotel_admin_name'] = 'OPENADMIN';
$lang['hotel_admin_workers_online'] = 'Mitarbeiter online';
$lang['hotel_admin_client'] = 'Client';
$lang['hotel_admin_logout'] = 'Ausloggen';
$lang['hotel_admin_online_users'] = 'User online';
$lang['hotel_admin_user_record'] = 'Userrekord';
$lang['hotel_admin_user_total'] = 'angemeldete User';
$lang['hotel_admin_edit'] = 'Bearbeiten';
$lang['hotel_admin_delete'] = 'L�schen';
$lang['hotel_admin_manage'] = 'Verwaltung';
$lang['hotel_admin_actions'] = 'Aktionen';
$lang['hotel_admin_save'] = 'Speichern &raquo;';
$lang['hotel_admin_delete_confirmation'] = 'Willst du diesen Datensatz wirklich l�schen?';


// LOGIN
$lang['hotel_admin_login'] = 'Openadmin-Login';
$lang['hotel_admin_login_security_code'] = 'Sicherheitscode';
$lang['hotel_admin_login_submit'] = 'Einloggen &raquo;';

// MESSAGES
$lang['hotel_admin_error_message_security_code_wrong'] = 'Der Sicherheitscode wurde falsch eingegeben!';
$lang['hotel_admin_error_chatlogs_wrong_type'] = 'Du hast einen falschen Typ angegeben!';
$lang['hotel_admin_error_chatlogs_wrong_input'] = 'Die Such-Angabe war inkorrekt!';

// DASHBOARD
$lang['hotel_admin_dashboard_last_online_users'] = 'Zuletzt eingeloggte User';
$lang['hotel_admin_dashboard_last_ip'] = 'IP-Adresse';
$lang['hotel_admin_last_online_users'] = 'Letzte Logins';
$lang['hotel_admin_last_staff_activities'] = 'Letzte Staff-Aktivit�ten';
$lang['hotel_admin_dashboard'] = 'Dashboard';

// MEMBERS
$lang['hotel_admin_members_login_ipaddress'] = 'Login IP-Adresse';
$lang['hotel_admin_members_register_ipaddress'] = 'Registrierung IP-Adresse';
$lang['hotel_admin_members_last_online'] = 'Zuletzt Online';
$lang['hotel_admin_members'] = 'Mitglieder';
$lang['hotel_admin_members_rank'] = 'Rank';
$lang['hotel_admin_members_delete_no_user_found'] = 'User wurde nicht gefunden!';
$lang['hotel_admin_members_deleted_success'] = 'User mit der ID \'%d\' erfolgreich gel�scht!';
$lang['hotel_admin_members_delete_no_permission'] = 'Du hast keine Berechtigung, um den User \'%s\' zu l�schen!';

// EDIT USER
$lang['hotel_admin_useredit'] = 'Mitglied bearbeiten';
$lang['hotel_admin_useredit_search'] = 'Nach Mitglied suchen';
$lang['hotel_admin_useredit_input_user'] = 'User-ID oder Username';
$lang['hotel_admin_useredit_input_user_description'] = 'Gib hier ein User ein (User-ID oder Username), um diesen zu bearbeiten.';
$lang['hotel_admin_useredit_input_submit'] = 'Mitglied bearbeiten &raquo;';
$lang['hotel_admin_useredit_user_not_found'] = 'Es wurde kein User gefunden!';
$lang['hotel_admin_useredit_general_settings'] = 'Allgemein';
$lang['hotel_admin_useredit_input_username'] = 'Username';
$lang['hotel_admin_useredit_input_email'] = 'E-Mail';
$lang['hotel_admin_useredit_input_password'] = 'Passwort';
$lang['hotel_admin_useredit_input_password_description'] = 'Benutze das Passwort-Tool, um Passw�rter zu �ndern!';
$lang['hotel_admin_useredit_edit'] = '%s bearbeiten';
$lang['hotel_admin_useredit_input_reg_ip'] = 'Registrierung-IP';
$lang['hotel_admin_useredit_input_last_ip'] = 'Letzte IP';
$lang['hotel_admin_useredit_input_id'] = 'ID';
$lang['hotel_admin_useredit_checkbox_block_newfriends'] = 'Freundesanfragen blockieren?';
$lang['hotel_admin_useredit_checkbox_hide_online'] = 'Online-Status verbergen?';
$lang['hotel_admin_useredit_checkbox_trading'] = 'Antauschen zulassen?';
$lang['hotel_admin_useredit_rank_and_subscriptions_settings'] = 'Rank und Privatsph�re';
$lang['hotel_admin_useredit_dropdown_rank'] = 'User-Rank';
$lang['hotel_admin_useredit_checkbox_vip'] = 'VIP?';
$lang['hotel_admin_useredit_look'] = 'Aussehen';
$lang['hotel_admin_useredit_bans'] = 'Bans';
$lang['hotel_admin_useredit_bans_date'] = 'Bann-Datum';
$lang['hotel_admin_useredit_bans_reason'] = 'Grund';
$lang['hotel_admin_useredit_bans_added_by'] = 'Gebannt von';
$lang['hotel_admin_useredit_bans_expire'] = 'Ablauf-Datum';
$lang['hotel_admin_useredit_error_user_not_exists'] = 'Dieser User existiert nicht!';
$lang['hotel_admin_useredit_error_no_rights'] = 'Du kannst keine h�her- oder gleich-rangige User bearbeiten!';
$lang['hotel_admin_useredit_edit_success'] = 'User mit der ID \'%s\' erfolgreich gespeichert!';
$lang['hotel_admin_useredit_user_edited'] = 'User mit der ID \'%d\' bearbeitet!';

// PASSWORD-TOOL
$lang['hotel_admin_password_tool'] = 'Passwort-Tool';
$lang['hotel_admin_password_tool_edit'] = 'Passwort �ndern';
$lang['hotel_admin_password_tool_input_user'] = 'Username/User-ID';
$lang['hotel_admin_password_tool_success'] = 'Passwort erfolgreich gesetzt!';
$lang['hotel_admin_password_tool_input_desc'] = 'Gib hier den Username oder die User-ID ein, von dem du das Passwort �ndern m�chtest.';
$lang['hotel_admin_password_tool_password_new'] = 'Neues Passwort';
$lang['hotel_admin_password_tool_password_new_desc'] = 'Gib hier das neue Passwort f�r den User ein.';
$lang['hotel_admin_password_tool_password_changed'] = 'Passwort von \'%s\' ge�ndert!';

// DATATABLES (FOR TRANSLATIONS SEE: http://datatables.net/plug-ins/i18n)
$lang['hotel_admin_datatables_show_records'] = '_MENU_ Eintr�ge anzeigen'; // _MENU_ is a parameter!!!
$lang['hotel_admin_datatables_no_records_found'] = 'Keine Eintr�ge gefunden!';
$lang['hotel_admin_datatables_records_showing'] = 'Zeige _START_ bis _END_ von _TOTAL_ Eintr�gen'; // _START_, _END_ and _TOTAL_ are parameters!!!
$lang['hotel_admin_datatables_filtered_records'] = '(gefiltert von insgesamt _MAX_ Eintr�gen)'; // _MAX_ is a parameter!!!
$lang['hotel_admin_datatables_search_records'] = 'Suchen:';
$lang['hotel_admin_datatables_processing_records'] = 'Bitte warten ...';
$lang['hotel_admin_datatables_next_page'] = 'N�chste';
$lang['hotel_admin_datatables_previous_page'] = 'Zur�ck';
$lang['hotel_admin_datatables_first_page'] = 'Erste';
$lang['hotel_admin_datatables_last_page'] = 'Letzte';

// PERMISSIONS
$lang['hotel_admin_permissions'] = 'Berechtigungen';
$lang['hotel_admin_permissions_edit'] = 'Berechtigungen bearbeiten';
$lang['hotel_admin_permissions_dropdown_rank_description'] = 'Gib hier die Mitarbeiter-Gruppe an, von denen du die Berechtigung �ndern willst.';
$lang['hotel_admin_permissions_dropdown_rank'] = 'Rank';
$lang['hotel_admin_permissions_submit_input'] = 'Berechtigungen bearbeiten &raquo;';
$lang['hotel_admin_permissions_error_rank_not_found'] = 'Rank zum Bearbeiten nicht gefunden!';
$lang['hotel_admin_permissions_save_success'] = 'Die Berechtigungen wurden erfolgreich gespeichert!';
$lang['hotel_admin_wrong_permissions'] = 'Berechtigungen fehlen';
$lang['hotel_admin_permissions_saved'] = 'Berechtigungen von Rank \'%s\' ge�ndert!';
$lang['hotel_admin_no_permission'] = 'Du hast leider keine Berechtigung, um auf die Seite \'%s\' zugreifen zu k�nnen. Wende dich daf�r an den Hotelbesitzer!';

// LOGS
$lang['hotel_admin_logs_loggedin'] = 'Hat sich in %s eingeloggt!';
$lang['hotel_admin_logs'] = 'Auswertungen';
$lang['hotel_admin_logs_user_deleted_news'] = 'News mit der ID \'%d\' gel�scht!';
$lang['hotel_admin_logs_user_added_news'] = 'News \'%s\' ver�ffentlicht!';
$lang['hotel_admin_logs_user_edited_news'] = 'News \'%s\' bearbeitet!';
$lang['hotel_admin_logs_user_added_employee_ranking_group'] = 'Mitarbeiter-Gruppe \'%s\' f�r Rank \'%d\' hinzugef�gt!';
$lang['hotel_admin_logs_employee_ranking_delete'] = 'Mitarbeiter-Gruppe mit der ID \'%d\' gel�scht!';
$lang['hotel_admin_logs_user_deleted'] = 'User mit der ID \'%d\' gel�scht!';

// CHATLOGS
$lang['hotel_admin_chatlogs'] = 'Chatlogs';
$lang['hotel_admin_chatlogs_search'] = 'Chatlogs suchen';
$lang['hotel_admin_chatlogs_show'] = 'Chatlogs anzeigen';
$lang['hotel_admin_chatlogs_inputfield'] = 'Suchen nach';
$lang['hotel_admin_chatlogs_inputfield_description'] = 'Gib hier eine Raum-ID, User-ID oder einen Usernamen ein. Je nachdem nach was du suchen willst.';
$lang['hotel_admin_chatlogs_dropdown_type'] = 'Such-Typ';
$lang['hotel_admin_chatlogs_chatlogstype_description'] = 'Gib hier den Typ an, nachdem du suchst: Raum oder User.';
$lang['hotel_admin_chatlogs_submit_input'] = 'Chatlogs suchen &raquo;';
$lang['hotel_admin_chatlogs_no_room_found'] = 'Raum nicht gefunden';

// NEWS
$lang['hotel_admin_news'] = 'News';
$lang['hotel_admin_news_title'] = 'News-Titel';
$lang['hotel_admin_news_short_story'] = 'Kurzbeschreibung';
$lang['hotel_admin_news_author'] = 'Autor';
$lang['hotel_admin_news_published'] = 'Ver�ffentlichung';
$lang['hotel_admin_news_image'] = 'News-Image';
$lang['hotel_admin_news_is_campaign'] = 'Kampagne?';
$lang['hotel_admin_news_campaign_image'] = 'Kampagne-Image';
$lang['hotel_admin_news_add'] = 'News hinzuf�gen';
$lang['hotel_admin_news_long_story'] = 'Nachricht';
$lang['hotel_admin_news_submit'] = 'Hinzuf�gen &raquo;';
$lang['hotel_admin_news_add_success'] = 'News erfolgreich hinzugef�gt!';
$lang['hotel_admin_news_delete_success'] = 'News erfolgreich gel�scht!';
$lang['hotel_admin_news_delete_no_news_found'] = 'Diese News wurde nicht gefunden!';
$lang['hotel_admin_news_not_found'] = 'Die News mit der ID %d wurde nicht gefunden!';#
$lang['hotel_admin_news_edit_submit'] = 'Speichern &raquo;';
$lang['hotel_admin_news_edit'] = 'News bearbeiten';
$lang['hotel_admin_news_edit_success'] = 'News mit der ID \'%d\' erfolgreich bearbeitet!';

// Employee-Ranking
$lang['hotel_admin_employee_ranking'] = 'R�nge verwalten';
$lang['hotel_admin_employee_ranking_rank'] = 'Rang';
$lang['hotel_admin_employee_ranking_name'] = 'Rang-Name';
$lang['hotel_admin_employee_ranking_description'] = 'Rang-Beschreibung';
$lang['hotel_admin_employee_ranking_add_success'] = 'Rang erfolgreich gespeichert!';
$lang['hotel_admin_employee_ranking_not_found'] = 'Rang mit der ID \'%d\' nicht gefunden!';
$lang['hotel_admin_employee_ranking_edit_success'] = 'Rang mit der ID \'%d\' erfolgreich gespeichert!';
$lang['hotel_admin_employee_ranking_delete_success'] = 'Rang mit der ID \'%d\' erfolgreich gel�scht!';
$lang['hotel_admin_employee_ranking_edit'] = 'Rang bearbeiten';
$lang['hotel_admin_employee_ranking_add'] = 'Rang hinzuf�gen';

/**
 * PLUGINS
 */

// Employee
$lang['hotel_plugin_employee_registered'] = 'Registriert';
$lang['hotel_plugin_employee_last_online'] = 'Zuletzt online';
$lang['hotel_plugin_employee_information'] = 'Info &raquo';

// Pretty-Date
$lang['hotel_pretty_date_seconds'] = 'vor %d Sekunden';
$lang['hotel_pretty_date_second'] = 'vor einer Sekunde';
$lang['hotel_pretty_date_minutes'] = 'vor %d Minuten';
$lang['hotel_pretty_date_minute'] = 'vor einer Minute';
$lang['hotel_pretty_date_quarter_hour'] = 'vor einer viertel Stunde';
$lang['hotel_pretty_date_half_hour'] = 'vor einer halben Stunde';
$lang['hotel_pretty_date_three_quarter_hour'] = 'vor einer dreiviertel Stunde';
$lang['hotel_pretty_date_hour'] = 'vor einer Stunde';
$lang['hotel_pretty_date_hours'] = 'vor %d Stunden';
$lang['hotel_pretty_date_today'] = 'heute';
$lang['hotel_pretty_date_yesterday'] = 'gestern';
$lang['hotel_pretty_date_before_yesterday'] = 'vorgestern';
$lang['hotel_pretty_date_last_night'] = 'letzte Nacht';
$lang['hotel_pretty_date_night'] = 'Nacht';
$lang['hotel_pretty_date_morning'] = 'Morgen';
$lang['hotel_pretty_date_forenoon'] = 'Vormittag';
$lang['hotel_pretty_date_noon'] = 'Mittag';
$lang['hotel_pretty_date_afternoon'] = 'Nachmittag';
$lang['hotel_pretty_date_evening'] = 'Abend';
$lang['hotel_pretty_date_day'] = 'vor einem Tag';
$lang['hotel_pretty_date_days'] = 'vor %d Tage';
$lang['hotel_pretty_date_week'] = 'vor einer Woche';
$lang['hotel_pretty_date_weeks'] = 'vor %d Wochen';
$lang['hotel_pretty_date_month'] = 'vor einem Monat';
$lang['hotel_pretty_date_months'] = 'vor %d Monaten';
$lang['hotel_pretty_date_year'] = 'vor einem Jahr';
$lang['hotel_pretty_date_one_and_half_year'] = 'vor eineinhalb Jahren';
$lang['hotel_pretty_date_years'] = 'vor %d Jahren';
?>
