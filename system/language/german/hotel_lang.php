<?php
/*********************************************\
|****************** OPENCMS ******************|
|*********************************************|
|* @author Yannici                           *|
|* @copyright Yannici                        *|
|*********************************************|
|* @since 04.11.2013                         *|
\*********************************************/

// GENERAL
$lang['hotel_me'] = 'Ich';
$lang['hotel_home'] = 'Home';
$lang['hotel_settings'] = 'Einstellungen';
$lang['hotel_logout'] = 'Ausloggen';
$lang['hotel_community'] = 'Community';
$lang['hotel_goto_client'] = 'Einchecken &raquo;';
$lang['hotel_readmore'] = 'Lies mehr';
$lang['hotel_read'] = 'Lesen &raquo;';
$lang['hotel_online_users'] = '%d User online';
$lang['hotel_online_users_small'] = '<strong>%d</strong> User online';
$lang['hotel_credits'] = 'Taler';
$lang['hotel_activity_points'] = 'Pixel';
$lang['hotel_vip_points'] = 'Diamant%s';
$lang['hotel_email'] = 'E-Mail';
$lang['hotel_motto'] = 'Mission';
$lang['hotel_yes'] = 'Ja';
$lang['hotel_no'] = 'Nein';
$lang['hotel_latest_news'] = 'Aktuelle News';
$lang['hotel_username'] = 'Username';
$lang['hotel_password'] = 'Passwort';
$lang['hotel_unknown_user'] = 'Unbekannter';
$lang['hotel_identification'] = 'ID';
$lang['hotel_online'] = 'Online';
$lang['hotel_offline'] = 'Offline';
$lang['hotel_users'] = 'User';
$lang['hotel_user'] = 'User';
$lang['hotel_rooms'] = 'R�ume';
$lang['hotel_room'] = 'Raum';
$lang['hotel_back'] = '&laquo; Zur�ck';
$lang['hotel_date'] = 'Datum';
$lang['hotel_message'] = 'Nachricht';
$lang['hotel_plural'] = 'en';
$lang['hotel_startup'] = '%s startet! Bitte warten ...';
$lang['hotel_game'] = 'Client';
$lang['hotel_hour'] = '1 Stunde';
$lang['hotel_hours'] = '%d Stunden';
$lang['hotel_day'] = '1 Tag';
$lang['hotel_days'] = '%d Tage';
$lang['hotel_month'] = '1 Monat';
$lang['hotel_months'] = '%d Monate';
$lang['hotel_year'] = '1 Jahr';
$lang['hotel_years'] = '%d Jahre';
$lang['hotel_permanent'] = 'Permanent';
$lang['hotel_shop'] = 'Shop';
$lang['hotel_ip'] = 'IP-Adresse';

// INSTALL AND UPDATE
$lang['hotel_update_service_unavailable'] = 'Der Update-Server konnte nicht erreicht werden. Bitte deaktiviere die Update-Funktion oder melde dich beim Entwickler!';
$lang['hotel_created_admin_success'] = 'Dein Admin-Account wurde erfolgreich erstellt! Standardm��ig lauten die Daten wie folgend.<br /><br /><strong>Username:</strong> admin<br /><strong>Passwort:</strong> admin';

// SHOP
$lang['hotel_shop_credits'] = 'Taler';
$lang['hotel_shop_what_can_i_do'] = 'Was kann ich mit Talern tun?';
$lang['hotel_shop_thumbnail_label_1'] = 'Tauschen';
$lang['hotel_shop_thumbnail_text_1'] = 'Tausche dich reich! Du kannst mit deinen Freunden handeln und deine M�bel oder Taler tauschen, um so an neue M�bel oder an mehr Taler zu gelangen.';
$lang['hotel_shop_thumbnail_label_2'] = 'R�ume bauen';
$lang['hotel_shop_thumbnail_text_2'] = 'Du kannst deine Taler ausgeben, um R�ume zu bauen. Kaufe M�bel im Katalog, baue deinen Raum, lade deine Freunde ein und hab Spa� :)';
$lang['hotel_shop_thumbnail_label_3'] = 'Rares kaufen';
$lang['hotel_shop_thumbnail_text_3'] = 'Rares sind die wertvollsten Gegenst�nde bzw. M�bel im Hotel. Mit viel Taler kannst du an diese gelangen! Besitzt du viele Rares, wird dich jeder respektieren ;)';

// INDEX
$lang['hotel_welcome'] = 'Willkommen im %s';
$lang['hotel_welcome_message'] = 'Willkommen im %s, wir bieten euch eine Menge Spa�, viele Taler und st�ndig neue Events! Checkt ein und macht mit! :)';
$lang['hotel_register'] = 'Jetzt Registrieren &raquo;';
$lang['hotel_login'] = 'Einloggen &raquo;';
$lang['hotel_thumbnail_label_1'] = 'Freunde finden';
$lang['hotel_thumbnail_label_2'] = 'R�ume bauen';
$lang['hotel_thumbnail_label_3'] = 'Spa� haben';
$lang['hotel_thumbnail_text_1'] = 'Im Hotel findest du garantiert viele neue Freunde!';
$lang['hotel_thumbnail_text_2'] = 'Du kannst tolle R�ume bauen und deiner Fantasie freien Lauf lassen.';
$lang['hotel_thumbnail_text_3'] = 'Mit unserer tollen Community k�nnen wir dir eine Menge Spa� garantieren :)';
$lang['hotel_create_first_news'] = 'Erstelle jetzt deine erste News!';
$lang['hotel_no_news_exists'] = 'Noch keine News vorhanden ...';

// FORM-SPECIFIC
$lang['hotel_form_reset'] = 'Abbrechen';

// PROFIL-SETTINGS
$lang['hotel_profile_settings'] = 'Profil-Einstellungen';
$lang['hotel_profile_settings_navigation'] = 'Einstellungen';
$lang['hotel_profile_settings_new_friends'] = 'Freundschaftsanfragen erlauben?';
$lang['hotel_profile_settings_hide_online'] = 'Online-Status verbergen?';
$lang['hotel_profile_settings_volume'] = 'Lautst�rke';
$lang['hotel_profile_settings_success_saving'] = 'Erfolgreich gespeichert!';
$lang['hotel_profile_settings_trading'] = 'Tauschen erlauben?';
$lang['hotel_profile_edit_password'] = 'Passwort �ndern';
$lang['hotel_settings_newpassword_active_password'] = 'Aktuelles Passwort';
$lang['hotel_settings_newpassword_new_password'] = 'Neues Passwort';
$lang['hotel_settings_newpassword_new_password_wdh'] = 'Neues Passwort wiederholen';
$lang['hotel_profile_newpassword_success_saving'] = 'Passwort erfolgreich gespeichert!';
$lang['hotel_settings_newpassword_active_password_wrong'] = 'Das aktuelle Passwort stimmt nicht!';

// GAMEERROR
$lang['hotel_gameerror_header'] = 'Game-Error';
$lang['hotel_gameerror_clienterror'] = 'Client-Error';
$lang['hotel_gameerror_clienterror_description'] = 'Das Hotel hat einen Error verursacht und die Verbindung wurde unterbrochen. Versuche erneut in das Hotel einzuchecken.';
$lang['hotel_gameerror_back_in_hotel'] = 'Zur�ck ins Hotel &raquo;';

// REGISTRATION
$lang['hotel_password_again'] = 'Passwort Wdh.';
$lang['hotel_registration_complete'] = 'Registrierung abschliessen &raquo;';

// ERROR-MESSAGES
$lang['hotel_error_message_captcha_wrong'] = 'Captcha-Code wurde falsch eingegeben!';
$lang['hotel_error_message_error'] = 'Fehler!';
$lang['hotel_error_message_check_login'] = '%s und %s stimmen nicht �berein!';
$lang['hotel_error_message_ip_check_failed'] = 'Multiaccount verd�chtig!';
$lang['hotel_error_message_username_incorrect'] = 'Username ung�ltig!';
$lang['hotel_error_message_success'] = 'Erfolgreich!';
$lang['hotel_error_message_check_login_bans'] = 'Du wurdest gebannt! Dein Banngrund lautet: "%s" und geht bis %s';

// NAVIGATION
$lang['hotel_hello_message'] = 'Hallo, %s';

// NEWS
$lang['hotel_news_author_date'] = '<strong>%s</strong> am %s um %s Uhr';
$lang['hotel_news_news'] = 'News';
$lang['hotel_news_comments'] = 'Kommentare (%s)';
$lang['hotel_news_send_comment'] = 'Kommentar abschicken &raquo;';
$lang['hotel_news_no_comments'] = 'Noch keine Kommentare vorhanden, sei der Erste!';
$lang['hotel_news_comment_in_time'] = 'Du kannst nur jede Minute einen Kommentar abgeben!';
$lang['hotel_news_comment_success'] = 'Kommentar erfolgreich gespeichert!';
$lang['hotel_news_write_comment'] = 'Schreibe einen Kommentar ...';

/*
 * COMMUNITY
 */

// MITARBEITER
$lang['hotel_community_employee'] = 'Mitarbeiter';

/*
 * OPENADMIN-AREA
 */
// GENERAL
$lang['hotel_admin_name'] = 'OPENADMIN';
$lang['hotel_admin_workers_online'] = 'Mitarbeiter online';
$lang['hotel_admin_client'] = 'Client';
$lang['hotel_admin_logout'] = 'Ausloggen';
$lang['hotel_admin_online_users'] = 'User online';
$lang['hotel_admin_user_record'] = 'Userrekord';
$lang['hotel_admin_user_total'] = 'angemeldete User';
$lang['hotel_admin_edit'] = 'Bearbeiten';
$lang['hotel_admin_delete'] = 'L�schen';
$lang['hotel_admin_manage'] = 'Verwaltung';
$lang['hotel_admin_actions'] = 'Aktionen';
$lang['hotel_admin_save'] = 'Speichern &raquo;';
$lang['hotel_admin_delete_confirmation'] = 'Willst du diesen Datensatz wirklich l�schen?';
$lang['hotel_admin_update_available'] = 'Es ist ein OpenCMS Update vorhanden! Die Version %s wurde ver�ffentlicht (deine Version: %s). Klicke <a href=\'%s\'>hier</a>, um die neuste Version herunterzuladen.';
$lang['hotel_admin_update'] = 'Update verf�gbar!';

// CLONECHECK
$lang['hotel_admin_clonecheck'] = 'Klon-Check';
$lang['hotel_admin_clonecheck_check'] = 'Nach Klone pr�fen';
$lang['hotel_admin_clonecheck_input_checkstring'] = 'Zu pr�fen (User/IP)';
$lang['hotel_admin_clonecheck_input_checkstring_description'] = 'Gib hier ein Username, eine User-ID oder eine IP ein (je nachdem was du bei Typ ausw�hlst), um danach Klone zu suchen';
$lang['hotel_admin_clonecheck_dropdown_checktype'] = 'Such-Typ';
$lang['hotel_admin_clonecheck_dropdown_checktype_description'] = 'W�hle hier, ob du oben ein User (also Username oder User-ID) oder eine IP eingegeben hast!';
$lang['hotel_admin_clonecheck_submit_input'] = 'Nach Klone suchen &raquo;';
$lang['hotel_admin_clonecheck_no_clones_found'] = 'Es wurden keine Klone gefunden!';
$lang['hotel_admin_clonecheck_found_clones'] = 'Es wurden %d potenzielle Klone gefunden!';

// LOGIN
$lang['hotel_admin_login'] = 'Openadmin-Login';
$lang['hotel_admin_login_security_code'] = 'Sicherheitscode';
$lang['hotel_admin_login_submit'] = 'Einloggen &raquo;';

// MESSAGES
$lang['hotel_admin_error_message_security_code_wrong'] = 'Der Sicherheitscode wurde falsch eingegeben!';
$lang['hotel_admin_error_chatlogs_wrong_type'] = 'Du hast einen falschen Typ angegeben!';
$lang['hotel_admin_error_chatlogs_wrong_input'] = 'Die Such-Angabe war inkorrekt!';
$lang['hotel_admin_error_cmdlogs_wrong_input'] = 'Die Such-Angabe war inkorrekt!';

// DASHBOARD
$lang['hotel_admin_dashboard_last_online_users'] = 'Zuletzt eingeloggte User';
$lang['hotel_admin_dashboard_last_ip'] = 'IP-Adresse';
$lang['hotel_admin_last_online_users'] = 'Letzte Logins';
$lang['hotel_admin_last_staff_activities'] = 'Letzte Staff-Aktivit�ten';
$lang['hotel_admin_dashboard'] = 'Dashboard';

// MEMBERS
$lang['hotel_admin_members_login_ipaddress'] = 'Login IP-Adresse';
$lang['hotel_admin_members_register_ipaddress'] = 'Registrierung IP-Adresse';
$lang['hotel_admin_members_last_online'] = 'Zuletzt Online';
$lang['hotel_admin_members'] = 'Mitglieder';
$lang['hotel_admin_members_rank'] = 'Rank';
$lang['hotel_admin_members_delete_no_user_found'] = 'User wurde nicht gefunden!';
$lang['hotel_admin_members_deleted_success'] = 'User mit der ID \'%d\' erfolgreich gel�scht!';
$lang['hotel_admin_members_delete_no_permission'] = 'Du hast keine Berechtigung, um den User \'%s\' zu l�schen!';

// EDIT USER
$lang['hotel_admin_useredit'] = 'Mitglied bearbeiten';
$lang['hotel_admin_useredit_search'] = 'Nach Mitglied suchen';
$lang['hotel_admin_useredit_input_user'] = 'User-ID oder Username';
$lang['hotel_admin_useredit_input_user_description'] = 'Gib hier ein User ein (User-ID oder Username), um diesen zu bearbeiten.';
$lang['hotel_admin_useredit_input_submit'] = 'Mitglied bearbeiten &raquo;';
$lang['hotel_admin_useredit_user_not_found'] = 'Es wurde kein User gefunden!';
$lang['hotel_admin_useredit_general_settings'] = 'Allgemein';
$lang['hotel_admin_useredit_input_username'] = 'Username';
$lang['hotel_admin_useredit_input_email'] = 'E-Mail';
$lang['hotel_admin_useredit_input_password'] = 'Passwort';
$lang['hotel_admin_useredit_input_password_description'] = 'Benutze das Passwort-Tool, um Passw�rter zu �ndern!';
$lang['hotel_admin_useredit_edit'] = '%s bearbeiten';
$lang['hotel_admin_useredit_input_reg_ip'] = 'Registrierung-IP';
$lang['hotel_admin_useredit_input_last_ip'] = 'Letzte IP';
$lang['hotel_admin_useredit_input_id'] = 'ID';
$lang['hotel_admin_useredit_checkbox_block_newfriends'] = 'Freundesanfragen blockieren?';
$lang['hotel_admin_useredit_checkbox_hide_online'] = 'Online-Status verbergen?';
$lang['hotel_admin_useredit_checkbox_trading'] = 'Antauschen zulassen?';
$lang['hotel_admin_useredit_rank_and_subscriptions_settings'] = 'Rank und Privatsph�re';
$lang['hotel_admin_useredit_dropdown_rank'] = 'User-Rank';
$lang['hotel_admin_useredit_checkbox_vip'] = 'VIP?';
$lang['hotel_admin_useredit_look'] = 'Aussehen';
$lang['hotel_admin_useredit_bans'] = 'Bans';
$lang['hotel_admin_useredit_bans_date'] = 'Bann-Datum';
$lang['hotel_admin_useredit_bans_reason'] = 'Grund';
$lang['hotel_admin_useredit_bans_added_by'] = 'Gebannt von';
$lang['hotel_admin_useredit_bans_expire'] = 'Ablauf-Datum';
$lang['hotel_admin_useredit_error_user_not_exists'] = 'Dieser User existiert nicht!';
$lang['hotel_admin_useredit_error_no_rights'] = 'Du kannst keine h�her- oder gleich-rangige User bearbeiten!';
$lang['hotel_admin_useredit_edit_success'] = 'User mit der ID \'%s\' erfolgreich gespeichert!';
$lang['hotel_admin_useredit_user_edited'] = 'User mit der ID \'%d\' bearbeitet!';
$lang['hotel_admin_useredit_delete_user'] = 'User l�schen &raquo;';
$lang['hotel_admin_useredit_want_delete'] = 'M�chtest Du diesen User wirklich l�schen?';
$lang['hotel_admin_useredit_delete_user_success'] = 'Du hast den User erfolgreich gel�scht!';
$lang['hotel_admin_useredit_user_deleted'] = 'User \'%s\' gel�scht!';

// PASSWORD-TOOL
$lang['hotel_admin_password_tool'] = 'Passwort-Tool';
$lang['hotel_admin_password_tool_edit'] = 'Passwort �ndern';
$lang['hotel_admin_password_tool_input_user'] = 'Username/User-ID';
$lang['hotel_admin_password_tool_success'] = 'Passwort erfolgreich gesetzt!';
$lang['hotel_admin_password_tool_input_desc'] = 'Gib hier den Username oder die User-ID ein, von dem du das Passwort �ndern m�chtest.';
$lang['hotel_admin_password_tool_password_new'] = 'Neues Passwort';
$lang['hotel_admin_password_tool_password_new_desc'] = 'Gib hier das neue Passwort f�r den User ein.';
$lang['hotel_admin_password_tool_password_changed'] = 'Passwort von \'%s\' ge�ndert!';

// DATATABLES (FOR TRANSLATIONS SEE: http://datatables.net/plug-ins/i18n)
$lang['hotel_admin_datatables_show_records'] = '_MENU_ Eintr�ge anzeigen'; // _MENU_ is a parameter!!!
$lang['hotel_admin_datatables_no_records_found'] = 'Keine Eintr�ge gefunden!';
$lang['hotel_admin_datatables_records_showing'] = 'Zeige _START_ bis _END_ von _TOTAL_ Eintr�gen'; // _START_, _END_ and _TOTAL_ are parameters!!!
$lang['hotel_admin_datatables_filtered_records'] = '(gefiltert von insgesamt _MAX_ Eintr�gen)'; // _MAX_ is a parameter!!!
$lang['hotel_admin_datatables_search_records'] = 'Suchen:';
$lang['hotel_admin_datatables_processing_records'] = 'Bitte warten ...';
$lang['hotel_admin_datatables_next_page'] = 'N�chste';
$lang['hotel_admin_datatables_previous_page'] = 'Zur�ck';
$lang['hotel_admin_datatables_first_page'] = 'Erste';
$lang['hotel_admin_datatables_last_page'] = 'Letzte';

// PERMISSIONS
$lang['hotel_admin_permissions'] = 'Berechtigungen';
$lang['hotel_admin_permissions_edit'] = 'Berechtigungen bearbeiten';
$lang['hotel_admin_permissions_dropdown_rank_description'] = 'Gib hier die Mitarbeiter-Gruppe an, von denen du die Berechtigung �ndern willst.';
$lang['hotel_admin_permissions_dropdown_rank'] = 'Rank';
$lang['hotel_admin_permissions_submit_input'] = 'Berechtigungen bearbeiten &raquo;';
$lang['hotel_admin_permissions_error_rank_not_found'] = 'Rank zum Bearbeiten nicht gefunden!';
$lang['hotel_admin_permissions_save_success'] = 'Die Berechtigungen wurden erfolgreich gespeichert!';
$lang['hotel_admin_wrong_permissions'] = 'Berechtigungen fehlen';
$lang['hotel_admin_permissions_saved'] = 'Berechtigungen von Rank \'%s\' ge�ndert!';
$lang['hotel_admin_no_permission'] = 'Du hast leider keine Berechtigung, um auf die Seite \'%s\' zugreifen zu k�nnen. Wende dich daf�r an den Hotelbesitzer!';

// LOGS
$lang['hotel_admin_logs_loggedin'] = 'Hat sich in %s eingeloggt!';
$lang['hotel_admin_logs'] = 'Auswertungen';
$lang['hotel_admin_logs_user_deleted_news'] = 'News mit der ID \'%d\' gel�scht!';
$lang['hotel_admin_logs_user_added_news'] = 'News \'%s\' ver�ffentlicht!';
$lang['hotel_admin_logs_user_edited_news'] = 'News \'%s\' bearbeitet!';
$lang['hotel_admin_logs_user_added_employee_ranking_group'] = 'Mitarbeiter-Gruppe \'%s\' f�r Rank \'%d\' hinzugef�gt!';
$lang['hotel_admin_logs_employee_ranking_delete'] = 'Mitarbeiter-Gruppe mit der ID \'%d\' gel�scht!';
$lang['hotel_admin_logs_user_deleted'] = 'User mit der ID \'%d\' gel�scht!';

// CHATLOGS
$lang['hotel_admin_chatlogs'] = 'Chatlogs';
$lang['hotel_admin_chatlogs_search'] = 'Chatlogs suchen';
$lang['hotel_admin_chatlogs_show'] = 'Chatlogs anzeigen';
$lang['hotel_admin_chatlogs_inputfield'] = 'Suchen nach';
$lang['hotel_admin_chatlogs_inputfield_description'] = 'Gib hier eine Raum-ID, User-ID oder einen Usernamen ein. Je nachdem nach was du suchen willst.';
$lang['hotel_admin_chatlogs_dropdown_type'] = 'Such-Typ';
$lang['hotel_admin_chatlogs_chatlogstype_description'] = 'Gib hier den Typ an, nachdem du suchst: Raum oder User.';
$lang['hotel_admin_chatlogs_submit_input'] = 'Chatlogs suchen &raquo;';
$lang['hotel_admin_chatlogs_no_room_found'] = 'Raum nicht gefunden';

// CMD-LOGS
$lang['hotel_admin_cmdlogs'] = 'CMD-Logs';
$lang['hotel_admin_cmdlogs_search'] = 'Nach Command-Logs suchen';
$lang['hotel_admin_cmdlogs_input_cmdlogs'] = 'Suchen nach';
$lang['hotel_admin_cmdlogs_input_cmdlogs_description'] = 'Gib hier einen Usernamen, eine User-ID oder direkt einen Command ein, um danach zu suchen.';
$lang['hotel_admin_chatlogs_dropdown_cmdlogstype'] = 'Such-Typ';
$lang['hotel_admin_chatlogs_dropdown_cmdlogstype_description'] = 'Gib hier den Typ an, nach was du suchen willst. W�hlst du \'Command\', wird nach einem Kommando gesucht. Wird \'User\' ausgew�hlt, so wird nach einem Username oder User-ID gesucht.';
$lang['hotel_admin_cmdlogs_command'] = 'Command';
$lang['hotel_admin_cmdlogs_extra_data'] = 'Extras';

// BAN
$lang['hotel_admin_ban'] = 'User bannen/entbannen';
$lang['hotel_admin_ban_error_search'] = 'Suchangaben inkorrekt!';
$lang['hotel_admin_ban_dropdown_ban'] = 'Bannen/Entbannen?';
$lang['hotel_admin_ban_dropdown_ban_description'] = 'Hier w�hlst du, ob du jemanden bannen oder entbannen willst.';
$lang['hotel_admin_ban_input_value'] = 'User oder IP';
$lang['hotel_admin_ban_input_value_description'] = 'Gib hier die User-ID oder den Usernamen des Users der gebannt/entbannt werden soll an. Oder die IP, die gebannt/entbannt werden soll.';
$lang['hotel_admin_ban_dropdown_length'] = 'Ban-L�nge';
$lang['hotel_admin_ban_dropdown_length_description'] = 'W�hle hier die Banl�nge aus.';
$lang['hotel_admin_ban_input_reason'] = 'Bangrund';
$lang['hotel_admin_ban_input_reason_description'] = 'Gib hier den Grund an, wieso der User oder die IP gebannt wurde. Dies kann aber auch leer sein.';
$lang['hotel_admin_ban_submit_ban_input'] = 'User bannen &raquo;';
$lang['hotel_admin_ban_submit_unban_input'] = 'User entbannen &raquo;';
$lang['hotel_admin_ban_dropdown_ban_ban'] = 'Bannen';
$lang['hotel_admin_ban_dropdown_ban_unban'] = 'Entbannen';
$lang['hotel_ban_dropdown_bantype_user'] = 'User';
$lang['hotel_ban_dropdown_bantype_ip'] = 'IP-Adresse';
$lang['hotel_admin_ban_dropdown_bantype_description'] = 'W�hle hier, ob du eine IP-Adresse oder einen Usernamen/User-ID eingegeben hast.';
$lang['hotel_admin_ban_dropdown_bantype'] = 'Ban-Typ';
$lang['hotel_admin_ban_user_already_banned'] = 'Dieser User oder diese IP-Adresse ist bereits gebannt!';
$lang['hotel_admin_ban_user_already_unbanned'] = 'Keinen Ban dieses Users oder dieser IP-Adresse gefunden!';
$lang['hotel_admin_ban_no_permission'] = 'Du hast keine Berechtigung, um diesen User zu bannen!';
$lang['hotel_admin_ban_success'] = 'User erfolgreich gebannt!';
$lang['hotel_admin_ban_user_already_banned'] = 'Der User ist bereits gebannt!';
$lang['hotel_admin_unban_success'] = 'Der User wurde erfolgreich entbannt!';
$lang['hotel_admin_useredit_error_user_not_exists'] = 'Der angegebene User existiert nicht!';
$lang['hotel_admin_ban_user_already_unbanned'] = 'Der angegebene User ist nicht gebannt!';

// BACKUP
$lang['hotel_admin_backup'] = 'Database-Backup';
$lang['hotel_admin_create_backup'] = 'Datenbank-Backup erstellen und runterladen &raquo;';

// NEWS
$lang['hotel_admin_news'] = 'News';
$lang['hotel_admin_news_title'] = 'News-Titel';
$lang['hotel_admin_news_short_story'] = 'Kurzbeschreibung';
$lang['hotel_admin_news_author'] = 'Autor';
$lang['hotel_admin_news_published'] = 'Ver�ffentlichung';
$lang['hotel_admin_news_image'] = 'News-Image';
$lang['hotel_admin_news_is_campaign'] = 'Kampagne?';
$lang['hotel_admin_news_campaign_image'] = 'Kampagne-Image';
$lang['hotel_admin_news_add'] = 'News hinzuf�gen';
$lang['hotel_admin_news_long_story'] = 'Nachricht';
$lang['hotel_admin_news_submit'] = 'Hinzuf�gen &raquo;';
$lang['hotel_admin_news_add_success'] = 'News erfolgreich hinzugef�gt!';
$lang['hotel_admin_news_delete_success'] = 'News erfolgreich gel�scht!';
$lang['hotel_admin_news_delete_no_news_found'] = 'Diese News wurde nicht gefunden!';
$lang['hotel_admin_news_not_found'] = 'Die News mit der ID %d wurde nicht gefunden!';#
$lang['hotel_admin_news_edit_submit'] = 'Speichern &raquo;';
$lang['hotel_admin_news_edit'] = 'News bearbeiten';
$lang['hotel_admin_news_edit_success'] = 'News mit der ID \'%d\' erfolgreich bearbeitet!';

// Employee-Ranking
$lang['hotel_admin_employee_ranking'] = 'R�nge verwalten';
$lang['hotel_admin_employee_ranking_rank'] = 'Rang';
$lang['hotel_admin_employee_ranking_name'] = 'Rang-Name';
$lang['hotel_admin_employee_ranking_description'] = 'Rang-Beschreibung';
$lang['hotel_admin_employee_ranking_add_success'] = 'Rang erfolgreich gespeichert!';
$lang['hotel_admin_employee_ranking_not_found'] = 'Rang mit der ID \'%d\' nicht gefunden!';
$lang['hotel_admin_employee_ranking_edit_success'] = 'Rang mit der ID \'%d\' erfolgreich gespeichert!';
$lang['hotel_admin_employee_ranking_delete_success'] = 'Rang mit der ID \'%d\' erfolgreich gel�scht!';
$lang['hotel_admin_employee_ranking_edit'] = 'Rang bearbeiten';
$lang['hotel_admin_employee_ranking_add'] = 'Rang hinzuf�gen';

/**
 * PLUGINS
 */

// Employee
$lang['hotel_plugin_employee_registered'] = 'Registriert';
$lang['hotel_plugin_employee_last_online'] = 'Zuletzt online';
$lang['hotel_plugin_employee_information'] = 'Info &raquo';

// Pretty-Date
$lang['hotel_pretty_date_seconds'] = 'vor %d Sekunden';
$lang['hotel_pretty_date_second'] = 'vor einer Sekunde';
$lang['hotel_pretty_date_minutes'] = 'vor %d Minuten';
$lang['hotel_pretty_date_minute'] = 'vor einer Minute';
$lang['hotel_pretty_date_quarter_hour'] = 'vor einer viertel Stunde';
$lang['hotel_pretty_date_half_hour'] = 'vor einer halben Stunde';
$lang['hotel_pretty_date_three_quarter_hour'] = 'vor einer dreiviertel Stunde';
$lang['hotel_pretty_date_hour'] = 'vor einer Stunde';
$lang['hotel_pretty_date_hours'] = 'vor %d Stunden';
$lang['hotel_pretty_date_today'] = 'heute';
$lang['hotel_pretty_date_yesterday'] = 'gestern';
$lang['hotel_pretty_date_before_yesterday'] = 'vorgestern';
$lang['hotel_pretty_date_last_night'] = 'letzte Nacht';
$lang['hotel_pretty_date_night'] = 'Nacht';
$lang['hotel_pretty_date_morning'] = 'Morgen';
$lang['hotel_pretty_date_forenoon'] = 'Vormittag';
$lang['hotel_pretty_date_noon'] = 'Mittag';
$lang['hotel_pretty_date_afternoon'] = 'Nachmittag';
$lang['hotel_pretty_date_evening'] = 'Abend';
$lang['hotel_pretty_date_day'] = 'vor einem Tag';
$lang['hotel_pretty_date_days'] = 'vor %d Tage';
$lang['hotel_pretty_date_week'] = 'vor einer Woche';
$lang['hotel_pretty_date_weeks'] = 'vor %d Wochen';
$lang['hotel_pretty_date_month'] = 'vor einem Monat';
$lang['hotel_pretty_date_months'] = 'vor %d Monaten';
$lang['hotel_pretty_date_year'] = 'vor einem Jahr';
$lang['hotel_pretty_date_one_and_half_year'] = 'vor eineinhalb Jahren';
$lang['hotel_pretty_date_years'] = 'vor %d Jahren';
?>